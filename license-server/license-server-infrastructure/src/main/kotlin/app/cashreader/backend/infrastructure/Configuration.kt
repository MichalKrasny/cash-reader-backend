package app.cashreader.backend.infrastructure

object Configuration {

    const val REGION = "eu-central-1"
    const val ENV = "test"

}