package app.cashreader.backend.infrastructure

import app.cashreader.backend.infrastructure.Configuration.REGION
import aws.sdk.kotlin.services.s3.*
import aws.sdk.kotlin.services.s3.model.BucketLocationConstraint
import aws.smithy.kotlin.runtime.content.ByteStream
import kotlinx.coroutines.runBlocking
import java.util.*

val BUCKET = "bucket-delete-me-${UUID.randomUUID()}"
const val KEY = "enterKeyHere"

const val DB_IDENTIFIER = "cash-reader-db-test"

@SuppressWarnings("kotlin:S125")
fun main(): Unit = runBlocking {
//    DatabaseOperations().createDatabaseInstance("cash-reader-db-test")
//    DatabaseOperations().deleteDatabaseInstance(DB_IDENTIFIER)
//    DatabaseOperations().waitForInstanceReady("test_mk_delete_me")
    Ec2Operations().createEC2Instance("name-mk-delete-me", "ami-07df274a488ca9195")

}

suspend fun s3Tutorial() {
    S3Client
        .fromEnvironment { region = REGION }
        .use { s3 ->
            setupTutorial(s3)

            println("Creating object $BUCKET/$KEY...")

            s3.putObject {
                bucket = BUCKET
                key = KEY
                body = ByteStream.fromString("Testing with the Kotlin SDK")
            }

            println("Object $BUCKET/$KEY created successfully!")

            cleanUp(s3)
        }
}

suspend fun setupTutorial(s3: S3Client) {
    println("Creating bucket $BUCKET...")
    s3.createBucket {
        bucket = BUCKET
        createBucketConfiguration {
            locationConstraint = BucketLocationConstraint.fromValue(REGION)
        }
    }
    println("Bucket $BUCKET created successfully!")
}

suspend fun cleanUp(s3: S3Client) {
    println("Deleting object $BUCKET/$KEY...")
    s3.deleteObject {
        bucket = BUCKET
        key = KEY
    }
    println("Object $BUCKET/$KEY deleted successfully!")

    println("Deleting bucket $BUCKET...")
    s3.deleteBucket {
        bucket = BUCKET
    }
    println("Bucket $BUCKET deleted successfully!")
}