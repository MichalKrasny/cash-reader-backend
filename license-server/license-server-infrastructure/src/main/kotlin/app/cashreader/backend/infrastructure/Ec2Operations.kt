package app.cashreader.backend.infrastructure

import app.cashreader.backend.infrastructure.Configuration.REGION
import aws.sdk.kotlin.services.ec2.Ec2Client
import aws.sdk.kotlin.services.ec2.model.CreateTagsRequest
import aws.sdk.kotlin.services.ec2.model.InstanceType
import aws.sdk.kotlin.services.ec2.model.RunInstancesRequest
import aws.sdk.kotlin.services.ec2.model.Tag

class Ec2Operations {

    suspend fun createEC2Instance(nameVal: String, amiId: String): String? {

        val request = RunInstancesRequest {
            imageId = amiId
            instanceType = InstanceType.T2Small
            maxCount = 1
            minCount = 1
            keyName = "CashReaderBackendKeyPair"
            securityGroups = listOf("Licence Server Security Group")
        }

        Ec2Client { region = REGION }.use { ec2 ->
            val response = ec2.runInstances(request)
            val instanceId = response.instances?.get(0)?.instanceId

            val tagList = Ec2TagsFactory.getTags() + Tag.invoke {
                key = "Name"
                value = nameVal
            }

            val requestTags = CreateTagsRequest {
                resources = listOf(instanceId.toString())
                tags = tagList
            }
            ec2.createTags(requestTags)
            println("Successfully started EC2 Instance $instanceId based on AMI $amiId")
            return instanceId
        }
    }
}