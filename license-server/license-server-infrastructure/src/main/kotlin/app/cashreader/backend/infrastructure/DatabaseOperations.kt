package app.cashreader.backend.infrastructure

import app.cashreader.backend.infrastructure.Configuration.REGION
import aws.sdk.kotlin.services.rds.RdsClient
import aws.sdk.kotlin.services.rds.model.CreateDbInstanceRequest
import aws.sdk.kotlin.services.rds.model.DeleteDbInstanceRequest

class DatabaseOperations {

    suspend fun createDatabaseInstance(
        dbInstanceIdentifierVal: String?,
    ) {
        val instanceRequest = CreateDbInstanceRequest {
            dbInstanceIdentifier = dbInstanceIdentifierVal
            allocatedStorage = 20
            dbName = "cash_reader"
            engine = "postgres"
            dbInstanceClass = "db.t3.micro"
            engineVersion = "15.3"
            storageType = "standard"
            masterUsername = "postgres"
            masterUserPassword = "postgres"
            tags = RdsTagsFactory.getTags()

        }

        //todo make central
        RdsClient { region = REGION }.use { rdsClient ->
            val response = rdsClient.createDbInstance(instanceRequest)
            print("The status is ${response.dbInstance?.dbInstanceStatus}")
        }
    }

//
//    // Waits until the database instance is available.
//    suspend fun waitForInstanceReady(dbInstanceIdentifierVal: String?) {
//        val sleepTime: Long = 20
//        var instanceReady = false
//        var instanceReadyStr = ""
//        println("Waiting for instance to become available.")
//
//        val instanceRequest = DescribeDbInstancesRequest {
//            dbInstanceIdentifier = dbInstanceIdentifierVal
//        }
//
//        RdsClient { region = "us-west-2" }.use { rdsClient ->
//            while (!instanceReady) {
//                val response = rdsClient.describeDbInstances(instanceRequest)
//                val instanceList = response.dbInstances
//                if (instanceList != null) {
//                    for (instance in instanceList) {
//                        instanceReadyStr = instance.dbInstanceStatus.toString()
//                        if (instanceReadyStr.contains("available")) {
//                            instanceReady = true
//                        } else {
//                            println("...$instanceReadyStr")
//                            delay(sleepTime * 1000)
//                        }
//                    }
//                }
//            }
//            println("Database instance is available!")
//        }
//    }


    suspend fun deleteDatabaseInstance(dbInstanceIdentifierVal: String?) {

        val deleteDbInstanceRequest = DeleteDbInstanceRequest {
            dbInstanceIdentifier = dbInstanceIdentifierVal
            deleteAutomatedBackups = true
            skipFinalSnapshot = true
        }

        RdsClient { region = REGION }.use { rdsClient ->
            val response = rdsClient.deleteDbInstance(deleteDbInstanceRequest)
            print("The status of the database is ${response.dbInstance?.dbInstanceStatus}")
        }
    }


}