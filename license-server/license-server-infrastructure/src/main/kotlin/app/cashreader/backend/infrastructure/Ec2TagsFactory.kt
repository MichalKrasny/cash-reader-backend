package app.cashreader.backend.infrastructure

import app.cashreader.backend.infrastructure.Configuration.ENV
import aws.sdk.kotlin.services.ec2.model.Tag

object Ec2TagsFactory {

    fun getTags() = listOf(
        Tag.invoke {
            key = "application"
            value = "cash-reader"
        },
        Tag.invoke {
            key = "env"
            value = ENV
        },
        Tag.invoke {
            key = "service"
            value = "rates-downloader"
        },
    )

}