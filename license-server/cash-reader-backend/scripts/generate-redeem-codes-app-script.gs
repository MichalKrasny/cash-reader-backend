function onOpen() {
  var ui = SpreadsheetApp.getUi();
  ui.createMenu('Cash Reader')
    .addItem('Generate Redeem Codes', 'generateRedeemCodes')
    .addToUi();
}

function generateRedeemCodes() {
  var settings = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Generator').getRange("B:B").getValues();

  var numberOfLicences = settings[0][0]
  Logger.log("numberOfLicences=" + numberOfLicences);

  var notes = settings[1][0]
  Logger.log("notes=" + notes);

  var kvLang = settings[2][0]
  Logger.log("kvLang=" + kvLang);

  var backendUrl = settings[3][0]
  Logger.log("backendUrl=" + backendUrl);

  postGenerateMessage(numberOfLicences, notes, kvLang, backendUrl)

}

function postGenerateMessage(numberOfLicences, notes, kvLang, url) {

  payloadObject = { "numberOfLicences": numberOfLicences, "notes": notes, "kvLang": kvLang}

  var payload = JSON.stringify(payloadObject);

  Logger.log("payload=" + payload);

  var options =
  {
    "method": "POST",
    "payload": payload,
    "followRedirects": true,
    "contentType": "application/json;charset=UTF-8"
  };

  var response = UrlFetchApp.fetch(url, options);

  Logger.log("response:");
  Logger.log(response.getResponseCode());
  Logger.log(response.getContentText());
}



