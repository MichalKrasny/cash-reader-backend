#!/usr/bin/env bash

for i in $(seq 1 1000);
do
    code=`cat /dev/urandom | tr -dc 'A-Z0-9' | fold -w 8 | head -n 1`
    echo "insert into redeem_code (id, redeem_code) VALUES (nextval('hibernate_sequence'), '$code');" >> V24__redeem_codes.sql
    echo "$code" >> codes.txt
done
