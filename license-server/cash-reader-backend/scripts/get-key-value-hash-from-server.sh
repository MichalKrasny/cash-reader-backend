#!/bin/bash

openssl s_client -connect license.cashreader.app:8444 -servername license.cashreader.app </dev/null 2>/dev/null \
| openssl x509 -pubkey -noout -outform PEM > ecdsa_pubkey.pem

pub_hex_value=$(openssl ec -in ecdsa_pubkey.pem -pubin -text -noout | grep -A5 "pub:" | tail -n +2 | tr -d ': \n')

echo "pub_hex_value=${pub_hex_value}"

echo "${pub_hex_value}" | xxd -r -p | openssl dgst -sha256 -binary | openssl base64

