* **split to two projects - one for backend server, one infrastructure, maybe the one for plugin**
* **prepare kotlin script to install database on AWS**
* **document tags**
* **prepare a wipeout script by tags**
* **prepare kotlin script to stop and start ec2 test**

# TODO:

## build.gradle.kts

* change implementation dependencies to something else
* upgrade java libraries
* upgrade to java 17
* upgrade flyway plugin

## project

* test healthcheck
* create full env deployment in kotlin aws client
* remove DB to separate service
* prepare blue-green env
* upgrade spring boot
* upgrade database

## CODE

* remove @Wither

## document project structure

* document environment
* document why we need non-ssl app
* How swagger file is generated
* reporting endpoint

* API
* API testing
* API Documentation

DOCKER

* Gitlab CI
* monitoring
* logging

<!-- TOC -->

* [TODO:](#todo)
    * [build.gradle.kts](#buildgradlekts)
    * [project](#project)
    * [CODE](#code)
    * [document project structure](#document-project-structure)
* [Cash Reader Backend](#cash-reader-backend)
    * [Prerequisites](#prerequisites)
        * [Versions](#versions)
            * [Installed on Windows](#installed-on-windows)
            * [Installed on Linux](#installed-on-linux)
    * [Build](#build)
    * [Installation](#installation)
        * [Installed Ansible in Linux on local machine](#installed-ansible-in-linux-on-local-machine)
        * [Prepare .ssh dir on local machine](#prepare-ssh-dir-on-local-machine)
        * [New server preparation](#new-server-preparation)
            * [.aws configuration](#aws-configuration)
            * [Download .aws configuration to you local machine](#download-aws-configuration-to-you-local-machine)
        * [EC2 Server installation](#ec2-server-installation)
            * [Associate Elastic IP address with correct machine](#associate-elastic-ip-address-with-correct-machine)
            * [Install letsencrypt and refresh DB](#install-letsencrypt-and-refresh-db)
            * [Database migration](#database-migration)
            * [AWS setup](#aws-setup)
        * [Redeem codes infrastructure](#redeem-codes-infrastructure)
        * [On both new and existing server](#on-both-new-and-existing-server)
            * [Run ansible in Linux](#run-ansible-in-linux)
    * [Installing migration scripts only](#installing-migration-scripts-only)
    * [Running](#running)
        * [On Local machine](#on-local-machine)
* [Developers documentation](#developers-documentation)
    * [Project structure](#project-structure)
    * [Configuration](#configuration)
        * [Local](#local)
        * [Server](#server)
    * [Healthcheck](#healthcheck)
    * [REST Api documentation](#rest-api-documentation)
    * [DB renew](#db-renew)
    * [DB show indexes](#db-show-indexes)
    * [Crontab](#crontab)
    * [journalctl](#journalctl)
    * [SSL Cheatsheet](#ssl-cheatsheet)
        * [Export certificate in PEM format](#export-certificate-in-pem-format)
        * [Covert PEM to DER](#covert-pem-to-der)
        * [Compute SHA256 hash from certificate](#compute-sha256-hash-from-certificate)
        * [View keystore content](#view-keystore-content)

<!-- TOC -->

# Cash Reader Backend

## Prerequisites

### Versions

Postgres 9.6

Java 11

Linux WSL

#### Installed on Windows

Postgres and psql tool

Java

Maven

#### Installed on Linux

Ansible

## Build

I'm building this on my local machine.

Start Docker Desktop in Windows

Build Docker image if needed

```
cd .\docker\postgres\
.\build.bat
```

Start PG image in Windows to be able to run integration tests

```
cd .\docker\postgres\
.\run.bat
```

Run `.\gradlew.bat clean build` in Windows

## Installation

User for installing cash reader backend should be

**Account ID:** 3512-1762-4093<BR/>
**IAM user:** cash-reader-backend

### Installed Ansible in Linux on local machine

### Prepare .ssh dir on local machine

on local machine `~/.ssh/config` has proper configuration

```
Host cr-prod
HostName 3.66.146.14
User ec2-user
IdentityFile ~/.ssh/CashReaderBackendKeyPair.pem

Host cr-test
HostName 3.125.24.37
User ec2-user
IdentityFile ~/.ssh/CashReaderBackendKeyPair.pem
```

To connect directly in Linux you can use command from home (for test server)

`ssh -i .ssh/CashReaderBackendKeyPair.pem ec2-user@3.68.108.15`

### New server preparation

#### .aws configuration

You are installing on a newly migrated account. That means the .aws configuration is not prepared, not in S3 and not in
install-hidden. Otherwise, skip this step.

1. Project root is the root directory of license-server repository, for example
   `C:\devel\git\cash-reader-backend\license-server`
   <br/><br/>
1. Create directory on your local machine `[project-root]/install-hidden/home/ec2-user/.aws`
   <br/><br/>
1. Go to: http://aws.amazon.com/
    1. Sign in, go to your AWS account overview.
    1. Account menu in the upper-right (has your name on it).
    1. Sub-menu: My Security Credentials
    1. Create new AccessKey - you will download file with AWSAccessKeyId and AWSSecretKey, put them
       to file `[project-root]/install-hidden/home/ec2-user/.aws/credentials`<BR/>
       Example:
   ```
   [default]
   aws_access_key_id = THISISNOTAKEYBUTLOOKSLIKEONE
   aws_secret_access_key = @rn0ldAsT3rm1n@t0rSa1d//I11BeB@ck
   ```
1. Upload this file to S3 to `s3://cz.hayaku.cashreader.backend.v2/home/ec2-user/.aws/credentials`

#### Download .aws configuration to you local machine

You can skip this step if you already did step [.aws configuration](#aws-configuration)

1. Project root is the root directory of license-server repository, for example
   `C:\devel\git\cash-reader-backend\license-server`
   <br/><br/>
1. Create directory on your local machine `[project-root]/install-hidden/home/ec2-user/.aws`
   <br/><br/>
1. Download file<br/>
   `s3://cz.hayaku.cashreader.backend.v2/home/ec2-user/.aws/credentials`
   <br/>
   to
   <br/>
   `[project-root]/install-hidden/home/ec2-user/.aws/credentials`
   <br/><br/>

They won't be committed to git, because they are ignored, but will be used by Ansible and installed to server.

### EC2 Server installation

If you need a new EC2 Server, then create <BR>
Application and OS Images (Amazon Machine Image): Amazon Linux 2023 AMI <BR>
Instance type: T2 Small <BR>
Key pair (login): CashReader <BR>BackendKeyPair
Network settings: Select existing security group -> Licence Server Security Group <BR>
Storage 10 GB <BR>

#### Associate Elastic IP address with correct machine

Go to Amazon EC2 -> Elastic IPs

If Elastic IP is not allocated:
Create a new elastic IP. You need to contact Martin Doudera to pair new Elastic IP with correct domain name.

If Elastic IP is allocated:
Go to Amazon EC2 -> Elastic IPs
Click on test or prod IP, Disassociate it with old server, associate with new server

#### Install letsencrypt and refresh DB

You need installed ansible in Linux.

```
cd <project_root>\cash-reader-backend\install\ansible\
ansible-playbook letsencrypt-install.yml -i inventory-test.yml -vvv
```

If this fails on error message cotaining `REMOTE HOST IDENTIFICATION HAS CHANGED!`, then you known_hosts don't like the
password change. You can find the command to purge known_hosts in the error message, it will look like this:
`ssh-keygen -f "/home/kras/.ssh/known_hosts" -R "3.125.24.37"`. Rerun the ansible script afterward.

Run `ssh cr-test` and continue with manual steps at the end of letsencrypt-install.yml.

```
sudo certbot -d test.cashreader.app -m michal.krasny@email.cz --agree-tos

#leave password blank
sudo openssl pkcs12 -export -in /etc/letsencrypt/live/test.cashreader.app/fullchain.pem -inkey /etc/letsencrypt/live/test.cashreader.app/privkey.pem -out /opt/certificate/keystore.p12 -name cash-reader-certificate  -CAfile /etc/letsencrypt/live/test.cashreader.app/chain.pem  -caname root


```

Then run

```
ansible-playbook main.yml -i inventory-test.yml --extra-vars "refresh_db=defined" -vvv
```

Each server will know what environment it is by $env property stored in file /etc/environment.

#### Database migration

On old server `ssh -i ~/.ssh/CashReaderBackendKeyPair.pem ec2-user@<ip_address>
`

```
#stop cash_reader
sudo systemctl status cash*
sudo systemctl stop cash*

#backup DB - both standard way and the one for dump
pg_dump -U postgres -d cash_reader -Z 9  | aws s3 cp --storage-class STANDARD_IA --sse aws:kms - s3://cz.hayaku.cashreader.db.v2/${env}/dump-`date +\%Y-\%m-\%d-\%H-\%M-\%S`.sql.gz
pg_dump -U postgres -d cash_reader  | aws s3 cp --storage-class STANDARD_IA --sse aws:kms - s3://cz.hayaku.cashreader.db.v2/${env}/dump-migration.sql
```

On new server:

```
#stop cash_reader
sudo systemctl status cash*
sudo systemctl stop cash*

# download sql dump script
aws s3 cp s3://cz.hayaku.cashreader.db.v2/${env}/dump-migration.sql /tmp/dump-migration.sql

# drop old DB
psql -U postgres -h localhost -c "drop database if exists cash_reader;" && psql -U postgres -h localhost -c "create database cash_reader;"

#import the dump
psql -U postgres -h localhost -d cash_reader -f /tmp/dump-migration.sql

# smoke test
psql -U postgres -h localhost -d cash_reader -c "select count(*) from license"


#start cash_reader
sudo systemctl start cash*
sudo systemctl status cash*
```

Stop old server in EC2 console

#### AWS setup

Route 53 is a global service to create health checks, that will monitor
an endpoint. An alarm can be configured to send a notification to an SNS topic.
When creating new healthcheck, set Failure threshold to 1. For some reason the topic can
not be in the Ireland region, however N. Virginia works.

1. go to Amazon SNS, create a standard topic
1. create subscriptions in the topic
1. Go to Route 53, create healthcheck and alarm

### Redeem codes infrastructure

Skip steps that are not relevant.

1. Create Google Spreadsheet in the location where you want the export to be done.
1. Follow Google documentation to create service account  https://cloud.google.com/iam/docs/service-accounts-create. Our
   service account is named `cash-reader-spreadsheets-api`.
1. You'll be able to get the service account json file, upload it to s3 into each environment folder.
   For PROD it is `s3://cz.hayaku.cashreader.backend.v2/conf/cr-prod/service-account.json`. Upload also for test and
   dev. The ansible installer will synchronize this s3 bucket to the ec2 linux instance during installation. If you lose
   it, just generate new key and get a new file.
1. Create Google Sheets file for the exported data. Create a sheet with name `DB Export`
1. Share the file with the service account. You need to get the service account email from `service-account.json`, it
   looks like this
   `cash-reader-backend-service-ac@cash-reader-spreadsheets-api.iam.gserviceaccount.com`
1. Copy the Google Sheet id from url in browser into the configuration template so the backend will know the Sheet ID to
   interact with. URL looks like
   this `https://docs.google.com/spreadsheets/d/1V5zuCuJRzOXShuQALF17ao_Foxw8RRGucFXBLFYNG94/edit?gid=1613085703#gid=1613085703`
   and the IDis this `1V5zuCuJRzOXShuQALF17ao_Foxw8RRGucFXBLFYNG94`

Redeem Codes Generation

1. Login to cloudflare, select `doudera.m@gmail.com`, go to `Manage Account -> Account API Tokens` and create Account API Token with name `cash-reader-backend-kv-storage`
   with right to edit the KV Store.
1. Copy the token to file application-hidden.properties and upload it to S3. For `PROD` it
   is `s3://cz.hayaku.cashreader.backend.v2/conf/cr-prod/application-hidden.properties. Do it for `TEST` and `DEV`
   too. It will be downloaded during installation of the application.
1. Under `doudera.m@gmail.com` go to `Workers & Pages->KV` and prepare two KV namespaces - `LINKS_KV` and `LINKS_KV_TEST`.
1. Prepare page Generator, use App Script in file `scripts/generate-redeem-codes-app-script.gs`

### On both new and existing server

#### Run ansible in Linux

This procedure will install all necessary stuff to linux and the both cash-reader-selfsigned and cash-reader-selfsigned
applications.

```
cd ./install/ansible
ansible-playbook main.yml -i inventory-test.yml -vvv
```

## Installing migration scripts only

If you need to upgrade DB with new content, like adding a new bundle, you may run just the DB installation.

1. Make sure DB is on the server is accessible. The server configuration should accept all connections from everywhere,
   you just need to add a rule into s security group `Licence Server Security Group`.
   ![img.png](docs/readme-md-images/img.png)

1. In Windows run

| environment | command                                                                                            |
|-------------|----------------------------------------------------------------------------------------------------|
| localhost   | `.\gradlew flywayMigrate`                                                                          |
| test        | `.\gradlew flywayMigrate "-Dflyway.url=jdbc:postgresql://test.cashreader.app:5432/cash_reader"`    |
| prod        | `.\gradlew flywayMigrate "-Dflyway.url=jdbc:postgresql://license.cashreader.app:5432/cash_reader"` |

## Running

### On Local machine

1. Run Postgres by `cash-reader-backend\docker\postgres\run.bat`
1. Run CashReaderBackend from the Intellij Idea with command line parameter `-Dspring.profiles.active=local,hidden`.

# Developers documentation

## Project structure

```
├── config - Spring boot application configuration for local run. See install/ansible for the config that will be deployed
├── docker
│   └── postgres - docker image for running DB for local usage 
├── gradle
├── install - files needed for installation and Ansible playbooks
├── install-hidden - ignored by git, now contains .aws/credentials file for instalation purposes
├── jmeter - test plans for performance test by JMeter
├── notes - some random stuff
├── src - application source code
├── test-requests - Intellij Idea HTTP requests 
└── .gitlab-ci.yml - gitlab continuous integration configuration file, probably not used at the moment
```

## Configuration

### Local

Spring boot uses by default the `config` directory in the project root. File application.properties contains settings that
should be shared by both locally run application and both tests. In application-local properties is configuration, that should
be used only for a local run (it contains IDs of spreadsheets to export to etc.) and we don't tests to reach them. Then
there is `application-hidden` properties, which contains data that shouldn't be commited to Git. Download this file from S3, preferably
`cr-test` config.

### Server

Ansible uses files from `cash-reader-backend\install\ansible\files` and `cash-reader-backend\install\ansible\templates`

## Healthcheck

Healthcheck indicators we removed from the code, because it was observed, that
Amazon does the healthcheck from 16 places at once and that was a too much load
for the system.

## REST Api documentation

- Runtime GUI documentation is accessible on path "/", for example `localhost:8080`.
- Runtime OpenApi documentation is accessible on path /api-docs, for example `localhost:8080/api-docs`

Documentation is also generated in an integration test `DocumentationGeneratingIT` and stored to file. Then it is moved
by `cp` command of maven-antrun-plugin in pom.xml to directory `target/dist/`. See execution
`<id>copy-open-api-documentation-to-dist</id>`. Beware, that this file will not be generated if the integration tests
are skipped.

## DB renew

In Windows disconnect everyone from DB, then

```
psql -U postgres -h localhost  -c "drop database cash_reader"
psql -U postgres -h localhost  -c "create database cash_reader"
psql -U postgres -h localhost -d cash_reader -f dump-2021-04-03-00-00-01.sql
```

## DB show indexes

```
SELECT tablename, indexname, indexdef FROM pg_indexes WHERE schemaname = 'public' ORDER BY tablename, indexname;
```

## Delete redeem codes after code

```
psql -U postgres -h localhost -d cash_reader -c "delete from redeem_code where id > (select id from redeem_code where redeem_code = '63UT5IIL');"
```

## Crontab

Crontab fails are in /var/spool/mail/ec2-user

## journalctl

`journalctl --disk-usage`

## SSL Cheatsheet

#password "123456"
#test
keytool -genkeypair -alias cash-reader-certificate -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore
cash-reader-certificate.p12 -validity 36500 -ext san=dns:test.cashreader.app -dname "CN=test.cashreader.app,
OU=Development, O=Hayaku s.r.o, L=Brno, ST=South Moravia, C=CZ"
#prod
keytool -genkeypair -alias cash-reader-certificate -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore
cash-reader-certificate.p12 -validity 36500 -ext san=dns:license.cashreader.app -dname "CN=license.cashreader.app,
OU=Development, O=Hayaku s.r.o, L=Brno, ST=South Moravia, C=CZ"

### Export certificate in PEM format

openssl pkcs12 -in cash-reader-certificate.p12 -clcerts -nokeys -out cash-reader-certificate.pem

### Covert PEM to DER

openssl x509 -outform der -in cash-reader-certificate.pem -out cash-reader-certificate.der

### Compute SHA256 hash from certificate

openssl x509 -noout -fingerprint -sha256 -inform pem -in cash-reader-certificate.pem >
cash-reader-certificate-sha256.txt

### View keystore content

keytool -list -v -keystore cash-reader-certificate.p12

#export private key - should not be needed
openssl pkcs12 -in cash-reader-certificate.p12 -out cash-reader-certificate.key.pem -clcerts -nodes

