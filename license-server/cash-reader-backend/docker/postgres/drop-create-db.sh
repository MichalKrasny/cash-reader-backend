#!/usr/bin/env bash

psql -U postgres -h localhost -c "drop database if exists cash_reader;" && psql -U postgres -h localhost -c "create database cash_reader;"
psql -U postgres -h localhost -c "drop database if exists cash_reader_test;" && psql -U postgres -h localhost -c "create database cash_reader_test;"
