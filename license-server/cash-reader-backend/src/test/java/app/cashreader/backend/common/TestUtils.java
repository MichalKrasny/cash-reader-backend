package app.cashreader.backend.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Another lovely Utils class with methods that have almost nothing in common except they are shared among the tests.
 */
@Component
public class TestUtils {

    @Autowired
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public static <T> T asJsonObject(final String string, Class<T> clazz) {
        try {
            return new ObjectMapper().readValue(string, clazz);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
