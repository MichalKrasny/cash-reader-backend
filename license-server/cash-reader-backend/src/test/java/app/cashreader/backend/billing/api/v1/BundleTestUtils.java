package app.cashreader.backend.billing.api.v1;

import app.cashreader.backend.db.model.Bundle;
import org.springframework.stereotype.Component;

@Component
public class BundleTestUtils {
    //todo make cz.hayaku.cashreader only testing bundle for e2e tests and use a different one for the integration tests
    public static final String BUNDLE_ID = "cz.hayaku.cashreader";
    public static final String BUNDLE_ID_EXPIRED = "cz.hayaku.cashreader.expired";

    public static final String VALID_UNLIMITED_BUNLE_ID = "valid.unlimited.bundle";
    public static final String VALID_LIMITED_BUNLE_ID = "valid.limited.bundle";

    public Bundle getValidUnlimitedBundle(String bundleId) {
        return new Bundle(null, bundleId, null, false, null);
    }
}
