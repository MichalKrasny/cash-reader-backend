package app.cashreader.backend.license.api.v1;

import java.util.UUID;

import static app.cashreader.backend.billing.api.v1.BundleTestUtils.BUNDLE_ID;

public class LicenseRequestTestUtils {

    // existing BundleId, should be present in DB

    public static final String USER_ID = "userId";
    public static final String DEVICE_ID = "deviceId";
    public static final String PLATFORM_IOS = "iOS";
    public static final String PLATFORM_ANDROID = "Android";

    public static LicenseRequest getLicenseRequest() {
        return getLicenseRequest(1);
    }

    public static LicenseRequest getLicenseRequestUnique() {
        return getLicenseRequest(UUID.randomUUID().toString());
    }

    public static LicenseRequest getLicenseRequest(int discriminant) {
        return getLicenseRequest((String.valueOf(discriminant)));
    }

    public static LicenseRequest getLicenseRequest(String discriminant) {
        return new LicenseRequest(BUNDLE_ID, USER_ID + discriminant, PLATFORM_IOS, DEVICE_ID);
    }

}
