package app.cashreader.backend.redeem.api.v1;

import app.cashreader.backend.db.model.RedeemCode;
import app.cashreader.backend.db.repository.RedeemCodeRepository;
import app.cashreader.backend.redeem.RedeemCodeUtils;
import app.cashreader.backend.redeem.service.RedeemCodeStatus;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import static app.cashreader.backend.redeem.api.v1.RedeemCodeController.REDEEM_PATH;
import static app.cashreader.backend.redeem.api.v1.RedeemCodeController.SETTINGS_ID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
@ActiveProfiles({"test"})
public class RedeemCodeControllerIT {


    @Autowired
    RedeemCodeRepository redeemCodeRepository;

    @LocalServerPort
    int port;

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    RedeemCodeUtils redeemCodeUtils;

    String url;

    @Before
    public void setUp() {
        this.url = "https://127.0.0.1:" + port + "/" + REDEEM_PATH;
        redeemCodeUtils.setUp(url);
    }

    @Test
    public void isValid_validCode() {
        redeemCodeRepository.save(RedeemCode.newEntity("ASDF1234"));

        ResponseEntity<RedeemCodeResponse> responseEntity = redeemCodeUtils.sendRedeemCodeGet("ASDF1234");

        assertThat(responseEntity.getStatusCode()).isEqualTo(OK);
        assertThat(responseEntity.getBody()).isEqualTo(new RedeemCodeResponse(RedeemCodeStatus.VALID));

        RedeemCode redeemCode = redeemCodeRepository.findByRedeemCode("ASDF1234");

        assertThat(redeemCode.getRedeemCode()).isEqualTo("ASDF1234");
        assertThat(redeemCode.getUserId()).isEqualTo("Michal");
        assertThat(redeemCode.getPlatform()).isEqualTo("iOS");
        assertThat(redeemCode.getCampaign()).isEqualTo("iran");
        assertThat(redeemCode.getAppVersion()).isEqualTo("1.67");
        assertThat(redeemCode.getCurrency()).isEqualTo("eur");
        assertThat(redeemCode.getLang()).isEqualTo("cs");
        assertThat(redeemCode.getSystemVersion()).isEqualTo("18.0.1");
    }


    @Test
    public void isValid_requiredAndOptionalParameters() {
        String redeemCode = "PUBLIC_AND_OPTIONAL";
        redeemCodeRepository.save(RedeemCode.newEntity(redeemCode));

        UriComponents onlyRequiredParams = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam(SETTINGS_ID, redeemCode)
                .build();

        ResponseEntity<RedeemCodeResponse> responseEntity = redeemCodeUtils.sendRedeemCodeGet(onlyRequiredParams);

        assertThat(responseEntity.getStatusCode()).isEqualTo(OK);
        assertThat(responseEntity.getBody()).isEqualTo(new RedeemCodeResponse(RedeemCodeStatus.VALID));
    }


    @Test
    public void isValid_invalidCode() {
        ResponseEntity<RedeemCodeResponse> responseEntity = redeemCodeUtils.sendRedeemCodeGet("I_DONT_EXIST");

        assertThat(responseEntity.getStatusCode()).isEqualTo(BAD_REQUEST);
        assertThat(responseEntity.getBody()).isEqualTo(new RedeemCodeResponse(RedeemCodeStatus.INVALID));
    }


    @Test
    public void isValid_alreadyUsedCode() {
        redeemCodeRepository.save(RedeemCode.newEntity("WILL_BE_USED_TWICE"));

        redeemCodeUtils.sendRedeemCodeGet("WILL_BE_USED_TWICE");
        ResponseEntity<RedeemCodeResponse> responseEntity = redeemCodeUtils.sendRedeemCodeGet("WILL_BE_USED_TWICE");

        assertThat(responseEntity.getStatusCode()).isEqualTo(BAD_REQUEST);
        assertThat(responseEntity.getBody()).isEqualTo(new RedeemCodeResponse(RedeemCodeStatus.ALREADY_SET));
    }


}