package app.cashreader.backend.bundle.api.v1;

import app.cashreader.backend.db.model.Bundle;
import app.cashreader.backend.db.repository.BundleRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.ZonedDateTime;

import static app.cashreader.backend.bundle.api.v1.BundleStatus.*;
import static org.assertj.core.api.Assertions.assertThat;


//todo own error object and tests
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
@ActiveProfiles({"test"})
public class BundleControllerIT {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    BundleRepository bundleRepository;

    private String url;


    @Before
    public void setUp() {
        url = "https://127.0.0.1:" + port + "/api/v1/bundle/validator";
    }

    @Test
    public void isValid_validBundle() {
        final BundleIsLicensedResponse result = postForBundleValidation("cz.hayaku.cashreader");
        assertThat(result.getBundleStatus()).isEqualTo(VALID);
    }

    @Test
    public void isValid_nonExistingBundle() {
        final BundleIsLicensedResponse result = postForBundleValidation("non-existing-bundle-id");
        assertThat(result.getBundleStatus()).isEqualTo(UNKNOWN_BUNDLE_ID);
    }


    @Test
    public void isValid_expiredBundle() {
        final String expiredBundleId = "expired-bundle-id";
        Bundle bundle = bundleRepository.findByBundleStoreId(expiredBundleId);
        if (bundle == null) {
            bundleRepository.save(new Bundle(null, expiredBundleId, ZonedDateTime.now().minusMinutes(1), false, null));
        }

        final BundleIsLicensedResponse result = this.postForBundleValidation(expiredBundleId);
        assertThat(result.getBundleStatus()).isEqualTo(EXPIRED);
    }

    @SuppressWarnings("SameParameterValue")
    private BundleIsLicensedResponse postForBundleValidation(final String bundleId) {
        final BundleIsLicensedRequest request = new BundleIsLicensedRequest(bundleId);
        return this.restTemplate.postForObject(url, request, BundleIsLicensedResponse.class);
    }

}
