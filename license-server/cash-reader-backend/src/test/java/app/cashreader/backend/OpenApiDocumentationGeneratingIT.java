package app.cashreader.backend;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.FileWriter;
import java.io.Writer;

/**
 * This a hack how to get swagger file from Springfox at build time. We run a test and output it into file.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Slf4j
@ActiveProfiles({"test"})
public class OpenApiDocumentationGeneratingIT {

    @Autowired
    private MockMvc mockMvc;

    @Value("${springdoc.api-docs.path}")
    private String apiDocPath;

    @Test
    public void generateOpenApiFile() throws Exception {
        log.info("Generating OpenAPI documentation");

        String contentAsString = mockMvc
                .perform(MockMvcRequestBuilders.get(apiDocPath)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse().getContentAsString();
        try (Writer writer = new FileWriter("build/generated/sources/cash-reader-backend-swagger.json")) {
            IOUtils.write(contentAsString, writer);
        }
    }
}
