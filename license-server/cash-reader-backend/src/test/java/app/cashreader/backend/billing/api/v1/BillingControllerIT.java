package app.cashreader.backend.billing.api.v1;

import app.cashreader.backend.billing.service.BillingService;
import app.cashreader.backend.common.TestUtils;
import app.cashreader.backend.db.model.Bundle;
import app.cashreader.backend.db.model.License;
import app.cashreader.backend.db.repository.BundleRepository;
import app.cashreader.backend.db.repository.LicenseRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;

import static app.cashreader.backend.billing.api.v1.BillingErrorResponse.BillingErrorCode.UNKNOWN_BUNDLE_ID;
import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles({"test"})
//todo all tests to mocks
//todo or make all tests running on random port
@AutoConfigureMockMvc
@Slf4j
@Transactional
public class BillingControllerIT {

    public static final String REQUESTED_BUNDLE_ID = "testing.bundle.id";
    public static final double FEE = 5.0;
    public static final int YEAR = 2019;
    public static final int MONTH = 5;
    public static final LocalDateTime LAST_DAY_LAST_MINUTE_OF_MONTH = LocalDateTime.of(YEAR, MONTH, 31, 23, 59, 59);
    public static final LocalDateTime FIRST_DAY_FIRST_MINUTE_OF_MONTH = LocalDateTime.of(YEAR, MONTH, 1, 0, 0);
    //just to better express intention
    public static final LocalDateTime ANY_DAY_OF_BILLED_YEAR_AND_MONTH = FIRST_DAY_FIRST_MINUTE_OF_MONTH;


    @Autowired
    private LicenseRepository licenseRepository;

    @Autowired
    private BundleRepository bundleRepository;

    @Autowired
    private MockMvc mvc;


    @Test
    public void recordsInDifferentMonthsWillNotAppearInResult() throws Exception {

        final Bundle bundle = createBundleInDb(REQUESTED_BUNDLE_ID);
        final License firstDayLicense = createLicenseInDb(bundle, FIRST_DAY_FIRST_MINUTE_OF_MONTH);
        final License lastDayLicense = createLicenseInDb(bundle, LAST_DAY_LAST_MINUTE_OF_MONTH);

        //another licenses that should not be contained in the output
        createLicenseInDb(bundle, LAST_DAY_LAST_MINUTE_OF_MONTH.minusMonths(1));
        createLicenseInDb(bundle, FIRST_DAY_FIRST_MINUTE_OF_MONTH.plusMonths(1));
        createLicenseInDb(bundle, FIRST_DAY_FIRST_MINUTE_OF_MONTH.plusYears(1));
        createLicenseInDb(bundle, LAST_DAY_LAST_MINUTE_OF_MONTH.plusYears(1));

        final BillingRequestForMonth billingRequestForMonth = new BillingRequestForMonth(REQUESTED_BUNDLE_ID, YEAR, MONTH, FEE, newArrayList());
        final MvcResult mvcResult = sendRequest(billingRequestForMonth);
        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(200);

        final BillingResponse result = TestUtils.asJsonObject(mvcResult.getResponse().getContentAsString(), BillingResponse.class);

        assertThat(result.getBillingItems()).hasSize(2);
        assertThat(result.getBillingItems()).containsExactlyInAnyOrder(toBillingItem(firstDayLicense), toBillingItem(lastDayLicense));
        assertThat(result.getBundleId()).isEqualTo(REQUESTED_BUNDLE_ID);
        assertThat(result.getTotalFee()).isEqualTo(2 * FEE);
    }


    @Test
    public void moreBundlesInOneMonthReturnsOneBundleLicenses() throws Exception {
        final Bundle billedBundle = createBundleInDb(REQUESTED_BUNDLE_ID);
        final License billedLicense = createLicenseInDb(billedBundle, ANY_DAY_OF_BILLED_YEAR_AND_MONTH);
        final Bundle notBilledBundle = createBundleInDb("other.bundle.id");
        createLicenseInDb(notBilledBundle, ANY_DAY_OF_BILLED_YEAR_AND_MONTH);


        final BillingRequestForMonth billingRequestForMonth = new BillingRequestForMonth(REQUESTED_BUNDLE_ID, YEAR, MONTH, FEE, newArrayList());
        final MvcResult mvcResult = sendRequest(billingRequestForMonth);
        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(200);

        final BillingResponse result = TestUtils.asJsonObject(mvcResult.getResponse().getContentAsString(), BillingResponse.class);

        assertThat(result.getBillingItems()).containsExactlyInAnyOrder(toBillingItem(billedLicense));
    }

    @Test
    public void notAuthenticatedUserShallNotPass_noCredentials() throws Exception {
        final BillingRequestForMonth billingRequestForMonth = new BillingRequestForMonth(REQUESTED_BUNDLE_ID, YEAR, MONTH, FEE, newArrayList());

        final MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .post("/api/v1/billing")
                //no authorization, no access
                //.header("Authorization", "Basic c2xvbjpkdXBlQUtvdXJpVm9kbmlEeW1rdU1pcnU")
                .contentType("application/json;charset=UTF-8")
                .content(TestUtils.asJsonString(billingRequestForMonth))
        ).andReturn();

        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(401);
    }

    @Test
    public void notAuthenticatedUserShallNotPass_badCredentials() throws Exception {
        final BillingRequestForMonth billingRequestForMonth = new BillingRequestForMonth(REQUESTED_BUNDLE_ID, YEAR, MONTH, FEE, newArrayList());

        final MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .post("/api/v1/billing")
                .header("Authorization", "Basic c2xvbjpkdXBlQUtvdXJpVm9kbmlEeW1rdU1pcnU" + "WRONG!")
                .contentType("application/json;charset=UTF-8")
                .content(TestUtils.asJsonString(billingRequestForMonth))
        ).andReturn();

        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(401);
    }

    @Test
    public void unpaidUserWillNotBeCharged() throws Exception {
        final Bundle bundle = createBundleInDb(REQUESTED_BUNDLE_ID);
        final License billedLicense = createLicenseInDb(bundle, ANY_DAY_OF_BILLED_YEAR_AND_MONTH);
        final License freeLicense = createLicenseInDb(bundle, ANY_DAY_OF_BILLED_YEAR_AND_MONTH);

        //test also trimming, because there can be a white space in the google sheet, that will send us the data
        final String freeUserIdWithWhiteSpaces = " " + freeLicense.getUserId() + " ";
        final BillingRequestForMonth billingRequestForMonth = new BillingRequestForMonth(REQUESTED_BUNDLE_ID, YEAR, MONTH, FEE, newArrayList(freeUserIdWithWhiteSpaces));
        final MvcResult mvcResult = sendRequest(billingRequestForMonth);
        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(200);

        final BillingResponse result = TestUtils.asJsonObject(mvcResult.getResponse().getContentAsString(), BillingResponse.class);

        assertThat(result.getBillingItems()).hasSize(2);
        assertThat(result.getBillingItems()).containsExactlyInAnyOrder(toBillingItem(billedLicense), toBillingItem(freeLicense, 0.0));
        assertThat(result.getBundleId()).isEqualTo(REQUESTED_BUNDLE_ID);
        assertThat(result.getTotalFee()).isEqualTo(FEE);
        log.info("toBillingItem(billedLicense)={}", toBillingItem(billedLicense));
    }

    @Test
    public void expiredBundleDoesNotMatter() throws Exception {
        final Bundle expiredBundle = new Bundle(null, REQUESTED_BUNDLE_ID, ZonedDateTime.of(ANY_DAY_OF_BILLED_YEAR_AND_MONTH.minusYears(10), ZoneId.systemDefault()), false, null);
        final Bundle savedExpiredBundle = bundleRepository.save(expiredBundle);
        final License billedLicense = createLicenseInDb(savedExpiredBundle, ANY_DAY_OF_BILLED_YEAR_AND_MONTH);

        final BillingRequestForMonth billingRequestForMonth = new BillingRequestForMonth(REQUESTED_BUNDLE_ID, YEAR, MONTH, FEE, newArrayList());

        final MvcResult mvcResult = sendRequest(billingRequestForMonth);
        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(200);

        final BillingResponse result = TestUtils.asJsonObject(mvcResult.getResponse().getContentAsString(), BillingResponse.class);

        assertThat(result.getBillingItems()).containsExactlyInAnyOrder(toBillingItem(billedLicense));
        assertThat(result.getBundleId()).isEqualTo(REQUESTED_BUNDLE_ID);
        assertThat(result.getTotalFee()).isEqualTo(FEE);
    }


    @Test
    public void unknownBundleReturnsReasonableError() throws Exception {
        final String unknownBundleStoreId = "iDontExist";
        final BillingRequestForMonth billingRequestForMonth = new BillingRequestForMonth(unknownBundleStoreId, YEAR, MONTH, FEE, newArrayList());

        final MvcResult mvcResult = sendRequest(billingRequestForMonth);
        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());

        final BillingErrorResponse result = TestUtils.asJsonObject(mvcResult.getResponse().getContentAsString(), BillingErrorResponse.class);
        assertThat(result).isEqualTo(new BillingErrorResponse(UNKNOWN_BUNDLE_ID, "Bundle doesn't exist, bundleId=" + unknownBundleStoreId));
    }


    private MvcResult sendRequest(final BillingRequestForMonth billingRequestForMonth) {
        try {
            return mvc.perform(MockMvcRequestBuilders
                    .post("/api/v1/billing")
                    .header("Authorization", "Basic c2xvbjpkdXBlQUtvdXJpVm9kbmlEeW1rdU1pcnU")
                    .contentType("application/json;charset=UTF-8")
                    .content(TestUtils.asJsonString(billingRequestForMonth))
            ).andReturn();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private BillingItem toBillingItem(final License license) {
        return toBillingItem(license, FEE);
    }

    private BillingItem toBillingItem(final License license, double fee) {
        return new BillingItem(license.getUserId(), license.getLicenseNumber().toString(), license.getBillingDate().format(BillingService.FORMATTER), fee);
    }

    private License createLicenseInDb(final Bundle bundle, final LocalDateTime createdAt) {
        final License license = new License(null, UUID.randomUUID(), bundle, "userId" + UUID.randomUUID(), createdAt, newArrayList());
        return licenseRepository.save(license);
    }

    private Bundle createBundleInDb(final String bundleStoreId) {
        final Bundle bundle = new Bundle(null, bundleStoreId, null, false, null);
        return bundleRepository.save(bundle);
    }


}