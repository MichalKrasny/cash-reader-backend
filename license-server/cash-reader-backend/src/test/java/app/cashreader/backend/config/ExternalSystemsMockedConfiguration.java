package app.cashreader.backend.config;

import app.cashreader.backend.redeem.service.cloudflare.kv.KvClient;
import app.cashreader.backend.redeem.service.export.SheetsWriter;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("test")
public class ExternalSystemsMockedConfiguration {

    @MockBean
    public KvClient kvClient;

    @MockBean
    public SheetsWriter sheetsWriter;
}
