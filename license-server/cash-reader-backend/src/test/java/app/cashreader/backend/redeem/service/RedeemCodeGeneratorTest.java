package app.cashreader.backend.redeem.service;

import app.cashreader.backend.redeem.service.generator.CodeGenerator;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RedeemCodeGeneratorTest {


    CodeGenerator tested = new CodeGenerator();

    @Test
    public void get() {
        assertThat(tested.get("abcd1234", 8)).isNotBlank().hasSize(8);
    }
}