package app.cashreader.backend.redeem.service.export;

import app.cashreader.backend.db.repository.RedeemCodeRepository;
import app.cashreader.backend.redeem.RedeemCodeUtils;
import app.cashreader.backend.redeem.api.v1.RedeemCodeResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static app.cashreader.backend.redeem.api.v1.RedeemCodeController.REDEEM_PATH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * Class tests that feature can be turned off.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"test"})
@TestPropertySource(properties = {"redeem.service.export.enabled=false"})
public class RedeemExporterFeatureFlagIT {

    public static final String REDEEM_CODE = "ASDFFDSA";

    @LocalServerPort
    int port;

    @SpyBean
    RedeemExporterNoop redeemExporterNoop;

    @Autowired
    RedeemCodeRepository redeemCodeRepository;

    @Autowired
    RedeemCodeUtils redeemCodeUtils;

    @Before
    public void setUp() {
        String url = "https://127.0.0.1:" + port + "/" + REDEEM_PATH;
        redeemCodeUtils.setUp(url);
        reset(redeemExporterNoop);
    }

    @Test
    public void noExportWhenFeatureFlagTurnedOff_returnOK() {
        redeemCodeUtils.prepareValidRedeemCode(REDEEM_CODE);

        ResponseEntity<RedeemCodeResponse> result = redeemCodeUtils.sendRedeemCodeGet(REDEEM_CODE);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);

        //export was triggered, but noop was called
        verify(redeemExporterNoop, times(1)).exportExceptionSafe();
    }

}