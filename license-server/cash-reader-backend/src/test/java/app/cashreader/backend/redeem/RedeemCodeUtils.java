package app.cashreader.backend.redeem;

import app.cashreader.backend.db.model.RedeemCode;
import app.cashreader.backend.db.repository.RedeemCodeRepository;
import app.cashreader.backend.redeem.api.v1.RedeemCodeResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import static app.cashreader.backend.redeem.api.v1.RedeemCodeController.*;

@Slf4j
@Component
public class RedeemCodeUtils {


    /**
     * If this is null, then you need to update your @SpringBootTest to start using webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
     * or other that provides TestRestTemplate
     */
    @Autowired(required = false)
    TestRestTemplate restTemplate;

    @Autowired
    RedeemCodeRepository redeemCodeRepository;

    String url;

    public void setUp(String url) {
        this.url = url;
    }

    public ResponseEntity<RedeemCodeResponse> sendRedeemCodeGet(String redeemCode) {
        UriComponents builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam(SETTINGS_ID, redeemCode)
                .queryParam(USER_ID, "Michal")
                .queryParam(PLATFORM, "iOS")
                .queryParam(NOTES, "iran")
                .queryParam(APP_VERSION, "1.67")
                .queryParam(CURRENCY, "eur")
                .queryParam(LANG, "cs")
                .queryParam(SYSTEM_VERSION, "18.0.1")
                .build();

        return sendRedeemCodeGet(builder);
    }

    public ResponseEntity<RedeemCodeResponse> sendRedeemCodeGet(UriComponents builder) {
        String uriString = builder.toUriString();
        log.info("uriString={}", uriString);
        try {
            return restTemplate.getForEntity(uriString, RedeemCodeResponse.class);
            // every fourth call fails on ResourceAccessException, don't know why
        } catch (ResourceAccessException e) {
            return restTemplate.getForEntity(uriString, RedeemCodeResponse.class);
        }
    }


    public void prepareValidRedeemCode(@SuppressWarnings("SameParameterValue") String redeemCode) {
        ensureCodeDoesntExistInDb(redeemCode);
        RedeemCode newRedeemCode = RedeemCode.newEntity(redeemCode);
        redeemCodeRepository.save(newRedeemCode);
    }

    public void ensureCodeDoesntExistInDb(String redeemCode) {
        RedeemCode oldRedeemCode = redeemCodeRepository.findByRedeemCode(redeemCode);
        if (oldRedeemCode != null) {
            redeemCodeRepository.delete(oldRedeemCode);
        }
    }
}
