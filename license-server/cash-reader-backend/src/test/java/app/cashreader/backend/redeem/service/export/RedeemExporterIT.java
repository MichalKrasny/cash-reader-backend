package app.cashreader.backend.redeem.service.export;

import app.cashreader.backend.db.repository.RedeemCodeRepository;
import app.cashreader.backend.redeem.RedeemCodeUtils;
import app.cashreader.backend.redeem.api.v1.RedeemCodeResponse;
import com.google.api.services.sheets.v4.Sheets;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static app.cashreader.backend.redeem.api.v1.RedeemCodeController.REDEEM_PATH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"test", "redeem-service-on"})
public class RedeemExporterIT {

    public static final String REDEEM_CODE = "ASDFFDSA";

    @LocalServerPort
    int port;

    @Autowired
    SheetsWriter sheetsWriterMock;

    @MockBean
    Sheets sheetsMock;

    @Autowired
    RedeemCodeRepository redeemCodeRepository;

    @Autowired
    RedeemCodeUtils redeemCodeUtils;

    @Before
    public void setUp() {
        String url = "https://127.0.0.1:" + port + "/" + REDEEM_PATH;
        redeemCodeUtils.setUp(url);
        reset(sheetsWriterMock);
        reset(sheetsMock);
    }

    @Test
    public void redeemServiceReturnsOK_exportIsPerformed() {
        redeemCodeUtils.prepareValidRedeemCode(REDEEM_CODE);

        ResponseEntity<RedeemCodeResponse> result = redeemCodeUtils.sendRedeemCodeGet(REDEEM_CODE);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);

        //export was triggered, but noop was called
        verify(sheetsWriterMock, times(1)).write(any());
    }


    @Test
    public void exportFails_butCodeIsGenerated() {
        redeemCodeUtils.prepareValidRedeemCode(REDEEM_CODE);

        doThrow(new RuntimeException("testException")).when(sheetsWriterMock).write(any());

        ResponseEntity<RedeemCodeResponse> result = redeemCodeUtils.sendRedeemCodeGet(REDEEM_CODE);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);

        //export was triggered, but noop was called
        verify(sheetsWriterMock, times(1)).write(any());
    }


    @Test
    public void redeemServiceReturnsNotOK_noExport() {
        redeemCodeUtils.ensureCodeDoesntExistInDb(REDEEM_CODE);

        ResponseEntity<RedeemCodeResponse> result = redeemCodeUtils.sendRedeemCodeGet(REDEEM_CODE);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        //export was triggered, but noop was called
        verify(sheetsWriterMock, times(0)).write(any());
    }


}