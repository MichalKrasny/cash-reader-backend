package app.cashreader.backend.license.api.v1;

import app.cashreader.backend.billing.api.v1.BundleTestUtils;
import app.cashreader.backend.db.model.Bundle;
import app.cashreader.backend.db.repository.BundleRepository;
import app.cashreader.backend.db.repository.LicenseRepository;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.ResourceAccessException;

import static app.cashreader.backend.billing.api.v1.BundleTestUtils.*;
import static app.cashreader.backend.license.api.v1.LicenseErrorResponse.LicenseErrorCode.*;
import static app.cashreader.backend.license.api.v1.LicenseRequestTestUtils.*;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;


@SuppressWarnings("ConstantConditions")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
@ActiveProfiles({"test"})
public class LicenseControllerIT {


    @Autowired
    private BundleTestUtils bundleTestUtils;

    @Autowired
    private BundleRepository bundleRepository;

    @Autowired
    private LicenseRepository licenseRepository;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate testRestTemplate;


    private String url;


    @Before
    public void setUp() {
        url = "https://127.0.0.1:" + port + "/api/v1/license";
    }

    @Test
    public void postForLicense_returnsLicense() {
        final LicenseResponse result = postForLicense(BUNDLE_ID, USER_ID, PLATFORM_IOS, DEVICE_ID);
        Assertions.assertThat(result.getLicenseId()).isNotNull();
    }

    @Test
    public void postForLicense_bundleIdNull() {
        final LicenseRequest request = new LicenseRequest(null, USER_ID, PLATFORM_IOS, DEVICE_ID);

        final ResponseEntity<LicenseErrorResponse> result = getLicenseErrorResponseResponseEntity(request);

        Assertions.assertThat(result.getStatusCode()).isEqualTo(BAD_REQUEST);
        Assertions.assertThat(result.getBody().getLicenseErrorCode()).isEqualTo(OTHER);
        Assertions.assertThat(result.getBody().getMessage()).contains("bundleId: must not be blank");
    }

    @Test
    public void postForLicense_everythingBlank() {
        final LicenseRequest request = new LicenseRequest(null, null, null, null);

        final ResponseEntity<LicenseErrorResponse> result = getLicenseErrorResponseResponseEntity(request);

        Assertions.assertThat(result.getStatusCode()).isEqualTo(BAD_REQUEST);
        Assertions.assertThat(result.getBody().getLicenseErrorCode()).isEqualTo(OTHER);
        Assertions.assertThat(result.getBody().getMessage())
                .contains("bundleId: must not be blank")
                .contains("platform: must not be blank")
                .contains("userId: must not be blank");
    }

    @Test
    public void postForLicenseTwice_returnsSameLicense() {
        final LicenseResponse result1 = postForLicense(BUNDLE_ID, USER_ID, PLATFORM_IOS, DEVICE_ID);
        final LicenseResponse result2 = postForLicense(BUNDLE_ID, USER_ID, PLATFORM_IOS, DEVICE_ID);
        Assertions.assertThat(result1.getLicenseId()).isEqualTo(result2.getLicenseId());
    }


    /**
     * tests period, when some applications are not updated and have null deviceId, then they are updated and start sending deviceId.
     */
    @Test
    public void postForLicense_nullDeviceIdThenDeviceId() {
        final LicenseResponse result1 = postForLicense(BUNDLE_ID, USER_ID, PLATFORM_IOS, null);
        final LicenseResponse result2 = postForLicense(BUNDLE_ID, USER_ID, PLATFORM_IOS, DEVICE_ID);
        Assertions.assertThat(result1.getLicenseId()).isEqualTo(result2.getLicenseId());
    }

    @Test
    public void postForSameBundleAndUserButDifferentPlatform_returnsSameLicense() {
        final LicenseResponse result1 = postForLicense(BUNDLE_ID, USER_ID, PLATFORM_IOS, DEVICE_ID);
        final LicenseResponse result2 = postForLicense(BUNDLE_ID, USER_ID, PLATFORM_ANDROID, DEVICE_ID);
        Assertions.assertThat(result1).isEqualTo(result2);
    }


    @Test
    @Ignore // TODO
    public void postForSameUserIdButDifferentBundle_returnsDifferentLicense() {
        final LicenseResponse result1 = postForLicense(BUNDLE_ID, USER_ID, PLATFORM_IOS, DEVICE_ID);
        final LicenseResponse result2 = postForLicense(BUNDLE_ID + "Different", USER_ID, PLATFORM_IOS, DEVICE_ID);
        Assertions.assertThat(result1).isNotEqualTo(result2);
    }

    @SuppressWarnings("SameParameterValue")
    private LicenseResponse postForLicense(final String bundleId, final String userId, final String platform, final String deviceId) {
        LicenseRequest request = new LicenseRequest(bundleId, userId, platform, deviceId);
        try {
            return this.testRestTemplate.postForObject(url, request, LicenseResponse.class);
            // every fourth call fails on ResourceAccessException when run in intellij, don't know why
        } catch (ResourceAccessException e) {
            return this.testRestTemplate.postForObject(url, request, LicenseResponse.class);
        }
    }

    @Test
    public void postForNonExistingBundle() {
        final LicenseRequest request = new LicenseRequest("NonExistingBundle", USER_ID, PLATFORM_IOS, DEVICE_ID);

        final ResponseEntity<LicenseErrorResponse> result = getLicenseErrorResponseResponseEntity(request);

        Assertions.assertThat(result.getStatusCode()).isEqualTo(BAD_REQUEST);
        Assertions.assertThat(result.getBody().getLicenseErrorCode()).isEqualTo(UNKNOWN_BUNDLE_ID);
        Assertions.assertThat(result.getBody().getMessage()).contains("Bundle not found, bundleId=NonExistingBundle");
    }


    @Test
    public void postForExpiredBundle() {
        final LicenseRequest request = new LicenseRequest(BUNDLE_ID_EXPIRED, USER_ID, PLATFORM_IOS, DEVICE_ID);

        final ResponseEntity<LicenseErrorResponse> result = getLicenseErrorResponseResponseEntity(request);

        Assertions.assertThat(result.getStatusCode()).isEqualTo(BAD_REQUEST);
        Assertions.assertThat(result.getBody().getLicenseErrorCode()).isEqualTo(EXPIRED_BUNDLE);
        Assertions.assertThat(result.getBody().getMessage()).contains("Bundle has already expired");
    }


    @Test
    public void postForLicense_LimitedBundleWithNoUsers() {
        deleteBundleAndLicenses(VALID_LIMITED_BUNLE_ID);
        final Bundle limitedBundleWithNoUsers = bundleTestUtils.getValidUnlimitedBundle(VALID_LIMITED_BUNLE_ID).withLimitedUserIds(true);
        bundleRepository.save(limitedBundleWithNoUsers);

        final LicenseRequest request = new LicenseRequest(VALID_LIMITED_BUNLE_ID, USER_ID, PLATFORM_IOS, DEVICE_ID);

        final ResponseEntity<LicenseErrorResponse> result = getLicenseErrorResponseResponseEntity(request);

        Assertions.assertThat(result.getStatusCode()).isEqualTo(BAD_REQUEST);
        Assertions.assertThat(result.getBody().getLicenseErrorCode()).isEqualTo(UNKNOWN_USER_ID);
        Assertions.assertThat(result.getBody().getMessage()).contains("Unknown user");
    }

    private ResponseEntity<LicenseErrorResponse> getLicenseErrorResponseResponseEntity(LicenseRequest request) {
        try {
            return this.testRestTemplate.postForEntity(url, request, LicenseErrorResponse.class);
            // every fourth call fails on ResourceAccessException when run in intellij, don't know why
        } catch (ResourceAccessException e) {
            return this.testRestTemplate.postForEntity(url, request, LicenseErrorResponse.class);
        }
    }


    @Test
    public void postForLicense_LimitedBundleWithSomeUsersButUserIsNotAmongThem() {
        deleteBundleAndLicenses(VALID_LIMITED_BUNLE_ID);
        final Bundle limitedBundleUser = bundleTestUtils.getValidUnlimitedBundle(VALID_LIMITED_BUNLE_ID)
                .withLimitedUserIds(true)
                .withUserStoreIds(Sets.newHashSet("knownUser"));
        bundleRepository.save(limitedBundleUser);

        final LicenseRequest request = new LicenseRequest(VALID_LIMITED_BUNLE_ID, "unknownUser", PLATFORM_IOS, DEVICE_ID);

        final ResponseEntity<LicenseErrorResponse> result = getLicenseErrorResponseResponseEntity(request);

        Assertions.assertThat(result.getStatusCode()).isEqualTo(BAD_REQUEST);
        Assertions.assertThat(result.getBody().getLicenseErrorCode()).isEqualTo(UNKNOWN_USER_ID);
        Assertions.assertThat(result.getBody().getMessage()).contains("Unknown user");
    }


    @Test
    public void postForLicense_LimitedBundleWithSomeUsersAndUserIsAmongThem() {
        deleteBundleAndLicenses(VALID_LIMITED_BUNLE_ID);
        final Bundle limitedBundleUser = bundleTestUtils.getValidUnlimitedBundle(VALID_LIMITED_BUNLE_ID)
                .withLimitedUserIds(true)
                .withUserStoreIds(Sets.newHashSet("knownUser"));
        bundleRepository.save(limitedBundleUser);

        final LicenseRequest request = new LicenseRequest(VALID_LIMITED_BUNLE_ID, "knownUser", PLATFORM_IOS, DEVICE_ID);

        final ResponseEntity<LicenseResponse> result = this.testRestTemplate.postForEntity(url, request, LicenseResponse.class);

        Assertions.assertThat(result.getStatusCode()).isEqualTo(OK);
        Assertions.assertThat(result.getBody().getLicenseId()).isNotNull();
    }

    private void deleteBundleAndLicenses(final String bundleStoreId) {
        final Bundle bundle = bundleRepository.findByBundleStoreId(bundleStoreId);
        if (bundle != null) {
            licenseRepository.deleteByBundleId(bundle.getId());
        }
        bundleRepository.deleteByBundleStoreId(bundleStoreId);
    }
}
