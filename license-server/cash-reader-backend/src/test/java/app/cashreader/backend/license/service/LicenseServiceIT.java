package app.cashreader.backend.license.service;

import app.cashreader.backend.bundle.service.BundleExpiredException;
import app.cashreader.backend.bundle.service.UnknownBundleIdException;
import app.cashreader.backend.db.model.License;
import app.cashreader.backend.db.model.LicenseHistory;
import app.cashreader.backend.db.repository.LicenseRepository;
import app.cashreader.backend.license.api.v1.LicenseRequest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static app.cashreader.backend.billing.api.v1.BundleTestUtils.BUNDLE_ID_EXPIRED;
import static app.cashreader.backend.license.api.v1.LicenseRequestTestUtils.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles({"test"})
public class LicenseServiceIT {

    @Autowired
    private LicenseService tested;

    @Autowired
    private LicenseRepository licenseRepository;

    @Test
    public void getLicense_returnsLicense() {
        final UUID result = tested.getLicense(getLicenseRequestUnique());
        final License license1 = licenseRepository.findByLicenseNumber(result);
        assertThat(license1.getHistory()).hasSize(1);
    }

    @Test
    public void getLicenseTwice_returnsSameLicense_historyGrows() {
        final LicenseRequest licenseRequest = getLicenseRequestUnique();

        final UUID result1 = tested.getLicense(licenseRequest);
        final License license1 = licenseRepository.findByLicenseNumber(result1);

        final UUID result2 = tested.getLicense(licenseRequest);

        assertThat(result1).isEqualTo(result2);

        final License license2 = licenseRepository.findByLicenseNumber(result1);
        final List<LicenseHistory> history1 = license1.getHistory();
        final List<LicenseHistory> history2 = license2.getHistory();

        assertThat(history1).hasSize(1);
        assertThat(history2).hasSize(2);

        assertThat(toIds(history2)).containsAll(toIds(history1));
    }

    private List<Long> toIds(List<LicenseHistory> history2) {
        return history2.stream().map(LicenseHistory::getId).collect(Collectors.toList());
    }

    @Test
    public void getLicense_LicenseHistorySavedCorrectly_nullDeviceId() {
        final LicenseRequest licenseRequest = getLicenseRequestUnique();
        final UUID licenseNumber = tested.getLicense(licenseRequest.withDeviceId(null));

        final License license = licenseRepository.findByLicenseNumber(licenseNumber);
        final List<LicenseHistory> history = license.getHistory();
        assertThat(history).hasSize(1);
        assertThat(history.get(0).getDeviceId()).isNull();
        assertThat(history.get(0).getPlatform()).isEqualTo(PLATFORM_IOS);
    }


    @Test
    public void getLicense_differentDeviceIdsAppearInHistory() {
        final LicenseRequest licenseRequest = getLicenseRequestUnique();

        final UUID licenseNumberNullDeviceId = tested.getLicense(licenseRequest.withDeviceId(null));
        final License licenseNullDeviceId = licenseRepository.findByLicenseNumber(licenseNumberNullDeviceId);

        final UUID licenseNumberDeviceId = tested.getLicense(licenseRequest.withDeviceId(DEVICE_ID));
        final License licenseDeviceId = licenseRepository.findByLicenseNumber(licenseNumberDeviceId);

        final UUID licenseNumberAnotherDeviceId = tested.getLicense(licenseRequest.withDeviceId("anotherDeviceId"));
        final License licenseAnotherDeviceId = licenseRepository.findByLicenseNumber(licenseNumberAnotherDeviceId);

        assertThat(licenseNumberNullDeviceId).isEqualTo(licenseNumberDeviceId).isEqualTo(licenseNumberAnotherDeviceId);


        final List<LicenseHistory> historyNullDeviceId = licenseNullDeviceId.getHistory();
        final List<LicenseHistory> historyDeviceId = licenseDeviceId.getHistory();
        final List<LicenseHistory> historyAnotherDeviceId = licenseAnotherDeviceId.getHistory();

        assertThat(historyNullDeviceId).hasSize(1);
        assertThat(historyDeviceId).hasSize(2);
        assertThat(historyAnotherDeviceId).hasSize(3);

        assertThat(toIds(historyDeviceId)).containsAll(toIds(historyNullDeviceId));
        assertThat(toIds(historyAnotherDeviceId)).containsAll(toIds(historyDeviceId));
    }

    @Test
    public void getLicense_unknownBundle() {
        final LicenseRequest licenseRequest = getLicenseRequestUnique().withBundleId("NonExistingBundleId");
        assertThatThrownBy(() -> tested.getLicense(licenseRequest)).isOfAnyClassIn(UnknownBundleIdException.class);
    }

    @Test
    public void getLicense_expiredBundle() {
        final LicenseRequest licenseRequest = getLicenseRequestUnique().withBundleId(BUNDLE_ID_EXPIRED);
        assertThatThrownBy(() -> tested.getLicense(licenseRequest)).isOfAnyClassIn(BundleExpiredException.class);
    }
}
