package app.cashreader.backend.db.repository;

import app.cashreader.backend.billing.api.v1.BundleTestUtils;
import app.cashreader.backend.db.model.Bundle;
import com.google.common.collect.Sets;
import junit.framework.TestCase;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Slf4j
@Transactional
@ActiveProfiles({"test"})
public class BundleRepositoryTest extends TestCase {

    public static final String BUNDLE_ID_1 = "bundleId1";
    public static final String BUNDLE_ID_2 = "bundleId2";
    @Autowired
    private BundleRepository tested;

    @Autowired
    private BundleTestUtils bundleTestUtils;

    @Test
    public void whenTwoLimitedBundlesHaveSameUsers_WillWork() {
        tested.deleteByBundleStoreId(BUNDLE_ID_1);
        tested.deleteByBundleStoreId(BUNDLE_ID_2);

        final Bundle bundle1 = bundleTestUtils.getValidUnlimitedBundle(BUNDLE_ID_1)
                .withLimitedUserIds(true).withUserStoreIds(Sets.newHashSet("userId"));
        tested.save(bundle1);
        final Bundle bundle2 = bundleTestUtils.getValidUnlimitedBundle(BUNDLE_ID_2)
                .withLimitedUserIds(true).withUserStoreIds(Sets.newHashSet("userId"));
        tested.save(bundle2);

        Assertions.assertThat(tested.countAllByBundleStoreId(BUNDLE_ID_1)).isEqualTo(1);
        Assertions.assertThat(tested.countAllByBundleStoreId(BUNDLE_ID_2)).isEqualTo(1);
    }
}