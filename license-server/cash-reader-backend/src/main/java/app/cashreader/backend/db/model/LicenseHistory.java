package app.cashreader.backend.db.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "license_history")
@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
@NoArgsConstructor
public class LicenseHistory extends AuditModel {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String platform;

    @Column
    private String deviceId;

    @ManyToOne
    @JoinColumn(name = "fk_license_id", nullable = false)
    private License license;

    public String toString() {
        return "LicenseHistory{" +
               "id=" + id +
               ", platform='" + platform + "'" +
               ", deviceId='" + deviceId + "'" +
               ", licenseId=" + ((license == null) ? "null" : license.getId()) +
               '}';
    }
}
