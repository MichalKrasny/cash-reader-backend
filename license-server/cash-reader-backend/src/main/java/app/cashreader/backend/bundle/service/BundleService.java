package app.cashreader.backend.bundle.service;

import app.cashreader.backend.bundle.api.v1.BundleIsLicensedRequest;
import app.cashreader.backend.bundle.api.v1.BundleIsLicensedResponse;
import app.cashreader.backend.db.model.Bundle;
import app.cashreader.backend.db.repository.BundleRepository;
import app.cashreader.backend.logging.methods.LoggedIO;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

import static app.cashreader.backend.bundle.api.v1.BundleStatus.*;
import static app.cashreader.backend.logging.methods.LoggedIO.Level.DEBUG;
import static java.time.ZonedDateTime.now;

@Slf4j
@Service
@LoggedIO(input = DEBUG, result = DEBUG)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
// todo find a way, how not to leak API to Service and what to do with tests
public class BundleService {

    private final BundleRepository bundleRepository;

    public BundleIsLicensedResponse isValid(BundleIsLicensedRequest bundleIsLicensedRequest) {
        final String bundleStoreId = bundleIsLicensedRequest.getBundleId();
        final Bundle bundle = bundleRepository.findByBundleStoreId(bundleStoreId);
        if (bundle == null) {
            return new BundleIsLicensedResponse(UNKNOWN_BUNDLE_ID);
        }

        if (isExpired(bundle, ZonedDateTime.now())) {
            return new BundleIsLicensedResponse(EXPIRED);
        }

        return new BundleIsLicensedResponse(VALID);
    }

    private boolean isExpired(Bundle bundle, ZonedDateTime now) {
        return bundle.getValidTo() != null && bundle.getValidTo().isBefore(now);
    }

    /**
     * @param bundleStoreId Can not be null.
     * @return Bundle entity that is not expired.
     * @throws UnknownBundleIdException when bundle ID is not known
     * @throws BundleExpiredException   when bundle ID is not valid
     */
    public Bundle getValidBundle(@NonNull String bundleStoreId) {
        final Bundle bundle = getBundleOrException(bundleStoreId);

        if (isExpired(bundle, now())) {
            throw new BundleExpiredException("Bundle has already expired, bundleId=" + bundle.getBundleStoreId() + ", validTo=" + bundle.getValidTo());
        }
        return bundle;
    }


    /**
     * @param bundleStoreId
     * @return Bundle entity
     * @throws UnknownBundleIdException when bundle ID is not known
     */
    private Bundle getBundleOrException(final @NonNull String bundleStoreId) {
        final Bundle bundle = bundleRepository.findByBundleStoreId((bundleStoreId));
        if (bundle == null) {
            throw new UnknownBundleIdException("Bundle not found, bundleId=" + bundleStoreId);
        }
        return bundle;
    }

    /**
     * @param bundleStoreId
     * @throws UnknownBundleIdException when bundle ID does not exist
     */
    public void verifyBudleExists(final @NonNull String bundleStoreId) {
        final int count = bundleRepository.countAllByBundleStoreId(bundleStoreId);
        if (count == 0) {
            throw new UnknownBundleIdException("Bundle doesn't exist, bundleId=" + bundleStoreId);
        }
    }

}
