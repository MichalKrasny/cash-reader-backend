package app.cashreader.backend.db.model;

import java.time.ZonedDateTime;
import java.util.Set;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Wither;

@Entity(name = "bundle")
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Wither
public class Bundle extends AuditModel {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "bundle_store_id", nullable = false)
    private String bundleStoreId;

    @Column(name = "valid_to")
    private ZonedDateTime validTo;

    @Column(name = "limited_user_ids")
    private boolean limitedUserIds;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name= "limited_user", joinColumns = {@JoinColumn(name = "fk_bundle_id", referencedColumnName = "id")})
    @Column(name="user_store_id")
    private Set<String> userStoreIds;
}
