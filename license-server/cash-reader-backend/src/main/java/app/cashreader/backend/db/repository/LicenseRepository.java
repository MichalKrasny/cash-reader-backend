package app.cashreader.backend.db.repository;

import app.cashreader.backend.db.model.License;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Repository
public interface LicenseRepository extends JpaRepository<License, Long> {

    License findByBundleIdAndUserId(long bundleId, String userId);

    @Transactional
    void deleteByBundleId(long bundleId);

    License findByLicenseNumber(UUID licenseNumber);

    @Query(value = "Select l.* " +
            " from license l," +
            "     bundle b" +
            " where l.fk_bundle_id = b.id " +
            " and b.bundle_store_id = :bundleStoreId " +
            " and EXTRACT (YEAR from l.billing_date) = :year " +
            " and EXTRACT (MONTH from l.billing_date) = :month", nativeQuery = true)
    List<License> findByBundleStoreIdAndYearAndMonth(final String bundleStoreId, final int year, final int month);
}
