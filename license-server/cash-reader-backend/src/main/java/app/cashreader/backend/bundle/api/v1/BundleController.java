
package app.cashreader.backend.bundle.api.v1;

import static app.cashreader.backend.logging.methods.LoggedIO.Level.INFO;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import javax.validation.Valid;

import app.cashreader.backend.license.api.v1.LicenseErrorResponse;
import app.cashreader.backend.license.api.v1.LicenseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import app.cashreader.backend.bundle.service.BundleService;
import app.cashreader.backend.logging.methods.LoggedIO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("api/v1/bundle/validator")
@Slf4j
@LoggedIO(input = INFO, result = INFO)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BundleController {

    private final BundleService bundleService;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @Operation(summary = "Check, if bundle is valid at the moment.", tags = "Bundle Endpoint", description = "Bundle related operations.",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "State of the bundle.",
                            content = {@Content(schema = @Schema(implementation = BundleIsLicensedResponse.class))}
                    )
            }
    )
    @LoggedIO(input = INFO, result = INFO)
    public BundleIsLicensedResponse isValid(@Valid @RequestBody BundleIsLicensedRequest bundleIsLicensedRequest) {
        return bundleService.isValid(bundleIsLicensedRequest);
    }
}
