package app.cashreader.backend.billing.api.v1;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Data
public class BillingRequestForMonth {

    @Schema(example = "cz.hayaku.cashreader", required = true)
    private final String bundleId;

    @Schema(example = "2020", required = true)
    private final int year;

    @Schema(description = "Month of the report, JAN=1, FEB=2, etc..", example = "3", required = true)
    private final int month;

    @Schema(description = "Fee per license", example = "9.5", required = true)

    private final double fee;


    @Schema(type= "List", description = "fdsa", example = "[\"4f3fcedb\", \"a8cc918f\"]")
    private final List<String> freeOfChargeUserIds;


    /**
     * @param bundleId - bundleStoreId, not a database id.
     * @param year     - i.e. 2020
     * @param month    - January is 1
     * @param fee      -
     */
    @JsonCreator
    public BillingRequestForMonth(
            @NotBlank @JsonProperty(value = "bundleId", required = true) final String bundleId,
            @NotBlank @JsonProperty(value = "year", required = true) final int year,
            @NotBlank @JsonProperty(value = "month", required = true) final int month,
            @JsonProperty(value = "fee", required = true) final double fee,
            @JsonProperty(value = "freeOfChargeUserIds", required = true) final List<String> freeOfChargeUserIds) {
        this.bundleId = bundleId;
        this.year = year;
        this.month = month;
        this.fee = fee;
        this.freeOfChargeUserIds = freeOfChargeUserIds == null ? new ArrayList<>() : freeOfChargeUserIds;
    }
}
