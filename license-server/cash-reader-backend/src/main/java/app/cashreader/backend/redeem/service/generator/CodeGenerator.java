package app.cashreader.backend.redeem.service.generator;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;

/**
 * Generates a redeem code value.
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class CodeGenerator {

    SecureRandom random = new SecureRandom();

    public String get(String chars, int codeLength) {
        StringBuilder codeBuilder = new StringBuilder(codeLength);
        for (int j = 0; j < codeLength; j++) {
            int index = random.nextInt(chars.length());
            codeBuilder.append(chars.charAt(index));
        }
        return codeBuilder.toString();
    }
}
