package app.cashreader.backend.logging.http;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import io.micrometer.core.instrument.util.IOUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class LoggingService {

    public void logRequest(HttpServletRequest httpServletRequest, Object body) {
        log.debug("httpServletRequest={}\nbody={}", httpServletRequest, body);
    }

    public void logResponse(HttpServletRequest httpServletRequest,
                HttpServletResponse httpServletResponse,
                Object body) {
        log.debug("httpServletRequest={}\nhttpServletResponse={}\nbody={}", httpServletRequest, httpServletResponse, body);
    }
}
