package app.cashreader.backend.redeem.service.cloudflare.kv;

import app.cashreader.backend.redeem.service.generator.CodeGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Generates a kv key.
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class KvKeyGenerator {

    public static final int CODE_LENGTH = 8;
    public static final String CHARS = "abcdefghijklmnopqrstuvwxyz0123456789";

    private final CodeGenerator codeGenerator;

    public String get() {
        return codeGenerator.get(CHARS, CODE_LENGTH);
    }
}
