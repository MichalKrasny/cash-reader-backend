package app.cashreader.backend.db.repository;

import app.cashreader.backend.db.model.Bundle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface BundleRepository extends JpaRepository<Bundle, Long> {

    Bundle findByBundleStoreId(String bundleStoreId);

    int countAllByBundleStoreId(String bundleStoreId);

    @Transactional
    void deleteByBundleStoreId(String bundleStoreId);
}
