package app.cashreader.backend.logging.http.response;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ReadableServletOutputStream extends ServletOutputStream {

    private ServletOutputStream original;
    private ByteArrayOutputStream copy;

    public ReadableServletOutputStream(ServletOutputStream original) {
        this.original = original;
        this.copy = new ByteArrayOutputStream();
    }

    @Override
    public void write(int b) throws IOException {
        original.write(b);
        copy.write(b);
    }

    public byte[] getCopy() {
        return copy.toByteArray();
    }

    @Override
    public boolean isReady() {
        //ByteArrayOutputStream is always ready
        return original.isReady();
    }

    @Override
    public void setWriteListener(final WriteListener listener) {
        throw new RuntimeException("not implemented");
    }
}