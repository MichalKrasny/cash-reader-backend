package app.cashreader.backend.logging.http.request;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Implementation of ServletInputStream for usage in HTTP Filters.
 */
public class ByteArrayServletInputStream extends ServletInputStream {

    private ByteArrayInputStream inputStream;

    public ByteArrayServletInputStream(final byte[] bytes) {
        this.inputStream = new ByteArrayInputStream(bytes);
    }

    @Override
    public boolean isFinished() {
        return inputStream.available() == 0;
    }

    @Override
    public boolean isReady() {
        return true;
    }

    @Override
    public void setReadListener(final ReadListener listener) {
        throw new RuntimeException("setReadListener is not implemented");
    }

    @SuppressWarnings("RedundantThrows")
    @Override
    public int read() throws IOException {
        return inputStream.read();
    }
}
