package app.cashreader.backend.bundle.api.v1;

import javax.validation.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BundleIsLicensedRequest {

    @JsonProperty(value = "bundleId", required = true)
    @NotBlank
    private final String bundleId;

    //todo test for nonblankiness
    @JsonCreator
    public BundleIsLicensedRequest(@NotBlank String bundleId) {
        this.bundleId = bundleId;
    }
}
