package app.cashreader.backend.redeem.api.v1;

import app.cashreader.backend.logging.methods.LoggedIO;
import app.cashreader.backend.redeem.service.GenerateRedeemCodesRequest;
import app.cashreader.backend.redeem.service.RedeemService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static app.cashreader.backend.logging.methods.LoggedIO.Level.INFO;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Slf4j
@LoggedIO(input = INFO, result = INFO)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping(GenerateRedeemCodesController.GENERATE_REDEEM_CODE_PATH)
public class GenerateRedeemCodesController {

    /**
     * Named settingsValidator to disguise.
     */
    public static final String GENERATE_REDEEM_CODE_PATH = "api/v1/generate";

    private final RedeemService redeemService;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @LoggedIO(input = INFO, result = INFO)
    public ResponseEntity<Object> generate(@RequestBody GenerateRedeemCodesRequest generateRedeemCodesRequest) {
        try {
            redeemService.generateNewRedeemCodes(generateRedeemCodesRequest);
            return new ResponseEntity<>(OK);
        } catch (Exception e) {
            log.error("Redeem generation error: " + e.getMessage(), e);
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
    }
}
