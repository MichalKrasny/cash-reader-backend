package app.cashreader.backend.bundle.service;

public class BundleExpiredException extends RuntimeException {

    public BundleExpiredException() {
        super();
    }

    public BundleExpiredException(String message) {
        super(message);
    }

    public BundleExpiredException(String message, Throwable cause) {
        super(message, cause);
    }

    public BundleExpiredException(Throwable cause) {
        super(cause);
    }

    protected BundleExpiredException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
