package app.cashreader.backend.db.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.ZonedDateTime;

/**
 * This class is not meant to be created programmatically, it should be inserted by migration script and then
 * only updated when redeemed.
 */
@Entity(name = "redeem_code")
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class RedeemCode extends AuditModel {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "redeem_code", nullable = false)
    private String redeemCode;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "platform")
    private String platform;

    @Column(name = "campaign")
    private String campaign;

    @Column(name = "appVersion")
    private String appVersion;

    @Column(name = "currency")
    private String currency;

    @Column(name = "lang")
    private String lang;

    @Column(name = "systemVersion")
    private String systemVersion;

    @Column(name = "redeem_date")
    private ZonedDateTime redeemDate;

    /**
     * Field is not editable by claiming redeem code, it should be set during code generation
     */
    @Column(name = "notes")
    private String notes;


    /**
     * Field is not editable by claiming redeem code, it should be set during code generation
     */
    @Column(name = "kv_key")
    private String kvKey;


    @Column(name = "device_model")
    private String deviceModel;


    public static RedeemCode newEntity(String redeemCode) {
        return new RedeemCode(null,
                redeemCode,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null);
    }
}
