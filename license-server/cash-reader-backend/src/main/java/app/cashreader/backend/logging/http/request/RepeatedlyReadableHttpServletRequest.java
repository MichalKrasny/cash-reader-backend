package app.cashreader.backend.logging.http.request;

import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;

/**
 * This class makes request input stream of HTTP body (ServletInputStream) repeatedly readable by creating byte[] in memory and opening input stream anytime is needed.
 * This is required, when you need to log the message for example.
 */
@Slf4j
public class RepeatedlyReadableHttpServletRequest implements HttpServletRequest {


    private final HttpServletRequest httpServletRequest;

    private final byte[] requestBody;

    /**
     * You should use {@link RepeatedlyReadableHttpServletRequest#from(ServletRequest)} instead.
     */
    public RepeatedlyReadableHttpServletRequest(final HttpServletRequest httpServletRequest) {
        try {
            this.httpServletRequest = httpServletRequest;

            final ServletInputStream inputStream = httpServletRequest.getInputStream();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            IOUtils.copy(inputStream, byteArrayOutputStream);
            requestBody = byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }

    /**
     * @param request
     * @return if request is instance of RepeatedlyReadableHttpServletRequest, then will return the instance of request, otherwise return new instance wrapping the old one
     */
    public static RepeatedlyReadableHttpServletRequest from(final ServletRequest request) {
        return request instanceof RepeatedlyReadableHttpServletRequest ? (RepeatedlyReadableHttpServletRequest) request : new RepeatedlyReadableHttpServletRequest((HttpServletRequest) request);
    }

    /**
     * Returns body as String without destroying input stream. May be called multiple times.
     * Reques character set is used as encoding.
     *
     * @return Copy of internal InputStream as String.
     */
    public String getPayloadAsString() {
        try {
            final String characterEncoding = httpServletRequest.getCharacterEncoding() == null ? "UTF-8" : httpServletRequest.getCharacterEncoding();
            return new String(requestBody, characterEncoding);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Get ServletInputStream multiple times.
     *
     * @return Copy of internal ServletInputStream.
     */
    @Override
    public ServletInputStream getInputStream() {
        return new ByteArrayServletInputStream(requestBody);
    }


    /**
     * Get reader multiple times.
     *
     * @return Copy of internal reader.
     * @throws IOException
     */
    @Override
    public BufferedReader getReader() throws IOException {
        return httpServletRequest.getReader();
    }


    //*********************** generated unmodified methods *************************

    @Override
    public String getAuthType() {
        return httpServletRequest.getAuthType();
    }

    @Override
    public Cookie[] getCookies() {
        return httpServletRequest.getCookies();
    }

    @Override
    public long getDateHeader(final String name) {
        return httpServletRequest.getDateHeader(name);
    }

    @Override
    public String getHeader(final String name) {
        return httpServletRequest.getHeader(name);
    }

    @Override
    public Enumeration<String> getHeaders(final String name) {
        return httpServletRequest.getHeaders(name);
    }

    @Override
    public Enumeration<String> getHeaderNames() {
        return httpServletRequest.getHeaderNames();
    }

    @Override
    public int getIntHeader(final String name) {
        return httpServletRequest.getIntHeader(name);
    }

    @Override
    public HttpServletMapping getHttpServletMapping() {
        return httpServletRequest.getHttpServletMapping();
    }

    @Override
    public String getMethod() {
        return httpServletRequest.getMethod();
    }

    @Override
    public String getPathInfo() {
        return httpServletRequest.getPathInfo();
    }

    @Override
    public String getPathTranslated() {
        return httpServletRequest.getPathTranslated();
    }

    @Override
    public PushBuilder newPushBuilder() {
        return httpServletRequest.newPushBuilder();
    }

    @Override
    public String getContextPath() {
        return httpServletRequest.getContextPath();
    }

    @Override
    public String getQueryString() {
        return httpServletRequest.getQueryString();
    }

    @Override
    public String getRemoteUser() {
        return httpServletRequest.getRemoteUser();
    }

    @Override
    public boolean isUserInRole(final String role) {
        return httpServletRequest.isUserInRole(role);
    }

    @Override
    public Principal getUserPrincipal() {
        return httpServletRequest.getUserPrincipal();
    }

    @Override
    public String getRequestedSessionId() {
        return httpServletRequest.getRequestedSessionId();
    }

    @Override
    public String getRequestURI() {
        return httpServletRequest.getRequestURI();
    }

    @Override
    public StringBuffer getRequestURL() {
        return httpServletRequest.getRequestURL();
    }

    @Override
    public String getServletPath() {
        return httpServletRequest.getServletPath();
    }

    @Override
    public HttpSession getSession(final boolean create) {
        return httpServletRequest.getSession(create);
    }

    @Override
    public HttpSession getSession() {
        return httpServletRequest.getSession();
    }

    @Override
    public String changeSessionId() {
        return httpServletRequest.changeSessionId();
    }

    @Override
    public boolean isRequestedSessionIdValid() {
        return httpServletRequest.isRequestedSessionIdValid();
    }

    @Override
    public boolean isRequestedSessionIdFromCookie() {
        return httpServletRequest.isRequestedSessionIdFromCookie();
    }

    @Override
    public boolean isRequestedSessionIdFromURL() {
        return httpServletRequest.isRequestedSessionIdFromURL();
    }

    @Override
    @Deprecated
    public boolean isRequestedSessionIdFromUrl() {
        return httpServletRequest.isRequestedSessionIdFromUrl();
    }

    @Override
    public boolean authenticate(final HttpServletResponse response) throws IOException, ServletException {
        return httpServletRequest.authenticate(response);
    }

    @Override
    public void login(final String username, final String password) throws ServletException {
        httpServletRequest.login(username, password);
    }

    @Override
    public void logout() throws ServletException {
        httpServletRequest.logout();
    }

    @Override
    public Collection<Part> getParts() throws IOException, ServletException {
        return httpServletRequest.getParts();
    }

    @Override
    public Part getPart(final String name) throws IOException, ServletException {
        return httpServletRequest.getPart(name);
    }

    @Override
    public <T extends HttpUpgradeHandler> T upgrade(final Class<T> httpUpgradeHandlerClass) throws IOException, ServletException {
        return httpServletRequest.upgrade(httpUpgradeHandlerClass);
    }

    @Override
    public Map<String, String> getTrailerFields() {
        return httpServletRequest.getTrailerFields();
    }

    @Override
    public boolean isTrailerFieldsReady() {
        return httpServletRequest.isTrailerFieldsReady();
    }

    @Override
    public Object getAttribute(final String name) {
        return httpServletRequest.getAttribute(name);
    }

    @Override
    public Enumeration<String> getAttributeNames() {
        return httpServletRequest.getAttributeNames();
    }

    @Override
    public String getCharacterEncoding() {
        return httpServletRequest.getCharacterEncoding();
    }

    @Override
    public void setCharacterEncoding(final String env) throws UnsupportedEncodingException {
        httpServletRequest.setCharacterEncoding(env);
    }

    @Override
    public int getContentLength() {
        return httpServletRequest.getContentLength();
    }

    @Override
    public long getContentLengthLong() {
        return httpServletRequest.getContentLengthLong();
    }

    @Override
    public String getContentType() {
        return httpServletRequest.getContentType();
    }


    @Override
    public String getParameter(final String name) {
        return httpServletRequest.getParameter(name);
    }

    @Override
    public Enumeration<String> getParameterNames() {
        return httpServletRequest.getParameterNames();
    }

    @Override
    public String[] getParameterValues(final String name) {
        return httpServletRequest.getParameterValues(name);
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return httpServletRequest.getParameterMap();
    }

    @Override
    public String getProtocol() {
        return httpServletRequest.getProtocol();
    }

    @Override
    public String getScheme() {
        return httpServletRequest.getScheme();
    }

    @Override
    public String getServerName() {
        return httpServletRequest.getServerName();
    }

    @Override
    public int getServerPort() {
        return httpServletRequest.getServerPort();
    }

    @Override
    public String getRemoteAddr() {
        return httpServletRequest.getRemoteAddr();
    }

    @Override
    public String getRemoteHost() {
        return httpServletRequest.getRemoteHost();
    }

    @Override
    public void setAttribute(final String name, final Object o) {
        httpServletRequest.setAttribute(name, o);
    }

    @Override
    public void removeAttribute(final String name) {
        httpServletRequest.removeAttribute(name);
    }

    @Override
    public Locale getLocale() {
        return httpServletRequest.getLocale();
    }

    @Override
    public Enumeration<Locale> getLocales() {
        return httpServletRequest.getLocales();
    }

    @Override
    public boolean isSecure() {
        return httpServletRequest.isSecure();
    }

    @Override
    public RequestDispatcher getRequestDispatcher(final String path) {
        return httpServletRequest.getRequestDispatcher(path);
    }

    @Override
    @Deprecated
    public String getRealPath(final String path) {
        return httpServletRequest.getRealPath(path);
    }

    @Override
    public int getRemotePort() {
        return httpServletRequest.getRemotePort();
    }

    @Override
    public String getLocalName() {
        return httpServletRequest.getLocalName();
    }

    @Override
    public String getLocalAddr() {
        return httpServletRequest.getLocalAddr();
    }

    @Override
    public int getLocalPort() {
        return httpServletRequest.getLocalPort();
    }

    @Override
    public ServletContext getServletContext() {
        return httpServletRequest.getServletContext();
    }

    @Override
    public AsyncContext startAsync() throws IllegalStateException {
        return httpServletRequest.startAsync();
    }

    @Override
    public AsyncContext startAsync(final ServletRequest servletRequest, final ServletResponse servletResponse) throws IllegalStateException {
        return httpServletRequest.startAsync(servletRequest, servletResponse);
    }

    @Override
    public boolean isAsyncStarted() {
        return httpServletRequest.isAsyncStarted();
    }

    @Override
    public boolean isAsyncSupported() {
        return httpServletRequest.isAsyncSupported();
    }

    @Override
    public AsyncContext getAsyncContext() {
        return httpServletRequest.getAsyncContext();
    }

    @Override
    public DispatcherType getDispatcherType() {
        return httpServletRequest.getDispatcherType();
    }


}
