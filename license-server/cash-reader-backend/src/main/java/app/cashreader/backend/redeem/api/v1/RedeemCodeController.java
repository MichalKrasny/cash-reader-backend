package app.cashreader.backend.redeem.api.v1;

import app.cashreader.backend.logging.methods.LoggedIO;
import app.cashreader.backend.redeem.service.RedeemCodeStatus;
import app.cashreader.backend.redeem.service.RedeemService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static app.cashreader.backend.logging.methods.LoggedIO.Level.INFO;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Slf4j
@LoggedIO(input = INFO, result = INFO)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping(RedeemCodeController.REDEEM_PATH)
public class RedeemCodeController {

    /**
     * Named settingsValidator to disguise.
     */
    public static final String REDEEM_PATH = "api/v1/settings";

    /**
     * Named settings ID to disguise.
     */
    public static final String SETTINGS_ID = "settingsId";
    public static final String USER_ID = "userId";
    public static final String PLATFORM = "platform";
    public static final String NOTES = "notes";
    public static final String APP_VERSION = "appVersion";
    public static final String CURRENCY = "currency";
    public static final String LANG = "lang";
    public static final String SYSTEM_VERSION = "systemVersion";
    public static final String DEVICE_MODEL = "deviceModel";
    private final RedeemService redeemService;

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @LoggedIO(input = INFO, result = INFO)
    public ResponseEntity<RedeemCodeResponse> redeem(@RequestParam(name = SETTINGS_ID) String redeemCode,
                                                     @RequestParam(name = USER_ID, required = false) String userId,
                                                     @RequestParam(name = PLATFORM, required = false) String platform,
                                                     @RequestParam(name = NOTES, required = false) String campaign,
                                                     @RequestParam(name = APP_VERSION, required = false) String appVersion,
                                                     @RequestParam(name = CURRENCY, required = false) String currency,
                                                     @RequestParam(name = LANG, required = false) String lang,
                                                     @RequestParam(name = SYSTEM_VERSION, required = false) String systemVersion,
                                                     @RequestParam(name = DEVICE_MODEL, required = false) String deviceModel
    ) {
        try {
            RedeemCodeStatus redeemCodeStatus = redeemService.redeem(
                    redeemCode,
                    userId,
                    platform,
                    campaign,
                    appVersion,
                    currency,
                    lang,
                    systemVersion,
                    deviceModel
            );

            if (redeemCodeStatus == RedeemCodeStatus.VALID) {
                return new ResponseEntity<>(new RedeemCodeResponse(redeemCodeStatus), OK);
            } else {
                return new ResponseEntity<>(new RedeemCodeResponse(redeemCodeStatus), BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("Redeem error: " + e.getMessage(), e);
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
    }
}
