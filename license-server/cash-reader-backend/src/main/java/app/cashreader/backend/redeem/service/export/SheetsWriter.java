package app.cashreader.backend.redeem.service.export;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Writes data to a Google sheet.
 */
@Component
@Slf4j
@ConditionalOnProperty(name = "redeem.service.export.enabled", havingValue = "true")
public class SheetsWriter {

    public static final String RANGE_TO_WRITE = "DB Export!A1";

    private final String spreadsheetId;
    private final Sheets sheets;

    public SheetsWriter(@Value("${redeem.service.spreadsheet.id}") String spreadsheetId, Sheets sheets) {
        this.spreadsheetId = spreadsheetId;
        this.sheets = sheets;
    }

    /**
     * Prints the names and majors of students in a sample spreadsheet:
     * <a href="https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit">Link</a>
     */
    public synchronized void write(List<List<Object>> redeemCodes) {

        try {
            ValueRange body = new ValueRange().setValues(redeemCodes);

            log.info("Exporting to spreadsheetId={}", spreadsheetId);
            log.debug("redeemCodes={}", redeemCodes);

            sheets.spreadsheets().values()
                    .update(spreadsheetId, RANGE_TO_WRITE, body)
                    .setValueInputOption("USER_ENTERED")
                    .execute();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}