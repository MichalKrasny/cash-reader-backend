package app.cashreader.backend.presignedurl.api.v1;

import app.cashreader.backend.logging.methods.LoggedIO;
import app.cashreader.backend.presignedurl.service.PresignedUrlService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static app.cashreader.backend.logging.methods.LoggedIO.Level.INFO;
import static org.springframework.http.HttpStatus.OK;

@RestController
@LoggedIO(input = INFO, result = INFO)
@RequiredArgsConstructor
@RequestMapping("api/v1/presigned-url")
public class PresignedUrlController {

    private final PresignedUrlService presignedUrlService;

    @GetMapping
    public ResponseEntity<Object> url(@RequestParam String bucketName, @RequestParam String keyName) {
        String url = presignedUrlService.createPresignedGetUrl(bucketName, keyName);
        return new ResponseEntity<>(url, OK);
    }

}
