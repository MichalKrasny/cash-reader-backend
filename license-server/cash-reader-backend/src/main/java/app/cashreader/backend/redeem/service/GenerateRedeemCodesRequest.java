package app.cashreader.backend.redeem.service;

import lombok.Data;

@Data
public class GenerateRedeemCodesRequest {

    private final int numberOfLicences;

    /**
     * Field persisted in DB.
     */
    private final String notes;

    /**
     * This will be used as lang url attribute in the value in the KV in Cloudflare
     */
    private final String kvLang;
}
