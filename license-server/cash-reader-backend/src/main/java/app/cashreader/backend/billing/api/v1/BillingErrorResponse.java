package app.cashreader.backend.billing.api.v1;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class BillingErrorResponse {

    @Schema(example = "UNKNOWN_BUNDLE_ID", required = true)
    private final BillingErrorCode billingErrorCode;

    @Schema(example = "Bundle doesn't exist, bundleId=com.unknown.id")
    private final String message;

    public enum BillingErrorCode {
        UNKNOWN_BUNDLE_ID, OTHER
    }

    public BillingErrorResponse(@JsonProperty(value = "errorCode", required = true) final BillingErrorCode billingErrorCode,
                                @JsonProperty(value = "message") final String message) {
        this.billingErrorCode = billingErrorCode;
        this.message = message;
    }
}
