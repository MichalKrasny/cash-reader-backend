package app.cashreader.backend.db.repository;

import app.cashreader.backend.db.model.RedeemCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RedeemCodeRepository extends JpaRepository<RedeemCode, Long> {

    RedeemCode findByRedeemCode(String redeemCode);

    List<RedeemCode> findAllByOrderByIdAsc();

}
