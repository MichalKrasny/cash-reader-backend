package app.cashreader.backend;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Arrays;

@SpringBootApplication
//proxyTargetClass=true is here to use CGLIB and deal with classes without interfaces
@EnableAspectJAutoProxy(proxyTargetClass = true)
@Slf4j
@EnableScheduling
public class CashReaderBackend {

    public static void main(String[] args) {
        SpringApplication.run(CashReaderBackend.class, args);
    }

    //Making this readable for newcomers
    @SuppressWarnings({"Convert2Lambda", "unused"})
    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return new CommandLineRunner() {
            @Override
            public void run(final String... args) {
                log.info("Cash Reader Backend started");
                //printBeans(ctx);
            }
        };
    }

    private void printBeans(final ApplicationContext ctx) {
        System.out.println("Let's inspect the beans provided by Spring Boot:");

        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
            System.out.println(beanName);
        }
    }

}
