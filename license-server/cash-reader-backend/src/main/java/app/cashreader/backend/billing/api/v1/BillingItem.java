package app.cashreader.backend.billing.api.v1;

import app.cashreader.backend.billing.service.BillingService;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class BillingItem {

    @Schema(example = "4f3fcedb", required = true)
    private final String userId;

    @Schema(example = "28f64fea-f7e7-4091-9fed-3876a7b43502", required = true)
    private final String licenseNumber;

    @Schema(example = "2019-05-01 00:00:00",
            description = "Be careful, this example may not be reliable. If the format changes in future," +
                    " I will surely forget to update the documentation example." +
                    " Formatter string from source code is \"" + BillingService.BILLING_DATE_FORMAT_PATTERN + "\"",
            required = true)
    private final String billingDate;

    @Schema(example = "5.5", required = true)
    private final Double fee;

    @JsonCreator
    public BillingItem(
            @JsonProperty(value = "userId", required = true) String userId,
            @JsonProperty(value = "licenseNumber", required = true) String licenseNumber,
            @JsonProperty(value = "billingDate", required = true) String billingDate,
            @JsonProperty(value = "fee", required = true) Double fee) {
        this.userId = userId;
        this.licenseNumber = licenseNumber;
        this.billingDate = billingDate;
        this.fee = fee;
    }
}
