package app.cashreader.backend.logging.http;

import app.cashreader.backend.logging.http.request.RepeatedlyReadableHttpServletRequest;
import app.cashreader.backend.logging.http.response.ReadableOutputStringHttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;

/**
 * Logs request and response.
 */
@Slf4j
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class LoggingFilter implements Filter {

    private final HttpMessageFormatter httpMessageFormatter;

    public LoggingFilter(final HttpMessageFormatter httpMessageFormatter) {
        this.httpMessageFormatter = httpMessageFormatter;
    }

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        //do not wrap if not necessary
        RepeatedlyReadableHttpServletRequest requestWrapper = RepeatedlyReadableHttpServletRequest.from(request);
        ReadableOutputStringHttpServletResponse responseWrapper = ReadableOutputStringHttpServletResponse.from(response);
        log.info("{}", httpMessageFormatter.formatRequest(requestWrapper));

        try {
            chain.doFilter(requestWrapper, responseWrapper);

        } catch (Exception e) {
            log.info("{}", httpMessageFormatter.formatResponse(responseWrapper));
            throw e;
        }

        log.info("{}", httpMessageFormatter.formatResponse(responseWrapper));
    }
}
