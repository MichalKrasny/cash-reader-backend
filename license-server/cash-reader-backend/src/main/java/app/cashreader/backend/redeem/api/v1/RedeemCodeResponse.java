package app.cashreader.backend.redeem.api.v1;

import app.cashreader.backend.redeem.service.RedeemCodeStatus;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class RedeemCodeResponse {

    @Schema(example = "VALID", requiredMode = Schema.RequiredMode.REQUIRED)
    private RedeemCodeStatus redeemCodeStatus;

    @JsonCreator
    public RedeemCodeResponse(@JsonProperty(value = "redeemStatus", required = true) final RedeemCodeStatus redeemCodeStatus) {
        this.redeemCodeStatus = redeemCodeStatus;
    }
}
