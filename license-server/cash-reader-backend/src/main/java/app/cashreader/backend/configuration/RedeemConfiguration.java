package app.cashreader.backend.configuration;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Collections;

@Configuration
public class RedeemConfiguration {

    private static final String APPLICATION_NAME = "Cash Reader Backend Server";


    @Bean
    @ConditionalOnProperty(name = "redeem.service.export.enabled", havingValue = "true")
    public Sheets sheets(@Value("${redeem.service.export.credentials.file}") String credentialsFilePath) {

        try {
            InputStream credentialsStream = new FileInputStream(credentialsFilePath);

            GoogleCredentials credentials = ServiceAccountCredentials.fromStream(credentialsStream)
                    .createScoped(Collections.singleton(SheetsScopes.SPREADSHEETS));

            return new Sheets.Builder(
                    GoogleNetHttpTransport.newTrustedTransport(),
                    GsonFactory.getDefaultInstance(),
                    new HttpCredentialsAdapter(credentials))
                    .setApplicationName(APPLICATION_NAME)
                    .build();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
