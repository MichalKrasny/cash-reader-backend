package app.cashreader.backend.monitoring;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.util.unit.DataSize;

import java.io.File;

/**
 * Copypaste updated to show that metric in GB GB
 * <p>
 * A {@link HealthIndicator} that checks available disk space and reports a status of
 * {@link Status#DOWN} when it drops below a configurable threshold.
 *
 * @author Mattias Severson
 * @author Andy Wilkinson
 * @author Stephane Nicoll
 * @since 2.0.0
 */
@Slf4j
public class DiskSpaceHealthIndicatorGb extends AbstractHealthIndicator {

    private static final Log logger = LogFactory.getLog(DiskSpaceHealthIndicatorGb.class);

    private final File path;

    private final DataSize threshold;

    /**
     * Create a new {@code DiskSpaceHealthIndicatorGB} instance.
     *
     * @param path      the Path used to compute the available disk space
     * @param threshold the minimum disk space that should be available
     */
    public DiskSpaceHealthIndicatorGb(File path, DataSize threshold) {
        super("DiskSpace health check failed");
        this.path = path;
        this.threshold = threshold;
    }

    /**
     * Create a new {@code DiskSpaceHealthIndicatorGB} instance.
     *
     * @param path      the Path used to compute the available disk space
     * @param threshold the minimum disk space that should be available (in bytes)
     * @deprecated since 2.1.0 in favor of
     * {@link #DiskSpaceHealthIndicatorGb(File, DataSize)}
     */
    @Deprecated
    public DiskSpaceHealthIndicatorGb(File path, long threshold) {
        this(path, DataSize.ofBytes(threshold));
    }

    @Override
    protected void doHealthCheck(Health.Builder builder) {
        long diskFreeInBytes = this.path.getUsableSpace();
        if (diskFreeInBytes >= this.threshold.toBytes()) {
            builder.up();
        } else {
            logger.warn(String.format(
                    "Free disk space below threshold. Available: %d bytes (threshold: %s)",
                    diskFreeInBytes, this.threshold));
            builder.down();
        }
        builder.withDetail("total", toGb(this.path.getTotalSpace()))
                .withDetail("free", toGb(diskFreeInBytes))
                .withDetail("threshold", toGb(this.threshold.toBytes()));
    }

    private String toGb(long totalSpace) {
        return ((totalSpace / 1024) / 1024) / 1024 + " GB";
    }

}
