package app.cashreader.backend.billing.api.v1;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Data
public class BillingResponse {

    @Schema(example = "cz.hayaku.cashreader", required = true)
    private final String bundleId;

    @Schema(example = "30241.5", required = true)
    private final double totalFee;

    private final List<BillingItem> billingItems;

    @JsonCreator
    public BillingResponse(
            @JsonProperty(value = "bundleId", required = true) String bundleId,
            @JsonProperty(value = "totalFee", required = true) double totalFee,
            @JsonProperty(value = "billingItems", required = true) List<BillingItem> billingItems) {
        this.bundleId = bundleId;
        this.totalFee = totalFee;
        this.billingItems = billingItems;
    }
}
