package app.cashreader.backend.bundle.service;

public class UnknownBundleIdException extends RuntimeException{

    public UnknownBundleIdException() {
        super();
    }

    public UnknownBundleIdException(String message) {
        super(message);
    }

    public UnknownBundleIdException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownBundleIdException(Throwable cause) {
        super(cause);
    }

    protected UnknownBundleIdException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
