package app.cashreader.backend.bundle.api.v1;

public enum BundleStatus {
    VALID, EXPIRED, UNKNOWN_BUNDLE_ID
}
