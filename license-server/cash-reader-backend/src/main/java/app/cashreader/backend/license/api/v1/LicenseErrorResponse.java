package app.cashreader.backend.license.api.v1;

import javax.validation.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class LicenseErrorResponse {

    @Schema(example = "UNKNOWN_BUNDLE_ID", required = true)
    private final LicenseErrorCode licenseErrorCode;

    @Schema(example = "Bundle doesn't exist, bundleId=com.unknown.id")
    private final String message;

    @JsonCreator
    public LicenseErrorResponse(
                @NotBlank @JsonProperty(value = "errorCode") final LicenseErrorCode licenseErrorCode,
                @NotBlank @JsonProperty(value = "message") final String message) {
        this.licenseErrorCode = licenseErrorCode;
        this.message = message;
    }

    public enum LicenseErrorCode {
        EXPIRED_BUNDLE, UNKNOWN_BUNDLE_ID, UNKNOWN_USER_ID, OTHER
    }

}
