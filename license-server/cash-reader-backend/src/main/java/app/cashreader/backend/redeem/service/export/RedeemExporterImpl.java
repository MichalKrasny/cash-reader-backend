package app.cashreader.backend.redeem.service.export;

import app.cashreader.backend.db.model.RedeemCode;
import app.cashreader.backend.db.repository.RedeemCodeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Responsible for fetch data and send them to Google Sheets.
 */
@Component
@Slf4j
@RequiredArgsConstructor
@ConditionalOnProperty(name = "redeem.service.export.enabled", havingValue = "true")
public class RedeemExporterImpl implements RedeemExporter {

    public static final String FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(FORMAT_PATTERN);

    private final RedeemCodeRepository redeemCodeRepository;
    private final SheetsWriter sheetsWriter;


    /**
     * Exports redeem codes when application is started.
     */
    @PostConstruct
    public void onStartup() {
        export();
    }


    @Override
    public void exportExceptionSafe() {
        try {
            this.export();
        } catch (Exception e) {
            log.error("Error exporting spreadsheets", e);
        }
    }


    @Override
    @Scheduled(cron = "0 30 0 * * ?")
    public void export() {
        log.info("Exporting to Google Sheet");

        List<RedeemCode> redeemCodes = redeemCodeRepository.findAllByOrderByIdAsc();
        ArrayList<Object> headerRow = newArrayList("Redeem Code",
                "User ID",
                "Platform",
                "App Version",
                "Currency",
                "Lang",
                "System Version",
                "Device Model",
                "Redeem Date",
                "Notes",
                "Download URL"
        );

        List<List<Object>> headerAndData = new ArrayList<>();
        headerAndData.add(headerRow);

        List<List<Object>> data = toSheetData(redeemCodes);
        headerAndData.addAll(data);

        sheetsWriter.write(headerAndData);
    }

    private List<List<Object>> toSheetData(List<RedeemCode> redeemCodes) {
        return redeemCodes.stream().map(this::toRow).collect(Collectors.toList());
    }

    private List<Object> toRow(RedeemCode redeemCode) {
        String downloadUrl = "https://download.cashreader.app/" + redeemCode.getKvKey();
        return Arrays.asList(
                redeemCode.getRedeemCode(),
                emptyIfStringNull(redeemCode.getUserId()),
                emptyIfStringNull(redeemCode.getPlatform()),
                emptyIfStringNull(redeemCode.getAppVersion()),
                emptyIfStringNull(redeemCode.getCurrency()),
                emptyIfStringNull(redeemCode.getLang()),
                emptyIfStringNull(redeemCode.getSystemVersion()),
                emptyIfStringNull(redeemCode.getDeviceModel()),
                formatedZdt(redeemCode.getRedeemDate()),
                emptyIfStringNull(redeemCode.getNotes()),
                downloadUrl
        );
    }

    private String formatedZdt(ZonedDateTime zonedDateTime) {
        if (zonedDateTime == null) {
            return "";
        }

        return FORMATTER.format(zonedDateTime);
    }

    private Object emptyIfStringNull(String s) {
        return s == null ? "" : s;
    }


}
