package app.cashreader.backend.redeem.api.v1;

import app.cashreader.backend.logging.methods.LoggedIO;
import app.cashreader.backend.redeem.service.export.RedeemExporter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static app.cashreader.backend.logging.methods.LoggedIO.Level.INFO;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Slf4j
@LoggedIO(input = INFO, result = INFO)
@RequiredArgsConstructor
@RequestMapping(ExportController.EXPORT_PATH)
@ConditionalOnProperty({"redeem.service.export.endpoint.enabled", "redeem.service.export.enabled"})
public class ExportController {

    public static final String EXPORT_PATH = "api/v1/export";

    private final RedeemExporter redeemExporter;

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @LoggedIO(input = INFO, result = INFO)
    public ResponseEntity<RedeemCodeResponse> redeem() {
        try {
            redeemExporter.export();
            return new ResponseEntity<>(OK);
        } catch (Exception e) {
            log.error("Export error: " + e.getMessage(), e);
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
    }
}
