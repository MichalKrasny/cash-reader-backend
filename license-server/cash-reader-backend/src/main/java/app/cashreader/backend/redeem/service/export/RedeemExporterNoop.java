package app.cashreader.backend.redeem.service.export;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Component;

/**
 * No operation implementation if Redeem Exporter is turned off.
 */
@Slf4j
@RequiredArgsConstructor
@Component
@ConditionalOnMissingBean(RedeemExporterImpl.class)
public class RedeemExporterNoop implements RedeemExporter {

    @Override
    public void exportExceptionSafe() {
        log.info("Export turned off.");
    }

    @Override
    public void export() {
        log.info("Export turned off.");
    }
}
