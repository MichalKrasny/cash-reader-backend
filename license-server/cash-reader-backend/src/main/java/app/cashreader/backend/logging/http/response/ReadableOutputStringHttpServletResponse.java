package app.cashreader.backend.logging.http.response;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletOutputStream;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.function.Supplier;

/**
 * This class makes response output stream readable. This is required, when you need to log the message for example.
 * Class wraps the original {@link HttpServletResponse} and uses the {@link ReadableServletOutputStream} to copy all payload data
 * sent to original OutputStream to byte[]. Then it is able to access and log the payload
 */
@Slf4j
public class ReadableOutputStringHttpServletResponse implements HttpServletResponse {

    private final HttpServletResponse originalResponse;

    private ServletOutputStream outputStream;
    private ReadableServletOutputStream copier;
    private PrintWriter printWriter;

    public ReadableOutputStringHttpServletResponse(HttpServletResponse originalResponse) {
        this.originalResponse = originalResponse;
    }

    public static ReadableOutputStringHttpServletResponse from(final ServletResponse response) {
        return response instanceof ReadableOutputStringHttpServletResponse ? (ReadableOutputStringHttpServletResponse) response : new ReadableOutputStringHttpServletResponse((HttpServletResponse) response);
    }

    public String getOutputAsString() {
        try {
            if (copier == null || copier.getCopy() == null) {
                return "";
            } else {
                final String characterEncoding = originalResponse.getCharacterEncoding() == null ? "UTF-8" : originalResponse.getCharacterEncoding();
                return new String(copier.getCopy(), characterEncoding);
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        if (printWriter != null) {
            throw new IllegalStateException("getWriter() has already been called on this response.");
        }

        if (outputStream == null) {
            outputStream = originalResponse.getOutputStream();
            copier = new ReadableServletOutputStream(outputStream);
        }

        return copier;
    }

    @Override
    public PrintWriter getWriter() throws IOException {
        if (outputStream != null) {
            throw new IllegalStateException("getOutputStream() has already been called on this response.");
        }

        if (printWriter == null) {
            copier = new ReadableServletOutputStream(this.originalResponse.getOutputStream());
            printWriter = new PrintWriter(new OutputStreamWriter(copier, this.originalResponse.getCharacterEncoding()), true);
        }

        return printWriter;
    }

    @Override
    public void flushBuffer() throws IOException {
        if (printWriter != null) {
            printWriter.flush();
        } else if (outputStream != null) {
            copier.flush();
        }
    }

    //*********************** generated unmodified methods *************************

    @Override
    public void addCookie(final Cookie cookie) {
        originalResponse.addCookie(cookie);
    }

    @Override
    public boolean containsHeader(final String name) {
        return originalResponse.containsHeader(name);
    }

    @Override
    public String encodeURL(final String url) {
        return originalResponse.encodeURL(url);
    }

    @Override
    public String encodeRedirectURL(final String url) {
        return originalResponse.encodeRedirectURL(url);
    }

    @Override
    @Deprecated
    public String encodeUrl(final String url) {
        return originalResponse.encodeUrl(url);
    }

    @Override
    @Deprecated
    public String encodeRedirectUrl(final String url) {
        return originalResponse.encodeRedirectUrl(url);
    }

    @Override
    public void sendError(final int sc, final String msg) throws IOException {
        originalResponse.sendError(sc, msg);
    }

    @Override
    public void sendError(final int sc) throws IOException {
        originalResponse.sendError(sc);
    }

    @Override
    public void sendRedirect(final String location) throws IOException {
        originalResponse.sendRedirect(location);
    }

    @Override
    public void setDateHeader(final String name, final long date) {
        originalResponse.setDateHeader(name, date);
    }

    @Override
    public void addDateHeader(final String name, final long date) {
        originalResponse.addDateHeader(name, date);
    }

    @Override
    public void setHeader(final String name, final String value) {
        originalResponse.setHeader(name, value);
    }

    @Override
    public void addHeader(final String name, final String value) {
        originalResponse.addHeader(name, value);
    }

    @Override
    public void setIntHeader(final String name, final int value) {
        originalResponse.setIntHeader(name, value);
    }

    @Override
    public void addIntHeader(final String name, final int value) {
        originalResponse.addIntHeader(name, value);
    }

    @Override
    public void setStatus(final int sc) {
        originalResponse.setStatus(sc);
    }

    @Override
    @Deprecated
    public void setStatus(final int sc, final String sm) {
        originalResponse.setStatus(sc, sm);
    }

    @Override
    public int getStatus() {
        return originalResponse.getStatus();
    }

    @Override
    public String getHeader(final String name) {
        return originalResponse.getHeader(name);
    }

    @Override
    public Collection<String> getHeaders(final String name) {
        return originalResponse.getHeaders(name);
    }

    @Override
    public Collection<String> getHeaderNames() {
        return originalResponse.getHeaderNames();
    }

    @Override
    public void setTrailerFields(final Supplier<Map<String, String>> supplier) {
        originalResponse.setTrailerFields(supplier);
    }

    @Override
    public Supplier<Map<String, String>> getTrailerFields() {
        return originalResponse.getTrailerFields();
    }

    @Override
    public String getCharacterEncoding() {
        return originalResponse.getCharacterEncoding();
    }

    @Override
    public String getContentType() {
        return originalResponse.getContentType();
    }

    @Override
    public void setCharacterEncoding(final String charset) {
        originalResponse.setCharacterEncoding(charset);
    }

    @Override
    public void setContentLength(final int len) {
        originalResponse.setContentLength(len);
    }

    @Override
    public void setContentLengthLong(final long length) {
        originalResponse.setContentLengthLong(length);
    }

    @Override
    public void setContentType(final String type) {
        originalResponse.setContentType(type);
    }

    @Override
    public void setBufferSize(final int size) {
        originalResponse.setBufferSize(size);
    }

    @Override
    public int getBufferSize() {
        return originalResponse.getBufferSize();
    }

    @Override
    public void resetBuffer() {
        originalResponse.resetBuffer();
    }

    @Override
    public boolean isCommitted() {
        return originalResponse.isCommitted();
    }

    @Override
    public void reset() {
        originalResponse.reset();
    }

    @Override
    public void setLocale(final Locale loc) {
        originalResponse.setLocale(loc);
    }

    @Override
    public Locale getLocale() {
        return originalResponse.getLocale();
    }
}
