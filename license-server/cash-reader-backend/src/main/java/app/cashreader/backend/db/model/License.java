package app.cashreader.backend.db.model;

import lombok.*;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static com.google.common.collect.Lists.newArrayList;
import static org.hibernate.annotations.CascadeType.ALL;

@Entity(name = "license")
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class License extends AuditModel {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private UUID licenseNumber;

    @JoinColumn(name = "fk_bundle_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Bundle bundle;

    @Column(name = "user_id", nullable = false)
    private String userId;

    /**
     * This column is needed, because if I set the auditing column created_at in tests, the
     * querying in @{@link app.cashreader.backend.db.repository.LicenseRepository} does not work
     */
    @Column(name = "billing_date", nullable = false, updatable = false)
    private LocalDateTime billingDate;

    @OneToMany(mappedBy = "license", orphanRemoval = true, fetch = FetchType.EAGER)
    @Cascade(ALL)
    private List<LicenseHistory> history = newArrayList();

    public static License of(UUID licenseNumber, Bundle bundle, String userId) {
        return new License(null, licenseNumber, bundle, userId, LocalDateTime.now(), newArrayList());
    }

}
