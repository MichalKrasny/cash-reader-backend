package app.cashreader.backend.license.api.v1;

import java.util.UUID;
import javax.validation.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class LicenseResponse {

    @Schema(example = "28f64fea-f7e7-4091-9fed-3876a7b43502", required = true)
    private final UUID licenseId;

    @JsonCreator
    public LicenseResponse(@NotBlank @JsonProperty(value = "licenseId", required = true) final UUID licenseId) {
        this.licenseId = licenseId;
    }
}
