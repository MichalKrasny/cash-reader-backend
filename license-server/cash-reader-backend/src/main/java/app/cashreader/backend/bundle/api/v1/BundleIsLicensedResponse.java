package app.cashreader.backend.bundle.api.v1;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class BundleIsLicensedResponse {

    @Schema(example = "VALID", required = true)
    private BundleStatus bundleStatus;

    @JsonCreator
    public BundleIsLicensedResponse(@JsonProperty(value = "licenseStatus", required = true) final BundleStatus bundleStatus) {
        this.bundleStatus = bundleStatus;
    }
}
