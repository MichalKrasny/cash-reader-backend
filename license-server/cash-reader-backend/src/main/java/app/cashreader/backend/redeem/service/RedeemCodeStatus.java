package app.cashreader.backend.redeem.service;

public enum RedeemCodeStatus {
    VALID, INVALID, ALREADY_SET
}
