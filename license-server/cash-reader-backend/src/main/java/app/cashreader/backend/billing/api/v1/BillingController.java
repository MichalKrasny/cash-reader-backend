
package app.cashreader.backend.billing.api.v1;

import app.cashreader.backend.billing.service.BillingService;
import app.cashreader.backend.bundle.service.UnknownBundleIdException;
import app.cashreader.backend.logging.methods.LoggedIO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static app.cashreader.backend.billing.api.v1.BillingErrorResponse.BillingErrorCode.UNKNOWN_BUNDLE_ID;
import static app.cashreader.backend.logging.methods.LoggedIO.Level.INFO;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("api/v1/billing")
@Slf4j
@LoggedIO(input = INFO, result = INFO)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Tag(name = "Billing Endpoint", description = "Operations associated with billing")
public class BillingController {

    private final BillingService billingService;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @Operation(summary = "Get billing for month", tags = "Billing Endpoint", description = "Retrieves monthly billing data,",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Billing for requested month.",
                            content = {@Content(schema = @Schema(implementation = BillingResponse.class))}
                    ),
                    @ApiResponse(responseCode = "500",
                            description = "Internal Error",
                            content = {@Content(schema = @Schema(implementation = BillingErrorResponse.class))
                            })
            }
    )
    public ResponseEntity<Object> getBilling(@Valid @RequestBody BillingRequestForMonth billingRequestForMonth) {
        try {
            return ResponseEntity.ok().body(billingService.getBilling(billingRequestForMonth));

        } catch (UnknownBundleIdException e) {
            return ResponseEntity.badRequest().body(new BillingErrorResponse(UNKNOWN_BUNDLE_ID, e.getMessage()));
        }
    }
}
