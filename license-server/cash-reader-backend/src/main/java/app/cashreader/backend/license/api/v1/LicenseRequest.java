package app.cashreader.backend.license.api.v1;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Wither;

import javax.validation.constraints.NotBlank;

@Data
@Wither
public class LicenseRequest {

    @Schema(example = "cz.hayaku.cashreader", required = true)
    @NotBlank
    @JsonProperty(value = "bundleId", required = true)
    private final String bundleId;

    @Schema(example = "4f3fcedb", required = true)
    @NotBlank
    @JsonProperty(value = "userId", required = true)
    private final String userId;

    @Schema(example = "iOS", required = true)
    @NotBlank
    @JsonProperty(value = "platform", required = true)
    private final String platform;

    @Schema(example = "3a650b285ee288c8")
    //nullable for backwards compatibility
    @JsonProperty(value = "deviceId")
    private final String deviceId;

}
