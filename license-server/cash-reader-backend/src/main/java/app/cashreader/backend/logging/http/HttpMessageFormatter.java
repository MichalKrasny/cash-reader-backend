package app.cashreader.backend.logging.http;

import app.cashreader.backend.logging.http.request.RepeatedlyReadableHttpServletRequest;
import app.cashreader.backend.logging.http.response.ReadableOutputStringHttpServletResponse;
import lombok.NonNull;
import org.springframework.stereotype.Component;

import java.util.Enumeration;

@Component
public class HttpMessageFormatter {

    public String formatRequest(@NonNull RepeatedlyReadableHttpServletRequest request) {
        StringBuilder stringBuilder = new StringBuilder("Request:").append("\n");
        stringBuilder.append("Request URI=").append(request.getRequestURI()).append("\n");
        stringBuilder.append("Method=").append(request.getMethod()).append("\n");
        stringBuilder.append("Header:").append("\n");

        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            String headerValue = request.getHeader(headerName);
            stringBuilder.append(headerName).append(": ").append(headerValue).append("\n");
        }

        stringBuilder.append("Payload:").append("\n");
        stringBuilder.append(request.getPayloadAsString());

        return stringBuilder.toString();
    }


    public String formatResponse(@NonNull ReadableOutputStringHttpServletResponse response) {
        StringBuilder stringBuilder = new StringBuilder("Response:").append("\n");
        stringBuilder.append("Status: ").append(response.getStatus()).append("\n");

        stringBuilder.append("Header:").append("\n");
        for (String headerName : response.getHeaderNames()) {
            for (String headerValue : response.getHeaders(headerName)) {
                stringBuilder.append(headerName).append(": ").append(headerValue).append("\n");
            }
        }

        stringBuilder.append("Payload:");
        stringBuilder.append(response.getOutputAsString());

        return stringBuilder.toString();
    }

}
