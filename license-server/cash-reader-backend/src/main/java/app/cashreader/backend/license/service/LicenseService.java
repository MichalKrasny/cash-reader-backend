package app.cashreader.backend.license.service;

import app.cashreader.backend.bundle.service.BundleExpiredException;
import app.cashreader.backend.bundle.service.BundleService;
import app.cashreader.backend.bundle.service.UnknownBundleIdException;
import app.cashreader.backend.db.model.Bundle;
import app.cashreader.backend.db.model.License;
import app.cashreader.backend.db.model.LicenseHistory;
import app.cashreader.backend.db.repository.LicenseRepository;
import app.cashreader.backend.license.api.v1.LicenseRequest;
import app.cashreader.backend.logging.methods.LoggedIO;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

import static app.cashreader.backend.logging.methods.LoggedIO.Level.DEBUG;

@Slf4j
@Service
@LoggedIO(input = DEBUG, result = DEBUG)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LicenseService {

    private final LicenseRepository licenseRepository;
    private final BundleService bundleService;

    /**
     * Generates a new license. It may refuse to generate a license if either the bundle is not valid or
     * the bundle has limited users and we don't know about the user who asked for the license.
     *
     * @param licenseRequest Can not be null.
     * @return New license or existing licence, if already requested for this user. Never returns null.
     * @throws UnknownBundleIdException when bundle ID is not known
     * @throws UnknownUserException when user ID is not known for a limited bundle
     * @throws BundleExpiredException   when bundle ID is not valid
     */
    public UUID getLicense(@NonNull final LicenseRequest licenseRequest) {

        @NotBlank final String bundleStoreId = licenseRequest.getBundleId();
        @NotBlank final String userStoreId = licenseRequest.getUserId();
        @NotBlank final String platform = licenseRequest.getPlatform();

        final String deviceId = licenseRequest.getDeviceId();

        final Bundle bundle = bundleService.getValidBundle(bundleStoreId);

        validateUserIdForLimitedBundles(bundle, userStoreId);


        License license = licenseRepository.findByBundleIdAndUserId(bundle.getId(), userStoreId);
        if (license == null) {
            license = License.of(UUID.randomUUID(), bundle, userStoreId);
        }

        license.getHistory().add(new LicenseHistory(null, platform, deviceId, license));
        License savedLicense = licenseRepository.save(license);
        return savedLicense.getLicenseNumber();
    }


    private void validateUserIdForLimitedBundles(final Bundle bundle, final @NotBlank String userStoreId) {
        if (bundle.isLimitedUserIds() && !bundle.getUserStoreIds().contains(userStoreId)) {
            throw new UnknownUserException("Unknown user \"" + userStoreId + "\" for bundle " + bundle.getBundleStoreId());
        }
    }


}
