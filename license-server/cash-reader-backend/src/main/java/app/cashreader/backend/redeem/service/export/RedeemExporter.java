package app.cashreader.backend.redeem.service.export;

public interface RedeemExporter {
    void exportExceptionSafe();

    void export();
}
