package app.cashreader.backend.redeem.service.cloudflare.kv;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 *
 */
@Component
@Slf4j
public class KvClient {

    private final RestTemplate restTemplate;
    private final String apiToken;
    private final String accountId;
    private final String namespaceId;

    public KvClient(RestTemplate restTemplate,
                    @Value("${cloudflare.kv-store.apiToken}") String apiToken,
                    @Value("${cloudflare.kv-store.accountId}") String accountId,
                    @Value("${cloudflare.kv-store.namespaceId}") String namespaceId) {
        this.restTemplate = restTemplate;
        this.apiToken = apiToken;
        this.accountId = accountId;
        this.namespaceId = namespaceId;
    }

    public void storeKv(String key, String value) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + apiToken);
        headers.setContentType(MediaType.TEXT_PLAIN); // Adjust if necessary
        HttpEntity<String> request = new HttpEntity<>(value, headers);

        String kvUrl = "https://api.cloudflare.com/client/v4/accounts/" + accountId + "/storage/kv/namespaces/" + namespaceId + "/values/" + key;

        restTemplate.exchange(kvUrl, HttpMethod.PUT, request, String.class);
    }
}
