package app.cashreader.backend.configuration.monitoring;

import app.cashreader.backend.monitoring.DiskSpaceHealthIndicatorGb;
import org.springframework.boot.actuate.autoconfigure.health.ConditionalOnEnabledHealthIndicator;
import org.springframework.boot.actuate.autoconfigure.system.DiskSpaceHealthIndicatorProperties;
import org.springframework.boot.actuate.system.DiskSpaceHealthIndicator;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * {@link EnableAutoConfiguration Auto-configuration} for
 * {@link DiskSpaceHealthIndicator}.
 */
@Configuration
@ConditionalOnEnabledHealthIndicator("diskspace")
public class ActuatorConfiguration {


    //will suppress original DiskSpaceHealthIndicator io autoconfiguration
    @Bean(name = "DiskSpaceHealthIndicator")
    public DiskSpaceHealthIndicatorGb diskSpaceHealthIndicator(DiskSpaceHealthIndicatorProperties properties) {
        return new DiskSpaceHealthIndicatorGb(properties.getPath(), properties.getThreshold());
    }
}
