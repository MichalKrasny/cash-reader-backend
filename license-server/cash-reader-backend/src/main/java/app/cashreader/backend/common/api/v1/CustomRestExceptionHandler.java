package app.cashreader.backend.common.api.v1;

import app.cashreader.backend.license.api.v1.LicenseController;
import app.cashreader.backend.license.api.v1.LicenseErrorResponse;
import static app.cashreader.backend.license.api.v1.LicenseErrorResponse.LicenseErrorCode.OTHER;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Custom handling of exceptions, thrown by {@link LicenseController}. Generally, they should be transformed to {@link LicenseErrorResponse}
 */
@ControllerAdvice(basePackages = "app.cashreader.backend")
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {


    // TODO Find out how this works and solve it
    @SuppressWarnings("NullableProblems")
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
                MethodArgumentNotValidException ex,
                HttpHeaders headers,
                HttpStatus status,
                WebRequest request) {

        String errorMessage = getErrorsAsString(ex);

        return handleExceptionInternal(ex, new LicenseErrorResponse(OTHER, errorMessage), headers, BAD_REQUEST, request);
    }

    private String getErrorsAsString(MethodArgumentNotValidException ex) {
        List<String> errors = new ArrayList<>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        return String.join("\n", errors);
    }
}
