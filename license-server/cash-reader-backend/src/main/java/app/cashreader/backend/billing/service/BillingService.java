package app.cashreader.backend.billing.service;

import app.cashreader.backend.billing.api.v1.BillingItem;
import app.cashreader.backend.billing.api.v1.BillingRequestForMonth;
import app.cashreader.backend.billing.api.v1.BillingResponse;
import app.cashreader.backend.bundle.service.BundleService;
import app.cashreader.backend.bundle.service.UnknownBundleIdException;
import app.cashreader.backend.db.model.License;
import app.cashreader.backend.db.repository.LicenseRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BillingService {

    public static final String BILLING_DATE_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(BILLING_DATE_FORMAT_PATTERN);
    private final LicenseRepository licenseRepository;
    private final BundleService bundleService;
    private final ObjectMapper objectMapper;

    /**
     * @throws UnknownBundleIdException when bundle ID does not exist
     */
    public BillingResponse getBilling(final BillingRequestForMonth request) {

        try {
            log.info("BillingRequestForMonth={}\n", objectMapper.writeValueAsString(request));
            bundleService.verifyBudleExists(request.getBundleId());

            final List<License> billedLicenses = licenseRepository.findByBundleStoreIdAndYearAndMonth(request.getBundleId(), request.getYear(), request.getMonth());
            final List<String> freeOfChargeUserIdsTrimmed = request.getFreeOfChargeUserIds().stream().map(String::trim).collect(Collectors.toList());

            double totalFee = 0;
            List<BillingItem> billingItems = newArrayList();
            for (License billedLicense : billedLicenses) {
                final String billingDate = billedLicense.getBillingDate().format(FORMATTER);
                double fee = freeOfChargeUserIdsTrimmed.contains(billedLicense.getUserId()) ? 0 : request.getFee();
                totalFee += fee;
                billingItems.add(new BillingItem(billedLicense.getUserId(), billedLicense.getLicenseNumber().toString(), billingDate, fee));
            }

            final BillingResponse resut = new BillingResponse(request.getBundleId(), totalFee, billingItems);
            log.info("BillingResponse={}\n", objectMapper.writeValueAsString(resut));
            return resut;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }


}
