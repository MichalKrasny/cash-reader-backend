package app.cashreader.backend.presignedurl.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.presigner.S3Presigner;
import software.amazon.awssdk.services.s3.presigner.model.GetObjectPresignRequest;

import java.net.URI;
import java.time.Duration;

@Component
@Slf4j
public class PresignedUrlService {

    /* Create a pre-signed URL to download an object in a subsequent GET request. */
    public String createPresignedGetUrl(String bucketName, String objectKey) {

        String accessKey = "daa0af5ca16dbad297662cbc18e83146";
        String secretKey = "e4b618fea5e84c4d1a1ca747ec59bb1cfe1c51d3fbd6d063e04ffe2608aca10a";
        //account id from UI
        String r2Endpoint = "https://a447808f8a62f6caacb396fd807e64f3.r2.cloudflarestorage.com";

        try (S3Presigner presigner = S3Presigner.builder()
                .endpointOverride(URI.create(r2Endpoint)) // Use the same R2 endpoint
                .credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(accessKey, secretKey)))
                .region(Region.of("auto"))
                .build()
        ) {

            GetObjectRequest getObjectRequest = GetObjectRequest.builder()
                    .bucket(bucketName)
                    .key(objectKey)
                    .build();

            GetObjectPresignRequest presignRequest = GetObjectPresignRequest.builder()
                    .getObjectRequest(getObjectRequest)
                    .signatureDuration(Duration.ofMinutes(10))
                    .build();

            return presigner.presignGetObject(presignRequest).url().toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
