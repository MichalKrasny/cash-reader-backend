package app.cashreader.backend.redeem.service.generator;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Generates a redeem code value.
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class RedeemCodeGenerator {

    public static final int CODE_LENGTH = 8;
    public static final String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private final CodeGenerator codeGenerator;


    public String get() {
        return codeGenerator.get(CHARS, CODE_LENGTH);
    }
}
