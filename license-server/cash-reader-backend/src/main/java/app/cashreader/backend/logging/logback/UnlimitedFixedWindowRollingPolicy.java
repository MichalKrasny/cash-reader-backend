package app.cashreader.backend.logging.logback;

import ch.qos.logback.core.rolling.FixedWindowRollingPolicy;

public class UnlimitedFixedWindowRollingPolicy extends FixedWindowRollingPolicy {
    @Override
    protected int getMaxWindowSize() {
        return 1024;
    }
}
