package app.cashreader.backend.redeem.service;

import app.cashreader.backend.db.model.RedeemCode;
import app.cashreader.backend.db.repository.RedeemCodeRepository;
import app.cashreader.backend.logging.methods.LoggedIO;
import app.cashreader.backend.redeem.service.cloudflare.kv.KvClient;
import app.cashreader.backend.redeem.service.cloudflare.kv.KvKeyGenerator;
import app.cashreader.backend.redeem.service.export.RedeemExporter;
import app.cashreader.backend.redeem.service.generator.RedeemCodeGenerator;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;

import static app.cashreader.backend.logging.methods.LoggedIO.Level.DEBUG;

@Slf4j
@Service
@LoggedIO(input = DEBUG, result = DEBUG)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RedeemService {

    private final RedeemCodeRepository redeemCodeRepository;

    private final RedeemExporter redeemExporter;

    private final RedeemCodeGenerator redeemCodeGenerator;
    private final KvKeyGenerator kvKeyGenerator;
    private final KvClient kvClient;

    @Transactional
    public RedeemCodeStatus redeem(String redeemCodeStr,
                                   String userId,
                                   String platform,
                                   String campaign,
                                   String appVersion,
                                   String currency,
                                   String lang,
                                   String systemVersion,
                                   String deviceModel) {
        String redeemCodeStrUpperCase = redeemCodeStr.toUpperCase();
        RedeemCode redeemCode = redeemCodeRepository.findByRedeemCode(redeemCodeStrUpperCase);

        // doesn't exist
        if (redeemCode == null) {
            return RedeemCodeStatus.INVALID;
        }

        // already taken
        if (redeemCode.getRedeemDate() != null) {
            return RedeemCodeStatus.ALREADY_SET;
        }

        redeemCode.setRedeemDate(ZonedDateTime.now());
        redeemCode.setUserId(userId);
        redeemCode.setPlatform(platform);
        redeemCode.setCampaign(campaign);
        redeemCode.setAppVersion(appVersion);
        redeemCode.setCurrency(currency);
        redeemCode.setLang(lang);
        redeemCode.setSystemVersion(systemVersion);
        redeemCode.setDeviceModel(deviceModel);
        redeemCodeRepository.save(redeemCode);

        redeemExporter.exportExceptionSafe();

        return RedeemCodeStatus.VALID;
    }

    @Transactional
    public void generateNewRedeemCodes(@NonNull GenerateRedeemCodesRequest generateRedeemCodesRequest) {
        int numberOfLicences = generateRedeemCodesRequest.getNumberOfLicences();
        for (int i = 0; i < numberOfLicences; i++) {
            String redeemCodeValue = redeemCodeGenerator.get();
            RedeemCode newEntity = RedeemCode.newEntity(redeemCodeValue);
            newEntity.setNotes(generateRedeemCodesRequest.getNotes());
            String kvKey = kvKeyGenerator.get();
            newEntity.setKvKey(kvKey);
            redeemCodeRepository.save(newEntity);

            String kvValue = getKvValue(redeemCodeValue, generateRedeemCodesRequest.getKvLang());
            kvClient.storeKv(kvKey, kvValue);
        }

        redeemExporter.exportExceptionSafe();
    }

    private static String getKvValue(String redeemCodeValue, String kvLang) {
        String result = "/settings?settingsId=" + redeemCodeValue;

        if (!StringUtils.isBlank(kvLang)) {
            result += "&lang=" + kvLang.trim();
        }
        return result;
    }
}
