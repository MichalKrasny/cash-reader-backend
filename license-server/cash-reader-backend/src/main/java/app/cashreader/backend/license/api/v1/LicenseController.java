package app.cashreader.backend.license.api.v1;

import app.cashreader.backend.bundle.service.BundleExpiredException;
import app.cashreader.backend.bundle.service.UnknownBundleIdException;
import app.cashreader.backend.license.service.LicenseService;
import app.cashreader.backend.license.service.UnknownUserException;
import app.cashreader.backend.logging.methods.LoggedIO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

import static app.cashreader.backend.license.api.v1.LicenseErrorResponse.LicenseErrorCode.*;
import static app.cashreader.backend.logging.methods.LoggedIO.Level.INFO;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("api/v1/license")
@Slf4j
@LoggedIO(input = INFO, result = INFO)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LicenseController {

    private final LicenseService licenseService;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @Operation(summary = "Get new license or retrieve previously issued one for this userId.", tags = "License Endpoint", description = "Licensing operations.",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "New license or previously issued license for this user, if request was called multiple times.",
                            content = {@Content(schema = @Schema(implementation = LicenseResponse.class))}
                    ),
                    @ApiResponse(responseCode = "400",
                            description = "License can not be retrieved because of either expired bundle or requested bundleId does not exist.",
                            content = {@Content(schema = @Schema(implementation = LicenseErrorResponse.class))
                            }
                    ),
                    @ApiResponse(responseCode = "500",
                            description = "Internal Error",
                            content = {@Content(schema = @Schema(implementation = LicenseErrorResponse.class))
                            }
                    )
            }
    )
    @LoggedIO(input = INFO, result = INFO)
    public ResponseEntity<Object> newLicense(@Valid @RequestBody LicenseRequest licenseRequest) {
        try {
            final UUID licenseNumber = licenseService.getLicense(licenseRequest);
            return new ResponseEntity<>(new LicenseResponse(licenseNumber), OK);
        } catch (UnknownBundleIdException e) {
            return new ResponseEntity<>(new LicenseErrorResponse(UNKNOWN_BUNDLE_ID, e.getMessage()), BAD_REQUEST);
        } catch (UnknownUserException e) {
            return new ResponseEntity<>(new LicenseErrorResponse(UNKNOWN_USER_ID, e.getMessage()), BAD_REQUEST);
        } catch (BundleExpiredException e) {
            return new ResponseEntity<>(new LicenseErrorResponse(EXPIRED_BUNDLE, e.getMessage()), BAD_REQUEST);
        }
    }

}
