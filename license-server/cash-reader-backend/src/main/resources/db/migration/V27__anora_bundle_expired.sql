UPDATE bundle
SET
    valid_to = TIMESTAMP WITH TIME ZONE '2024-10-30 18:00:00+01',
    updated_at = now()
where bundle_store_id = 'com.anora.anoratest';

