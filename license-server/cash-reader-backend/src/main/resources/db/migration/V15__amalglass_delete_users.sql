delete
from license_history
where fk_license_id in (select id
                        from license
                        where fk_bundle_id = (select id
                                              from bundle
                                              where bundle_store_id = 'com.amalglass.amalglassmobile')
                          and user_id = 'test');
delete
from license
where fk_bundle_id = (select id
                      from bundle
                      where bundle_store_id = 'com.amalglass.amalglassmobile')
  and user_id = 'test';


update bundle
set limited_user_ids = true
where bundle_store_id = 'com.amalglass.amalglassmobile';


INSERT INTO limited_user
VALUES ('Key-FullVersionTestingAmalGlassV2',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));

INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ejtIh-#EhqxsvbqV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Z7C1o66u3-g#oz18-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-qNEyprBv7aqH7Ghb-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-#edp3hq8DgakiCIY-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-HfxT@eqtli2x50Rh-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-yYerEvWTGg8Kww0x-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-lYhnYxT5QmhW3fY8-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-BQfZ-m!jCyIrfVDy-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-xUYxG#4g#mDuZzLC-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-d4#CTjooSJdSouwx-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-6OEAAQX-mLlNLOwW-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FsScNsoI2bDQKKv!-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-4sGTIJI-MK93gQEh-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-2QSFSkBAM@q1NEtT-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-2h!qfxOkq96idVQQ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8RWqh4stgbbGW@sL-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-0R8yZOljypoCNm7G-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-zjN+CJU7p2owGM9D-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Y4!5eXRFDBuWwnZR-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-tYWoMDwI#8AEZQry-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-SOja0onmWXXQCpXW-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-BNU-vxOTyLBFGIXW-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@qfWHAnTwCofBj7+-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-UKBQjNzxb7fV+8Ed-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-rOJ0Kgrko2s#@0!j-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-P1L-U!Wgm1HMfh9O-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-L-h5UnUGLJEuhy!z-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-bw1JP7@fovxS8o-z-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8PqyJKOp#7ZIK-@y-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-!rH2rBnaEGN0dRSQ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-piHnAjnuwOZyh94k-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-PlWtPsuNWa8Fg0jt-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-mgnsiStYK0tRj3vB-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-acjt2sdNBS-eAeP3-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-269oOv5fPqrp2FDq-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-NII0w3Q1VoLS4RYG-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-mot2!FC-JcevpLlN-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-UGBjkz6@5fGRqILv-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-emppPeWP#@M@-WJA-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Tv5ra7sbj3TFLR@u-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ju8TJFu3+8Hkpqn@-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-2RN@FznDmJUHY9ac-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-t-a#!7LzoZ-nw3A7-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-KJKWllsyIba-r7zf-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-W-Jr7unkyR-IQ9k2-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-SSyBa9GRZtY2euMj-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-mr@jcxYF6H-jWVFf-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-BkKzl34!!rpiRZZn-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-RpGLUtKqg!zsr!ph-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-c3!8lVKA#ldiwgJ8-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-zIMCd0e6ga4iJKpV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Ee@NSnHSaxGWLO8y-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Mda8eZchgHYAuZoO-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-u7yxk73wCBIM9SIC-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-SRjcRH!@5R5png6y-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-3lZ62ppsslYp5SgL-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-0UYkncPINRRifKIl-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-yCV5hbZ0w2kFL7BC-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MfUo7pYQsvyQv7gf-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-pFf7I#0mt9bBQykY-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-tygz2h8a0XQXA#wo-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001--nruMpawdo-EFWwx-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8UEA5+KrP7OMxs#D-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-2-!P+VhQB9KyXYA3-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-PmFJWAv0Ar5GWADx-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-DBaos6n2c7L@W3Hw-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-7ZIyCFVO#xKAa52W-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-C5y-OqVkm8jo14bs-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8jMfP3qzaIt0gUjk-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-6woL!UMG@GuxjS4y-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FIy39r25qRkt5MSi-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-v1KFJAmweC@yGzVv-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-aO0a3!NfLACr47uj-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-f6Hqou2i1iYhqIaL-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Oi46vVLk+83eHzWv-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-JpYAM4EiL6H1IQcf-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-LkJBGh+l5YYUZIBv-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-PniqnQsqCX!e-Opt-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Ui+IXcMz8ksAn5RL-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-+FsaJujGJSMye3MV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-LhXaVDjkhdjRlZKI-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Y42JtsyVFlezunZ9-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-bF74QeR883VSnMfP-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-U1FNSvRZlmVIfv86-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-5WwmXA4Z6F@+6UYf-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-q+KxIIUyu8MriiCX-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-dduP+ppa4TO#t9xy-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-kWeQgiuTyFVVmDaH-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-GLCReXVaBnpAa9Vh-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-d#z45YzQCrRIRrs0-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-G3JeByrWJLLA@-@X-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-83W0EJNUDHfJV-fz-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Gh6ZVNtON5m1f0yI-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-pF2Sjm+#s1@NYeQh-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-XlVFBI#WKWQXJ86T-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MKBDV7zo-C+-OI!D-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-5Ce!DKEKmpxuDwOs-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-EenYBCQyail9U8ka-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FmUcVV0ODXe--Plj-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-JR0VhICcNp+r-N2t-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-c4+vWmVblEQH#bWO-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-k4@ll0CwC3eUU0SX-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ZiNZYZncEQ29OySK-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-W3WqNumn75nF-@rl-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-#-SbuPAycKG0A!8A-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-4mWlSK9QHjvwJ1eh-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@GWqdXUwg#x7va8J-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@t7niav-M7Oz0eOR-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-KxnYnt2pjKHAkO!i-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ebAt5jpS60Xg4eU@-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-a469i299RLab#eNl-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-TC1l12Du@Rjo!BsS-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-08sAjSQCaFVAQCWs-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-EHMJSMMQhyIsqaQv-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Qv8-M6KLYfprsyKr-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-qy4NertFOLThw1Yg-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-3zUbziWEu3xI8@NL-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@s1aR3LhPRB-arxl-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-n#4gGTA6@GbCY4YV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-CQj25J+giI5BwEDO-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-oSaKeS5Q1a8DyHZn-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-xiZMSIcf4qEUa@QK-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-tB-pJqrirT3i09+v-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@9h89miUxlBYmz@V-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-WMnOU719JWjfve9I-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-TXDdOiWhOssclAOB-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-hVeG3EN-MQl7woTu-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-KlYk1oE8pP6WA+#0-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-H209xg5R!m8CzuB9-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-mLW9eeWY0PYKzLMf-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-1moEpVNL!1JeGG3B-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MPDn-qJA@XP98FDS-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-sxSE7JARtlGsAZYo-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MtBPFbisCG1Tf2Hd-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-J!x3MPkiUvgzp1mU-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-WCc1trmi4zJ0#yu1-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-B2Z@VuJ#dD-g66bq-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-uf6VFT@P2!f2RNpo-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-DuOy5csNv4i!r2Cw-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Q4yXB2EUBa9BQQtW-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-WEGqaG#aVsftsVWG-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-sd5LXSUp8l-gR73R-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-GPF4#rfx@ZDDc36T-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-+BCTy8ZJesRxhBYA-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-v-nH#lB74pvrhdy8-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-zQonW26oa+-fmtTP-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-!xKi0!120NEen0LV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-5buB6L6A517obF0x-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-oO7gopMKmVB7bIHt-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-PAm!q+8mFXmXjjfp-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-1MLUKiNk4F@997MB-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Q-eAwWaTsAk#zzTr-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-VBycv#DPLtHk-fds-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-urooIwiaBchtW5!s-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-7j-wEKJZ+G6VS+Md-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-efmGnnBLuJfReW1M-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-GSgT6SzA81DlL!AT-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-vRL-hxJ6wGR2iNxb-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-xe#EAx0LiHrjVtql-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-NwvAPgGWzkE5nkPG-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-xM5TVx5MN4QBxo7#-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-+tQlJxPaWeL!NNtB-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-l+tH#DB65V4+!e3P-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Qf50nMbDgl23K@FL-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-9JNd#LZ0UvuTogB#-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-htXSNMAOYHFRqGgq-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-VkY#Y3tkesRvzO2b-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-1w7oaPa924EtOwuU-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-0-TW!jxZWZftrEOV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-KJLBsQHo4oR2tM9F-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-dffOVLk0QMBF0+cG-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-RKCu!an-gUAUEm78-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-bERYASkqV+FCNJAR-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-PPT-4t7NtJ6k2kuO-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ccbAP2FqT4AOjmBK-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-g+3NkiX0b41MQ6cI-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-M3-1tUwGFkgykCQH-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-7PU+a4uORfuyqzFC-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-VZZTw7oRkbCNJX1B-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-lDWF+9mrq5oVCRgc-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-xwyrE5SDCfoWyYa--AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-AwuAx8l5xTtPEYTX-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Ni033iY!j8aHueTV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-XxCd9T0rg5W6K+q!-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Z4Bjxpvtv5ZVGLVy-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-SNtCNurYj9diDPk2-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-i9g5TcrG1sLY7pdm-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Lv2!YfCFRxgucY5p-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-5eNYJl!opY!O1K8!-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-TIz-7JHaXc#11!Lr-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-klcPFZ5yJ@mVqe9J-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-m2YnQT6MX8cnsDez-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-p8MDv#dDlVNUZ9Qs-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Bl@+cX2TrP3oUOXV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-#3PnJdMk@#KHSlh!-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FqCkCaYY1RQ@D5ls-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-WUeE-vIrwljF6TX4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Lv6bUTLMxXx3VSqI-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-lJKX3Bwbk1lB6a9M-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-DTgL6hrQ5W0jBJql-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-J-#4zTNJ@yYw@oU1-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-z+v3PqZY71@8#+xa-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-umPrbjX--nw8i0lG-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Ckz4CsvK7v+xvNKV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-uapA@-t3wfSZuA8n-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-a8j1E9aEF55bpsuc-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-W90ElWTgHdZWKbqN-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-DNH1bz#vVz4slekm-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Zr4aCmNrFcafb4Oz-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-9zzANZq@UWzKlfPP-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-mdMuETukmh5-fc@m-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-CWvdB5t8vRB0kT@A-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-yMOVw@ZPKvNUrbiA-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-o81eaO@Ql8TzKc98-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-KzsZTs!7L+TYUjEF-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-18w!SiB1p0flCexd-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-W!rdPEoq5qEFgSmH-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001--Bd+l77lPikeFykS-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-7GO9xnrA4oFqIWDK-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ZP0zk0dMUyw5eAKJ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-x3Z-ZiYF7Jp1250a-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-nnyhAoUNx1p8pLv6-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-G!jGw#2MuRBSp62c-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-UwF3aelkpD12V2dN-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Hdj#Lpn5lP5Pt93W-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FaWiliE-F-n@mVML-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-yyIy-IqwzJJQZHpL-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-qecOZto2w53tAUuG-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-tX+iT4A8QkXSnM0H-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-oWsrG3+B7Tua3IQl-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-w66ZSECsmdOAkugE-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-sUrS39XxXHNTXHeC-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-!PkLqCEWsD3H78XE-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-1HucKMpqQ!YD5+MG-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-DOUGs4T4d19mG95d-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-wwWtJM9yXrP6AJ54-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-gCDOys!!#xLCqU8l-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-z!cKlRdoaztpvlC8-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ODJvM64M6YvKx2BE-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-saU!84S!8DgHHkTA-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-qufMiJn9AFcqOe#X-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-m#SRYns2UQxLpq@f-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-U@+T#@34fmRX18rt-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-L9F5vZXX7phz8aeD-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-eRv+XE#jfCG-5+uu-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-JEnR@Pm2kH#RD!M@-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-#AdwVobtwDi8VnE9-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-t#oL5-3z5ZCWSVtA-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-f#XE8#38ynRM5IJQ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-U4Nt1ZW04MRmJTLS-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-IeSi6f3zduEYvh3v-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-xH2ZDjyOMoLcVT-O-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-LTJ-94pKsWDMGRim-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-+H@idw4X3suJv54x-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-LhvxtU8MIK0HW2#a-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Ny3EJXomdWBXhnLw-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-TEM6uCyBWe3XsWlx-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-imdIsmFUTJpWwkk--AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-JAWg0p3dmR1F1Tz4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-zEcMTmDD4-umI@AQ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-r2dTIKuXcBUT!mxV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-SqbjrlxcKKmaSAWF-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-YjULkgq7UFmZ8MjU-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-w!zlLBvhyt+J8AzX-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ucIQbu+hSkUCTbhj-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-DpPS4zF6z3UiGd12-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MBtFl5DqHxFnhaxF-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-M19lW50JSuc1v3U--AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@AKcph#BND7kSBFr-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-veP1G4u@yrQAoUfl-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-1eCj9tt1uKQ-3Hg4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Tc8qUGqOwhvdHOR5-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-gdiFE1XAtRa-MnBr-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-!7!2ai+bxofVzPos-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-QwykAyZ#rP9k6SSS-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-HC7I#2J0#sxGKlWo-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-QkZnUJPyDeWMU8r@-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-RgBQ85RQo7YZBeeu-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-1bwiOEU8f2LSo+qR-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-xi3ugVCK4zlcwWiW-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Y93u1!f093PAxqmx-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-G2pL87l3#Q3-Xltv-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-4TgwEYs9SPzCxRtE-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@nc89m#EpSsqRKiD-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-54yuPa!JGYy98CIQ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-lUzI-pH01AKSaoRD-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-tItoTVbhRYsbKkdv-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-RzOm23c+cGt3AVMC-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8qqhZRk2neGm28aL-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-6!y+6QdRfNy54szz-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-EXmefpzD@Pw-MkCt-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FACv+jH+LS9Wfgmk-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-!rm51yPhFsR2Yk13-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Nqb2d@RXge8!lB1z-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-kjR189!V1rk!#QYQ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Qyf##wFQ1toWveLT-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-9GIKhf4pNA8SJFlT-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FlQeehwlV4UNfS@1-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Av8xAVEzd+a7BVB5-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-bFqPIu!KK-YVtctW-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-PlMyxxeswv8-oVCZ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-M4mdt3rS3@tVK8A--AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-28EVfi8kurx!SMFD-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-1+L3T7E6@x#2ysxz-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-VWe1F4QXu8NqBp65-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-qbEYyIh9kIVwD7Qr-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-AZQ88Nl3-NidYj9n-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-#qNwm4gCa5lMMolw-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-NUk2MYXz4mad+8Hb-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-BlCE@!L6FLtqgpOg-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-uzCjhM2xpC6s7iTW-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-HPzchc5AB2hzA-TB-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-GoKr650L5-LTtdeY-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-429N5rswWR7VD-XV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-G6ps842ZNe+kywNd-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FqV8lpPl#OCzciNE-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-LRtdeO15WV#LTlWk-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-722LCMEEswQh3!Qg-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-UaIzg-rEJRK#JVM2-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-!nTjuoReMkSEtuUo-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-oKFPjwLPPIu@8A@6-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-LmSi5CDaqX+O9Wyb-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-i-GANF1TDok8CHa3-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-OmwPXut#GG!@E7UE-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-l0AiLu#a#A!cU+Hs-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-f7vu#zJqfqH13i5m-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-lR3Dgiz4p-WxynmA-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-n6iX!8vI+GjcqD@O-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-cqJQti!n-6VhO0Sw-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-kUkjzyokXGXh9Ocb-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-58qGdF5AOpnO@jMS-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-U0gnHnAl0+J7V2Et-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-nLVFL+IhLBw6DWEK-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8pdSrh#PBbga1dTZ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FAz+EB@#lwrQlQUm-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-l+Epcs!avzN10WkC-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-hn@L3Qm+miSUKngK-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-hD04FoSXB0Gp7A2p-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-stdgZ6I#XP9PIvXg-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-R-TbEMdxvI-1JwNy-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-aqAHFQntMEnNnuaD-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-+TZxj@UjBVcF9#7m-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-bfn@ZNoMK6vq-ULJ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-I5ZGzgeeLrKwoVPR-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MLaAEAjlGl!jlxGI-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FJlhHAeN+nP8sM-Q-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-6Nc!2hi5QS7r4A!q-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-PPfjqITabBcGh#2B-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-EOK@cgid12dcQ09b-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-JoX!#GNUdDeD9WS8-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-n1-npqmx+4EmZtdF-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-140K@AK0LtZYLIL8-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-RIwpbS#qYHLdMtPl-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-41AF!a89ws8JsLlc-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-bCZRRerqNKbWiMsl-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-OLW3Q7jzXOeYVLnl-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-wpvLpozghC8#vB!2-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-t+-VM4mUfuR#SDhY-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-QfQy5FXIx+!nDyA5-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-p86vBVZQLQ6b5XNw-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FL2dHvPsDI+y99lP-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-YeKTMzL1Q0wJBsXM-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-QrPu3-EC!G-QYK2Q-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8lhanoJEvBdwTbbd-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-hSEPxOFA3mo5RgT#-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-EnEwhUbP9PWP1ZFx-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-KlWlX3hQM0FOgDMy-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-k+xq8k4osYJLSSx6-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-gyvgGM8!vw@gJ!kn-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8UY74+GDpIJhnwlw-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-JFkQX2fR-Ri1hDz2-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8zojJchLH+pSiI-3-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-33NJzW69NAu7-5dY-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-6fj1V1cKLtpN9PLS-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001--IDDQolX4Zsx5jPC-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-k9ylu3tTkPRfO1bn-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-!xMDF2rlyH@PYl!n-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-kBIGbG-MqVfRwJWW-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MmD2Pjn!U2KIFDb!-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-IeF@XFfe-JLNDcFm-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-5UBygjWfWFg1ryxD-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-wOIkaq5G9Or-Q31x-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-te1ey1gbviFJ-FPP-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-1oIM!#@j+lNIjTsG-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8YfyoT-S2cNuuNu4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-C@5itdjYIS1SOAtu-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-wsoKcfGf0kdRUMxH-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-YjDwNleEOuxM8up1-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-oHCLOw4UcVj-PJM8-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-1vznfDRCWg6HXLcE-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-7Xzdd6#ZGhkojbz3-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-#bXpVFU1BwPoHKtN-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-xEwJ6g1MBPxE3uqo-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-1DozZryXd3Mot7SG-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Z!tmkU3ZWi0vn-n1-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-7Yk7EJ4RhnhHGZfX-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-GX3JfTPZKF-igSeg-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-AUVZMs2pnpmfHR2F-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-CxprSFEbcPXPCfP@-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ABzWt@SqjSrZ#X64-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-pNv!aymIkQqIcLWm-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-sV3vtu-dJEYPl7sV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-#Gurm0rHKRTycUGJ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-3gAn9xv@L@wFtC!h-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-v7R-jG0Q822#855f-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-VIrCvxfSI4@N@yq9-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-uAY6qc6Xluy#nLVb-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-fZCIR99GwG!f5OEb-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-G6H3aCF93xojsDpt-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-pD9doDo#1cMIP8uV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Gq04Jegd+uptIa1T-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-zOzoKG7X3KDnWXT#-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-OKiYysi#vIuYTexW-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FCr9Fu@q8haVNEJJ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-a!il#A+q7G+C3EUq-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-stXt!Q8-nMBZF8BC-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-a@#eL@hSTPK!hLO@-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-TtqKwZZ!sBp+11d0-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-P34F@BFfipdhaJjU-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-qCFm9OBEmoIqC4fL-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-PJklfio2g2MFHDFx-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-DaYls0lHQbpptP1f-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-X1SVxbLH#Ebv8Coz-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-!bWw7Jj4mIOLGY@y-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-uxp1K@1cvs4XCdd9-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-jSFrvtlJJZ+gIOGa-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-zHxra9#HAAclgCLg-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-1n9jxKp3sMR@-cV6-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-kUHebXwN!4ZJ4uxH-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-pWi48mZqeOVVIjnd-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-wsWVBJIVlT@+So4R-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-js8DkZGrxa#UmP5V-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-uF3K!b1Is8pJpcUl-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001--rqbJMTz#iqMd4Pd-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-QUVNneWfwlFBjgLl-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-zAF5l2O5sHC8XKbM-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-0jkz#-9glVWRJ5j4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-vnqfPoYXtYiaz@Au-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-O@N3Y9wEhiaoGZ0n-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-gje1aUVnmEi9E-EN-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-yxjUOrdk2ct8PKFf-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-88U9Ry0uoi3qP#x4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FZ#96Jg25-GG0pnB-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-AcRY+Q66ADlut3AV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-OTBDxs4TOJcdRhtN-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-sb3AS#on+Q+Udk9a-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-T8SNCFKzZuHZ27Ma-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MU@Ak4r1KOMvwaTh-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-y#W!V9WVY##PEAY1-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-aD#lp2qSB@iqkIsm-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-LV0YQa!@WHxM-W8H-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ZdfdHef7FNI-InsS-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-SMXmexgapeEj1Kbu-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-y9bIxV!gaK0ExFTv-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-+wmW-Cq8TH5wfZZz-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MLVZwRWrUg0Q8xy6-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001--xy-g4Mjg3bLhHCS-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-7Kozz-HknqZq0b9i-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-9@Z5-Sa5c+uJhC3M-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-CLkebwiWSai8KQz0-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-0UGrn-Z+x1H7MxzH-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-exoR3NNM#6UQVseL-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-nuMGspIvN!!tZC1D-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-iLbU-0CK9PSGTd06-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-LwQj9ZGcMmdl5A4p-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-l18wZ5EOEfufxZ!r-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Lnt-nO#hHNelFjya-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-YmKZ+79hI1eGgtFK-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-wLSe@iuIXs86N+UC-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-XOXTs4HifR9ttm4e-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-elV!I+1w3DX8cMu7-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-+GLCL-vjUOSSdvsL-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Nw921!IHVxyH#Tyj-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-jRDsUc9+LCtNEbIq-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-QxH9+tlBC7MP!!Sx-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-!F4qpevygf7DGn3A-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-0rzUViv9tHmmPMzg-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@NTF@s9uE8xb0kef-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-zz9Ypz73@p4G3wbk-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MD7-!J-4bDA#G4ti-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FKxoIwomPoUsiFgs-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-O5FyOCCdGeLQ8u4m-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-YFlyig2-kk8cWja3-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-6ORi#--ZLKjuW31#-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-wlfMHcDmHnW+4kk7-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-1faYQk!x4Q-EzN1i-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-PRKR8S33RDjeOA69-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-#VgRNHylRNz-#jeU-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-JhffTMOFJqbFwDDa-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-U87Oh3yKQ9VBp9KO-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Wl-HNgo1xjAu0@h!-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-r1qzBf@k-1bJHulK-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-3kBFBp7ZgCmwz!rk-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-OYfEHFHF5BkFnNqq-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Hio-b1JW!gkMzTmI-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-3w7f0-Y6qiw#CZyH-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-l4TlnWdBY6RAdfvV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-wcM+Ta5KuY9HGGN9-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-EqD9vmnbug+LUnKE-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-vpy@353vVPHq1iKt-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-BFtvIZxh07o8FGKw-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@l1+FAelVwOQKjpU-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ncv0BP878eWvL1do-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-!0CLn4f6lI1ey6FV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-OHKcwqj4YG413@qo-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-IVpLwk93bVr1D27b-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-HtwviG8LJyKXTjO#-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-x2dDd!YYvkRHBsQV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-gh8M4F#SeUpfWEZx-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-juvH+4hz3z0@gKvh-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Kv9z4z3Ej#l@lpF+-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-mkJlM42YScRcz4zS-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-rl0hr6442B0ihgWs-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-NyRzPe@LWlfUNMhT-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-lUE!Wr1ZTKiFV3hN-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-GzY2Xrgvao6IBZGZ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-hU2Dcp4YcLXnTa0w-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-NJtRfjkK@nAlftov-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Ht9F++JmPcRr8uUT-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-!b663d+PnjpFIipV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-a!eKFZspp-rFDvZc-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-gHx@D8m6qnQJB5+B-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-M3@E##1wxaP0RfiE-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-9xU6Z1-7h9xg8s-Z-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-3StVp#EpRgl7dunU-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-BOL9mIQl7-L7P37@-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@WGYvmihSRrnRaBh-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-cwelcI8ldb9A7cDS-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-#dUmA2ujYn+!o1zZ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-kfciOTBe4NK6sNkR-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8HNZHJds@Cr-RkhC-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FHCaRAd-lwz8SOTw-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-6IdgIjbALIKm-oI@-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-jSujln4EGR8LpfEF-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-vHbuSD7AGchPvdIT-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-u9N7fWk0SeVeBYHr-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-w866Wm7+HRg5bbWR-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-W9bC@d3BujXK#7!e-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-!QtkKUNyfzOSFRqv-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-J2!kb!+okGHFfTNm-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-cY+r1JhpASrBCGKz-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-nxXPG8ejpj4IgD13-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-5GRiINd96-ERpx@K-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8UGXGPI@-alPyeTY-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MFUVgW4h-FJDwt7F-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-tlyDpknf1SUpgtZs-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Sie7+CDIVBtWB4pE-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-OYv4J131Z4U4z4IH-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-7Za5vNWiIy6J5hBJ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-nDCqMwJITVaBn1qu-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Efjkxf5G6OeXe9vX-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-G2yzNPUeEKDo3dbf-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-WXQT55#Zyp5C5Qez-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-#EZ8-ucdOfulZBhA-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-tVv4ffPivdtr68e0-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Oh4IspQLV2SvUd+B-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-BHv959r46@@axUy4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Cs0o2c-corxin-!0-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-BWdb9VB#4S#lJWo4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-XSEi7ZMMpHLjv61j-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-rOkDt+FWt@JDlmjq-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-oxEorYB93j0Fy+ZB-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-A0YuU!Db2wUEH!DN-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-dbIsiQYot2I!gdHH-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-x9kGagH9wAMmpjC4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-tzcfGWuXhoMiMedT-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MRQBUKH7Q@UhCldA-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@MKVvslZ0Jk3FP7j-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-AqQIoIkVAOxTySM@-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FctfLYSANzsKKcu8-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-U926MR4po7FfHZXw-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Z!u5WqSEAqidWto3-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-B5CE7unMCrqwpjpm-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-lOapdLtB0wkdkg3G-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-JMlqeGr!xaJdS62X-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-xhiR7G0yebihqC7B-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Zbt45STjHivaeZTp-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-m8HrB-CVyLpOnLV!-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-4YWLbwJGh2p1x6L4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-0uzqocp8@DSEOw#4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-2nkGTWVbqiE1BW7y-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-RGg0u9Z7J6Q-QZoZ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ytqtb5zC89MQfqNf-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-VY#y-8v3Xualr#kK-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@xyBDznXKk#ZFOzR-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-9oo41INABNUvqgPI-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-2keFicokEDmy#B9Q-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-37DeGLFtyA#5Wnk1-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-0W@mizzt9zj03n8u-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@G-YYwhBlCtHPCfg-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-XZsjC6xoamuaecsM-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Eo5WiZXxi8EB+3j4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-SlE6iS1sJfsEP-UZ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MH#KoFkKCC!Wh!bb-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-fudl7tq2KihMw@1E-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-K60ffWz3hs@G+N+Q-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Ke3AWTRgidM7O54d-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Swa89R!IebV6MWJH-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-b!cD#-iedr+mehuV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-4xfu4AaN1zmlqdsP-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-0XRkUZVdnrTzvPIx-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-74sxye0Bm5WwJ4Wv-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-DrpNs@DWnI!SZWyh-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-QCD1owO4cAoe5sp!-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-iH!35h+e-!7LEa4E-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-bn4AnRto6vzY-LyI-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-J8U2pG5FYbB988A+-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-h@kM4#IF3QoQZ6v4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ox-TDd6#S-xgkmS1-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-LoHO4BfM18jK0D8j-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-cPlAW@WBgAWeF5-A-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-wNoST!11m88nDe56-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-W-kL+3ORMf@cLPRr-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Do0pe6K-FAaq2Fz+-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-3Va7x6RaWcGMCx#U-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-xvlMTBDf3H14XV8T-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-HxVfNxUJukgfji#z-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-9m-2uDdk@ua7a8MB-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-jl@NtpMSKStCnTlY-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-mGyaZQybQrc@IK!0-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-FqrGESweL@MU1moX-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-NLk02h+2m#s0i#VL-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ynp9A!O3-Xhi2dyW-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Xiv+GT0F7eQLs2yo-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-WhBqiMdx9HuC0MrY-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-cyEssgGeqWddzbc0-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-hwR-Yn7KSsTy!nq1-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-6XlKGs!m732ygG#r-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-mAaZeiRPUlS4+yDE-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-0wzoPFsrDzeAh2Hl-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-1GJoAELOWiFRSHpm-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Tn2bukcLQLNTfAHk-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-U#Vx4RprYXSviVTq-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-a-71JWWqtxUI#@Ii-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Frsr8C8Kz6sSi8IC-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-X0JUlwuBDHagG69--AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Vx!lem21FIM8S66i-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-xouJMGDSck#qx-#p-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-08Jk3R@D3+qn1Dpa-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-nnO0BKdFLHGyiZRH-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-BA5LKCNmddw6tWOr-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-vj-3GdC5XFxnb1g--AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-EvHWzSe#!9!l9Pup-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-uZHknw#WRp5uah1w-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MiSDZ60Whz6XY8M9-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ZrL1IN1Acgm6@RO5-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-gh8P5zWoTBiFsprm-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-jGrv6F#zVL8oAufd-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-3g3VWWa2gcg3gkhe-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-J4UOSxWtaioiS2vN-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-J#a+tZKzty2R2gX3-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Y@EWh0OX+-PNi8-y-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8NyYF21ZZnPbI8M0-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ELwf3Dm2x-B5iq6u-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-z5pl+6o-619d8vWY-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-G-3XJZSBIffLM1#R-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-gaco#W3d7j6ijb+J-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-kWAyJWD@7BJOxrVj-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-IN7DkWPM@T8VY@@O-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-E4HcClG0xe4rn36G-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-J#bJT#93BAx2JUdF-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-!pu8GI639AcLYE0h-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-PdIObv0fuDCZecvd-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-owufBK8TgFDRDmoD-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-e4iYte66u2m51uM9-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001--VobAQuJ8WlYfxDH-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-seyvTBFAn3ffg-NT-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-HY1j#2ZaoFTSr-I#-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-TAj6zQg9vr7Z@2-9-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Q@u4ZgZykfNqGF4h-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-u6FSziJeGPbXbZNz-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-L@pWswDXP-qZPWhD-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-VXSXqGNC+QgwoXvV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-QQa3a6dFwj!s6FBk-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-bpCVS3WwD7S-iEf8-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-fk5Z9-t0dPLYoYuy-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-B0FjRIMkSpE#t@@j-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-w@Brx42GOf+TH4NF-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-pefDLFuv-K-H@p7h-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-SY5uXSZF0Ch5JdrR-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-az00xEstSMi8BZBA-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-hU-!EqYtN0O1Eq90-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-u6Yyx!mjjlmPQ4At-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ii0S-5IPfYiPUMos-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-+Rfuaj8yyfNlG0ec-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-odLA4h@mAhjpJnP5-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-xNVAyBS-sTUwldmu-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-KHG6zBd@BJI@yOTu-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001--qfxI@nGqThE+!iu-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-tQ+UgU2rLwr7jVYI-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-lD7g@Fxyo0nI@PGz-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-PHLKhso7Kxa4yNqf-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-VDAq0nqwwEmihtMZ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-M@7W+vqLRNvLT@Og-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-HG@d42Kp0T+D@DPg-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-gyLe8E7uWjk0GBT#-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-KfQzgrHCXOU99!tV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-G@0jQP9Px6+vvZNw-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-QXeuT1@@GnKuIGzo-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-o4gt14!o0jwUW0uB-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-BmfUwJGj+l-Sn4bn-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-HQlgeF-b@zKvNxgE-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-mN#oC5QXmRyc1wwK-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-eyUA4@J4pS-Gz!cB-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-5i7+gDB3rC5An!2C-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-lUEU1VoxWF8FzeJH-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-yfKNtbRTZNfT9iJ8-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001--A2ueo8bPD1yJw+e-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-LmpKF0-l4-3sh9Me-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-bhAVx#sdsn!0XdT1-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@RpVNwJ!51SqemoF-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-wxVH6jfB7xmdJ2Q7-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-VA+zUc8yWG84EZgI-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-dIyc7W53czeI@tRE-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001--xQ!rA+lLYVjlgAs-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-NLz!Jrl+amX7CzOA-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@eELrjg8aGfDHz0s-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-d9--UaEeYehtd4-A-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-iZJfgqRDdJBBMo@@-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-l1#!c@i1c!@kSUhE-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-jJmgbIT6uQki5v2g-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-QIcqqDXEu2V2tBcL-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-4iAiL5e3#7No-oA6-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-6owbNIFqzSyy4dKn-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-lcIlfNM9JZG3njn4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-qz2W8X2sd+q2-0Q0-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-nv9T5DWuxAvu@D90-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Gk5EESso!mkcjfJQ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-uJHWAafKoi-wwjJE-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-9+GuLH2eMO1TGSNQ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-g1sfEehlCQcsB0Ip-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-q9GEX9Zerq7pU8n--AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-XoCQE2Tk-gdApUfJ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-+HwhAFq2d4!zWFVV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-iH0nUsSHwPxxgls6-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-!XLqZGLOOfPWs66g-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-9YrmALpZTY9EN#vy-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-A@#3cq@Yd7@E+b@b-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-9wGTVwNUTYJXV!8#-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-6tVrNHa1U+@sf3gJ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-YLoy9iMchPw1#MGw-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-xURzb07jQX2yOVAt-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-brN1wM@JzmCA8byc-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001--5!K95lSGR@yovRr-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-cCjxXOglHduCv9w3-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-m20gZG@Z@lJ#2EHh-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-6-YdoOfbaevbFUY2-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-XcS01EZFILm6FKU3-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-SuxET#m21dmCZuGZ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-5gCEsWXez9Peoikw-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-vLyDs1x32!P+rkEK-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-LHxWvS8maNR6fbnB-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-0NFyjx9OYZQ9S6QL-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-k4v1mrdFQZuEnpe1-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-IFJ2IvR68356Ccpq-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-kmQoV0p9N4LSoS67-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-be6YjNw!oS+hcAwP-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-LAIkQz0xJ5!CKyt+-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-PztrmPu1-+m7L6Q@-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-i@dTbbUYmD9N57BS-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MB1qkrxn9iIM7I6!-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8oWgpH9XTOFx9t+4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-F+sa8EqTbUW8pukc-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8QyLTNGDh@uEw-b--AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-9vQT#DoYVL6Hwx+M-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-#cHw8cem0Rn1s@@w-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-cbF93D2Jao46yUVe-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-rDNYp8w5UJ4wW1z9-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-R-YUezGARzLX0Xv#-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Q7RIQQYHGYz2KN#l-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8Mi5AZjuOPU6z9sm-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-GhWDAGklGRLqnLul-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-lR2idxl198+#nkrA-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-+P6s62I@3LlBUOf#-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-37-oCLl1hwC7m9sj-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-z-QNuup0qoJO+Qp2-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-k3I62lhsuPTRkz3E-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-F9GK0tO7FpG-ualL-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-AUQx3w6ZkF8Gm!Ym-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@iN0!bmoB4RXT!1G-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-pN@2OQi-VRe2bxTx-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-79#67OkEte9ALqgh-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-e-l7EQEtK2UcvAqw-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-wrAW@gC4s7DsD!Xw-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-WXGhv8Z!@M#eFmsM-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-03Omsagyapy16MY1-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-YACI19iLoNnIEE52-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-DXyWPnPtRN#zQicJ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-uyic9h2NtH77!It1-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-GXNXoXBiEp2Yg3lx-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-w9IWkPe8Sc6YS1AJ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-D2myDk@n#Ml8EqES-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-j1e+tE+jdLSHk8UN-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-gOvJriJd@ybsmvcc-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Y4y4YEyvaLmbEOt2-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MJUMtxrS5oz6WhoT-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Ik!DhOlzp+tfmTan-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-eEY1UxZaYHPhD49z-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-hC3lnIL+x8O@f5BI-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-qaPm8R5pHettJT8d-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-pyH+MeIDmg7Z1-#c-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-sCSbqWzE-ARdSSA6-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-z-v@XAMqGXNo7c#3-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-j2kuT7EP2sYDH3!p-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-aPJGAXmCCqtYl-k9-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ILOKclJdyWkBD#VK-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-AMLj0HmWfprz0ZaT-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-P!7t8NZfpUJ3bIM#-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-eE5y+hnjho!adHzZ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Ktftt#jWLuiimCe7-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-tdCoRTxKgZCH1lSr-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-E#dcxjblQELsmmi2-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-YJWfW527@HWZT9!l-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-LmlGMUKU3X8cwl#w-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-QlcGS62XlMn4m+se-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001--ERc1WKMLI8TX9Qx-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-tHX@JLXh-O3UVDm2-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-DS9QEPmqN7G4qp8t-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-txPlByvIsgAL@+i--AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-pbMxcQ76BCA!W4tV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-HUsiSQTYxLBKGUpM-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-#qpY6tbEkiIRAdPy-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-hgXsOSr2a7Ye1Ixb-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-CxFvRDq9lDxJqc+o-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-6lLRyEb2gjJLYZBO-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ja2kRPXeyq7jWG3z-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-D6-x3L305Xa3#pwv-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-v+7AXLDlM94EO0kM-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-jezCv#7AkpjSweHC-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-f-!-QgsroDQ5RnIi-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-3meVt1nc77D1BSUJ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001--KsUhoNQwwEI!pCt-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-9M+H22L@qg2yQM6+-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Vfs-IZLnF@9+-2e1-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-jnABTbFO#hFdrxJl-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-R6C0WVDgDjUp@jfk-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-O9tfOsnT8bZNbFU4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-HlVAr6B8pUfNwdg5-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-dOFt6mJ+hnuMCd#Y-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-eJ-GivcaE+MlMnWu-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-R!KLfCj16TL-M9Ir-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-6yBgaqWiacQYF352-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-DJetD-6#wNqIXIEH-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MEVBo6MtD-Qc@!Cr-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MTN36crLAmzsHJ39-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Z!F4z@a8zRbP85y6-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-SoMAlzrqUv7zaS7f-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-sYRb@QMx3Z81CX4z-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-0lYkS7#M487mGqNO-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-jjC5I6H7a6mSdTCc-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-xOhPR@6ruSXTIhTC-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-1ZJw5oBdc4LjLVA0-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-V4FExdLzNnlMw4ch-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-#K17WJhiICelURuO-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-WRjquBn1ZBlHaSak-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-zd5xJJ7gejpdJGIR-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-3RNish6kol-cNPyy-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-RJU8df@NKnKjxwv5-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-h3AAAXT@7XwILAj2-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-t7apD40Np06Uy04s-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ETdz@H28K9@pBGWo-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-WF!O5SPscyanxQtz-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-QXPVWEjCu@i!eCS3-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-BJlRJpduLRq9gGzT-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-PbYTlj-0GPk0Af95-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-CDvUTxub!CA+hZmg-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8U5r@zhVQ#@yYETm-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-dK1htjy!7WzSDXfF-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-VNc2RvRsSQKsEn@t-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-LWca6Cw6n!t7SDXS-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-nXlaPnYj4x@53VmW-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-xrAnacb3mySxGeC5-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Q+-QR!ByXoHj3fSM-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8+Trpj3sePAd7Ibv-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-eYfRaHMfziUdf+Gk-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ZDzsSdN15-laHuc6-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-nAYusfrEXojrBdFI-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-rhe#M37921K@1r@#-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-vlwQOe6hDIWzY2gd-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ydUQVN3w8fh2xFSn-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-iZCL+OP6-U#y9iW!-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-nILheWRrJ41Dp2YN-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-4YcupuTeiqaYV+!v-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-PfE-mMS@Q4yQJJCP-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-mxcw3Tpsy2Fwj#Ob-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ROBt8wFK9lHMgK8l-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-nSczz4st8uym4rHB-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-10piVD5QD9Vbxza+-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-XRbrl0M!VvXZM3MI-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-B!zp8TyWET1EVoVo-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-3N-VSjJCtN3G2cIt-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-KKYUu@PwxBUVWsRe-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-s6wp4pQaRIltm!c5-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-SbAwQTUkOy+N9Hm+-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-bkVkWP!f#iA-BcrD-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-mb3L+-QqJNRcDX9g-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-35myx+z@reH9nhYT-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-1TwPLgJYZ9l8SjiK-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-nQNH52IIsnol4Z8X-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-RN2kCvb#Tri!@Or8-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-1Wn8xMnsI!G!S93v-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Gcw#Lez#N249psOZ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001--AiyAjt-WsRaWQ3b-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-217EsUl!sXxkVdZC-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-xzY!xokKNG##PnO@-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-KInvh3RDbnPMB0tx-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-WE@eyIDzX#o+uzs7-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ks4buSk-h3R22xgI-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-kM70sRyPoWHMngN--AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-2QTj-@-rZvNM0dvm-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-dQEDoq7vtghQ4EtV-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-#pDmmEjJokIL-PsR-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001--hQhg0pBDoh8lIle-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-dI6sKTSsz17@7v1d-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-fjvoAs46aLyyj2XY-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-eob#H0eU8izfKzy2-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-9eWkKr5rAwiKIRlQ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-z8SoE+6eRrD3Qh6r-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-nh!GCRiWQHJrdVSg-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-iPd@bWomk9zG-7Az-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Yd-#SqGFX!ss5n6s-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-J+YxV2b1!nAhzo!L-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-F-ddHNAHhtGwOLLa-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-fi9VAz7JS6qadOrK-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-#RnkuAOoRBg9-UBn-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-zSe5rhl8QwAPxPui-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-PGp1TVrPPQfX3UgG-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-H0!xnWEobl!GGqP--AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-k!t63cK5Q7FhIvs6-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Y-2R018uh79wOk3a-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-IvdhiG!X1kl8A4He-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-G2+Ej86PFV1DkLr4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-AtflVYRWsNMm05Zs-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-gF1IH-xZr4GIRMsc-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-QA3LwsrBAAfFn+0I-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-UBLkQWXiw9V57WT7-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-L-eHddeSHYqjkde9-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-HDvETQJMmisUInnm-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-MiCw-vx82brDEDcv-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-kHD#dYAAM!QkkJG4-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-0o+LBReRkCni7cV@-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-dXxfFecBQz6O7AqQ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-P#9vgG3rUAM2YDax-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-s5@eAe2EjSNSjrR!-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-LtCK1QO@hETa4Nah-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-QfsBwo38atfd1-Mv-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@uwA88PGbaj-R2!m-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-cTL7vvELWKfmj+LS-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-cUX#UHZj@JZFiBfA-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-nRAc!tPfH6BW9Ws8-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-p#w6q-owLuf9x#p1-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-cpFt!nHVU46k#BfL-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-dJaMk0bvBmES@hO#-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Ecib+dg8bRR#W7Mz-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-V+SKHdMhL9yx#Fxa-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-sw693EB+1Llb0@8m-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-VCvCKj#Uxyoqw+Z3-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-BZx5#R5Qzj41wLJg-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-XFDr6YG2ERnOFVHw-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-I0QXqnGb7@q9!qdd-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-mCkQ@-6jec2hjr+k-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-TvsuXOvSqgcTs8U1-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-UjmZkSmvllFx2w7o-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-OLNSdXg-YcbrsWK8-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Du5qhQCw5TdO6uAb-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-h1aJ7TjXktB0j1Qy-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-2@R3hM!-vw#Tb3ja-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-4bWcESV#F6daJ4hZ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001--s1rekqQKMZbrz-@-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-KIGVVw4b4VBuP29c-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-O0gPLm9J+OQdJYUQ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-9bGKY6gVFsRF88k6-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-rRfaadl+zQWBXof8-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-YW-5Q90petx6kEVI-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@M4rJpvwf6+JAc@F-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-kY2ZgaAQCNNIs+p#-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-J@IXMy5tV7GesxLZ-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-aMEYhPzbWengP2Ng-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-YGiVSvTdJ#OLqU!#-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-fVeFWyYQTENOw4v3-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-ELraSle@WW18iLWh-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-sFw@hge-qtGbj6B0-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-5VFy@eNsVWNa9eGa-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-OIo#rde6KuVlmNZ9-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-rGC8zBoBCT#ag+38-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-c4azj6wKjGsO49L3-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-LHv2NE1mLogIR0Si-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-A2qQYQONrIyuxUEo-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001--6STV1enkzY02Bs2-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-dZyQK7vqMOf7A2fi-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Hi6XCi!Ndj1TmR@i-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-z5xl287BfVCJH#89-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-lWjv2WRnpbTP##tG-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-hokjGi6sIcU@J85d-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-8iNqTlgccf@L#kMA-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-VBHercdlcNDb6bGM-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-Hx9+WE-Fl!toUUh8-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-@5mVxEMMZ7iMmEnK-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-7JIeUR8iCGU077AW-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));
INSERT INTO limited_user
VALUES ('CashReaderKey-AGM-01-001-b52782ZY7+iNxGge-AmalGlass',
        (select id from bundle where bundle_store_id = 'com.amalglass.amalglassmobile'));

