create sequence hibernate_sequence start 1 increment 1;

create table bundle
(
    id              int8                     not null,
    bundle_store_id text                     not null unique,
    valid_to        timestamp with time zone,
    created_at      timestamp with time zone not null,
    updated_at      timestamp with time zone,
    primary key (id)
);


create table license
(
    id             int8                     not null,
    license_number uuid                     not null,
    fk_bundle_id   int8                     not null,
    user_id        text                     not null,
    created_at     timestamp with time zone not null,
    updated_at     timestamp with time zone,
    primary key (id),
    CONSTRAINT fk_bundle_id FOREIGN KEY (fk_bundle_id)
        REFERENCES bundle (id)
);


create table license_history
(
    id            int8                     not null,
    platform      text                     not null,
    fk_license_id int8                     not null,
    created_at    timestamp with time zone not null,
    updated_at    timestamp with time zone,
    primary key (id),
    CONSTRAINT fk_license_id FOREIGN KEY (fk_license_id)
        REFERENCES license (id)
);

INSERT INTO bundle
VALUES (nextval('hibernate_sequence'),
        'cz.hayaku.cashreader',
        null,
        now(),
        now());
