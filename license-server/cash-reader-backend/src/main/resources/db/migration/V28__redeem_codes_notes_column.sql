alter table redeem_code
    add column kv_key text,
    add column notes text;


update redeem_code
set kv_key = lower(redeem_code);

-- Notes manually added into the Spreadsheet before they got into the DB
update redeem_code
set notes = 'iran test kody'
where redeem_code = 'IFD8UCZA';

update redeem_code
set notes = 'iran test kody'
where redeem_code = 'OO9YO7YN';

update redeem_code
set notes = 'iran test kody'
where redeem_code = 'M1YRU5RV';

update redeem_code
set notes = 'tsengel mongolia'
where redeem_code = 'MEUAILFD';

update redeem_code
set notes = 'tsengel mongolia'
where redeem_code = '3TA6AH3S';

update redeem_code
set notes = 'tsengel mongolia'
where redeem_code = 'X52WRT9T';

update redeem_code
set notes = 'othniel free'
where redeem_code = 'O9W3ZH5X';

update redeem_code
set notes = 'othniel free'
where redeem_code = 'VUJ1L95Z';

update redeem_code
set notes = 'othniel free'
where redeem_code = 'H261BMJK';

update redeem_code
set notes = 'othniel free'
where redeem_code = 'ZXUAO81C';

update redeem_code
set notes = 'othniel free'
where redeem_code = '63UT5IIL';

update redeem_code
set notes = 'othniel free'
where redeem_code = 'CUTNJ3KA';

update redeem_code
set notes = 'othniel free'
where redeem_code = '62LME1EY';

update redeem_code
set notes = 'othniel free'
where redeem_code = 'J3VFE6OL';

update redeem_code
set notes = 'othniel free'
where redeem_code = 'Q11645MS';

update redeem_code
set notes = 'othniel free'
where redeem_code = '7HGKA89A';

update redeem_code
set notes = 'othniel free'
where redeem_code = '2LKYGRC2';

update redeem_code
set notes = 'othniel free'
where redeem_code = 'LDV1VVU0';

update redeem_code
set notes = 'othniel free'
where redeem_code = '39HA55IK';

update redeem_code
set notes = 'othniel free'
where redeem_code = '8M0S02MJ';

update redeem_code
set notes = 'othniel free'
where redeem_code = 'M6AG1RH6';

update redeem_code
set notes = 'othniel free'
where redeem_code = 'IEZEZGJJ';

update redeem_code
set notes = 'othniel free'
where redeem_code = '5YCWM94T';

update redeem_code
set notes = 'othniel free'
where redeem_code = 'QGL71J3N';

update redeem_code
set notes = 'othniel free'
where redeem_code = '6XP4X8TQ';

update redeem_code
set notes = 'othniel free'
where redeem_code = '6DEM7YCP';

update redeem_code
set notes = 'Beth Wood Bedford School woodb@sau25.net'
where redeem_code = '65278EV3';

update redeem_code
set notes = 'link test'
where redeem_code = 'ENRBWVTP';

update redeem_code
set notes = 'test'
where redeem_code = 'WV94SPCV';

update redeem_code
set notes = 'test'
where redeem_code = 'DYUBEB04';

update redeem_code
set notes = 'test'
where redeem_code = '5R6J6UOC';

update redeem_code
set notes = 'test'
where redeem_code = 'I4HW4S2E';

update redeem_code
set notes = 'iran nove test kody'
where redeem_code = '0MW8S0BU';

update redeem_code
set notes = 'iran nove test kody'
where redeem_code = 'RXTHHVD1';

update redeem_code
set notes = 'iran nove test kody'
where redeem_code = 'KPYX91EF';

--delete old stuff, we will generate them in the future
delete
from redeem_code
where id > (select id from redeem_code where redeem_code = 'KPYX91EF');