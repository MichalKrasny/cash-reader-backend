
--because the created_at column does problems when artificially set in tests
ALTER TABLE license ADD COLUMN billing_date timestamp with time zone not null default '2000-01-01';

--postgres argued with me, that there is no where clause
UPDATE license SET billing_date = created_at where 1=1;