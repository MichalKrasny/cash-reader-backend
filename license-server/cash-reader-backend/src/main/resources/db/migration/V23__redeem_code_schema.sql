create table redeem_code
(
    id          int8 not null,
    redeem_code text unique not null,
    user_id     text,
    platform    text,
    redeem_date timestamp with time zone,
    created_at  timestamp with time zone,
    updated_at  timestamp with time zone,
    primary key (id)
);

CREATE INDEX ON redeem_code (redeem_code);

