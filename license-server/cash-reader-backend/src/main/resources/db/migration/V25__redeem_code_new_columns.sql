alter table redeem_code
    add column campaign text,
    add column app_version text,
    add column currency text,
    add column lang text,
    add column system_version text;

