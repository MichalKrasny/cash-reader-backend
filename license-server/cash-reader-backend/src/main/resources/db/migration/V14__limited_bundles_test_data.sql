INSERT INTO bundle
VALUES (nextval('hibernate_sequence'),
        'cz.hayaku.cashreader.limited',
        null,
        now(),
        now(),
        true);

INSERT INTO limited_user
VALUES ('testUser1', (select id from bundle where bundle_store_id = 'cz.hayaku.cashreader.limited'));

INSERT INTO limited_user
VALUES ('testUser2', (select id from bundle where bundle_store_id = 'cz.hayaku.cashreader.limited'));
