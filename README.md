## Project structure

```
├── license-server
│   ├── cash-reader-backend - original backend application providing licenses
│   └── license-server-infrastructure - Kotlin infrastructure for backend application, currently experimental
├── rates-downloader
└── rates-downloader-lambda
```
