import json
import logging
from operator import itemgetter

import boto3
from botocore.exceptions import ClientError

from common import tags_to_dict, should_be_deleted_by_tags

logger = logging.getLogger(__name__)


class IamFacade:
    def __init__(self, tags):
        self.iam_resource = boto3.resource('iam')
        self.tags = tags
        self.tags_dict = dict(map(itemgetter('Key', 'Value'), tags))

    def get_iam_role(self, iam_role_name):
        """
        Get an AWS Identity and Access Management (IAM) role.
        :param iam_role_name: The name of the role to retrieve.
        :return: The IAM role.
        """
        role = None
        try:
            temp_role = self.iam_resource.Role(iam_role_name)
            temp_role.load()
            role = temp_role
            logger.info("Got IAM role %s", role.name)
        except ClientError as err:
            if err.response['Error']['Code'] == 'NoSuchEntity':
                logger.info("IAM role %s does not exist.", iam_role_name)
            else:
                logger.error(
                    "Couldn't get IAM role %s. Here's why: %s: %s", iam_role_name,
                    err.response['Error']['Code'], err.response['Error']['Message'])
                raise
        return role

    def create_role_for_lambda(self, iam_role_name):
        """
        Creates an IAM role that grants the Lambda function basic permissions.
        Waits for the role to be created.
        :param iam_role_name: The name of the role to create.
        :return: The role
        """

        try:
            logger.info(f'Creating role "{iam_role_name}"')

            # defines, who can assume this role.
            lambda_assume_role_policy = {
                'Version': '2012-10-17',
                'Statement': [
                    {
                        'Effect': 'Allow',
                        'Principal': {
                            'Service': 'lambda.amazonaws.com'
                        },
                        'Action': 'sts:AssumeRole'
                    },
                    # to be able to get events
                    {
                        "Effect": "Allow",
                        "Principal": {
                            "Service": "events.amazonaws.com"
                        },
                        "Action": "sts:AssumeRole"
                    }
                ]
            }

            role = self.iam_resource.create_role(
                RoleName=iam_role_name,
                AssumeRolePolicyDocument=json.dumps(lambda_assume_role_policy),
                Tags=self.tags
            )
            logger.info("Created role %s.", role.name)
            role.attach_policy(PolicyArn='arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole')
            role.attach_policy(PolicyArn="arn:aws:iam::aws:policy/AmazonS3FullAccess")

            # TODO CHECK IF NEEDED
            role.attach_policy(PolicyArn="arn:aws:iam::aws:policy/CloudWatchEventsFullAccess")
            # TODO CHECK IF NEEDED
            role.attach_policy(PolicyArn="arn:aws:iam::aws:policy/AWSLambda_FullAccess")

            logger.info("Attached basic execution policy to role %s.", role.name)
        except ClientError as error:
            if error.response['Error']['Code'] == 'EntityAlreadyExists':
                role = self.iam_resource.Role(iam_role_name)
                logger.warning("The role %s already exists. Using it.", iam_role_name)
            else:
                logger.exception(
                    "Couldn't create role %s or attach policy.",
                    iam_role_name)
                raise

        return role

    def create_role_for_events(self, iam_role_name):
        """
        Creates an IAM role that grants the Lambda function basic permissions.
        Waits for the role to be created.
        :param iam_role_name: The name of the role to create.
        :return: The role
        """

        try:
            logger.info(f'Creating role "{iam_role_name}"')

            # defines, who can assume this role.
            lambda_assume_role_policy = {
                'Version': '2012-10-17',
                'Statement': [
                    # to be able to get events
                    {
                        "Effect": "Allow",
                        "Principal": {
                            "Service": "events.amazonaws.com"
                        },
                        "Action": "sts:AssumeRole"
                    }
                ]
            }

            role = self.iam_resource.create_role(
                RoleName=iam_role_name,
                AssumeRolePolicyDocument=json.dumps(lambda_assume_role_policy),
                Tags=self.tags
            )
            logger.info("Created role %s.", role.name)
            role.attach_policy(PolicyArn="arn:aws:iam::aws:policy/CloudWatchEventsFullAccess")

            logger.info("Attached basic execution policy to role %s.", role.name)
        except ClientError as error:
            if error.response['Error']['Code'] == 'EntityAlreadyExists':
                role = self.iam_resource.Role(iam_role_name)
                logger.warning("The role %s already exists. Using it.", iam_role_name)
            else:
                logger.exception(
                    "Couldn't create role %s or attach policy.",
                    iam_role_name)
                raise

        return role

    def delete_iam_role(self, iam_role_name):
        iam_role = self.get_iam_role(iam_role_name)
        if iam_role is not None:
            for policy in iam_role.attached_policies.all():
                policy.detach_role(RoleName=iam_role.name)
            iam_role.delete()
            logger.info("IAM role deleted: " + iam_role_name)
        else:
            logger.info("AIM role not found: " + iam_role_name)

    def delete_roles_for_env(self, environment):
        roles = self.iam_resource.roles.all()

        for role in roles:
            tags_dict = self.get_role_tags(role)
            if should_be_deleted_by_tags(environment, tags_dict):
                print(f"Deleting role {role} with tags {tags_dict}")
                self.delete_iam_role(role.name)

    def get_role_tags(self, role):
        tags = self.iam_resource.Role(role.name).tags
        # for some weird reason the tags are list of dictionary, we want a dictionary
        tags_dict = tags_to_dict(tags)
        return tags_dict
