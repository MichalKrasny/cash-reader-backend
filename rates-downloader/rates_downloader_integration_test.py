import logging
from datetime import datetime
from datetime import timezone

import common
import logging_config
from S3Facade import S3Facade
from demo_tools.retries import wait
from deploy import deploy
from fire_event import fire_event
from undeploy import undeploy

logger = logging.getLogger(__name__)


def integration_test(environment):
    test_start_time = datetime.now(timezone.utc)
    logger.info(f'test_start_time="{test_start_time}"')

    deploy(environment)

    # Without this waiting the event sometimes couldn't be fired
    logger.info(f'wait="{10}"')
    wait(10)
    fire_event(environment)

    tags = common.get_tags(environment)
    s3_facade = S3Facade(tags, environment)

    test_new_file_on_s3(s3_facade, test_start_time, "rates-v2.json")
    test_new_file_on_s3(s3_facade, test_start_time, "old-rates.json")
    undeploy(environment)


def test_new_file_on_s3(s3_facade, test_start_time, file_name):
    logger.info(f'Checking file "{file_name}"')

    s3_response_object = s3_facade.get_s3_object_with_retry(
        bucket="cz.hayaku.cashreader.test-rates-delete-me",
        file_name=file_name)

    last_modified = s3_response_object['LastModified']
    logger.info(f'last_modified="{last_modified}"')
    attempts = 10

    while last_modified < test_start_time:
        attempts -= 1
        wait(1)
        s3_response_object = s3_facade.get_s3_object(
            bucket="cz.hayaku.cashreader.test-rates-delete-me",
            file_name=file_name)
        last_modified = s3_response_object['LastModified']
        logger.info(f'last_modified="{last_modified}"')
        if attempts <= 0:
            raise Exception(f'File {file_name} was not modified after the start of the test')

    file_content_bytes = s3_response_object['Body'].read()
    file_content = file_content_bytes.decode("utf-8")
    file_must_contain = '"code": "EUR"'

    if ('%s' % file_must_contain) not in file_content:
        raise Exception(f'Expected String not found: {file_must_contain}')

    logger.info(f'SUCCESS! Found a file on S3, last_modified="{last_modified}"')


if __name__ == '__main__':
    logging_config.setup_root_logger()
    env = "test"

    integration_test(env)
