import logging

import common
import logging_config
from EventsFacade import EventsFacade
from IamFacade import IamFacade
from LambdaFacade import LambdaFacade
from demo_tools.retries import wait

logger = logging.getLogger(__name__)


def deploy(environment):
    logger.info(f'Deploying "{environment}"')

    tags = common.get_tags(environment)

    if environment == "test":
        s3_bucket = "cz.hayaku.cashreader.test-rates-delete-me"
    else:
        s3_bucket = "cz.hayaku.cashreader.rates"

    iam_facade = IamFacade(tags)
    lambda_facade = LambdaFacade(tags)
    events_facade = EventsFacade(tags, environment)

    lambda_name = common.get_lambda_name(environment)
    lambda_role_name = common.get_lambda_role_name(environment)
    events_role_name = common.get_events_role_name(environment)

    # Cleanup obsolete stuff
    iam_facade.delete_iam_role(f'rates_downloader-{environment}')

    # Cleanup
    iam_facade.delete_iam_role(lambda_role_name)
    iam_facade.delete_iam_role(events_role_name)
    lambda_facade.safe_delete_lambda(lambda_name)

    # Deploy
    lambda_role = iam_facade.create_role_for_lambda(lambda_role_name)
    events_role = iam_facade.create_role_for_events(events_role_name)

    deployment_package = lambda_facade.create_deployment_package(f"{lambda_name}.py")

    # Some Cargo Cult, because Lambdas weren't working with dynamic waiting, fix me
    wait(10)
    lambda_arn = lambda_facade.create_rates_download_function_with_retry(lambda_name,
                                                                         f'{lambda_name}.lambda_handler',
                                                                         lambda_role,
                                                                         deployment_package,
                                                                         s3_bucket)

    rule = events_facade.create_rate_download_rule_with_retry(events_role.arn, lambda_arn)

    lambda_facade.add_permission_to_lambda(lambda_arn, rule["RuleArn"])


if __name__ == '__main__':
    logging_config.setup_root_logger()
    args = common.get_args()
    env = args.env.lower()

    deploy(env)
