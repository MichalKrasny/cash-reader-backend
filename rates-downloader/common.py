import argparse

SUPPORTED_ENVIRONMENTS = ["dev", "test", "prod"]

TAG_KEY_APPLICATION = 'application'
TAG_VALUE_APPLICATION = 'cash-reader'
TAG_KEY_SERVICE = 'service'
TAG_VALUE_SERVICE = 'rates-downloader'
TAG_KEY_ENV = 'env'


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-e", '--env', type=str, help=F"Environment [{'|'.join(SUPPORTED_ENVIRONMENTS)}]")
    args = parser.parse_args()
    env = args.env.lower()
    if env not in SUPPORTED_ENVIRONMENTS:
        raise RuntimeError(f'Unsupported environment "{env}"')

    return args


def get_tags(environment):
    tags = [
        {'Key': TAG_KEY_APPLICATION, 'Value': TAG_VALUE_APPLICATION},
        {'Key': TAG_KEY_SERVICE, 'Value': TAG_VALUE_SERVICE},
        {'Key': TAG_KEY_ENV, 'Value': environment}
    ]
    return tags


def get_lambda_name(environment):
    return f'rates-downloader-{environment}'


def get_lambda_role_name(environment):
    return f'rates-downloader-role-{environment}'


def get_events_role_name(environment):
    return f'rates-downloader-events-{environment}'


def tags_to_dict(tags):
    if tags is None:
        return {}
    tags_dict = {}
    for tag in tags:
        tags_dict[tag['Key']] = tag['Value']
    return tags_dict


def should_be_deleted_by_tags(environment, tags_dict):
    return tags_dict.get(TAG_KEY_APPLICATION) == TAG_VALUE_APPLICATION and \
           tags_dict.get(TAG_KEY_SERVICE) == TAG_VALUE_SERVICE and tags_dict.get(TAG_KEY_ENV) == environment
