import io
import logging
import zipfile
from operator import itemgetter

import boto3
from botocore.exceptions import ClientError

from common import should_be_deleted_by_tags
from demo_tools.retries import wait

logger = logging.getLogger(__name__)


class LambdaFacade:
    def __init__(self, tags):
        self.lambda_client = boto3.client('lambda')
        self.tags = tags
        self.tags_dict = dict(map(itemgetter('Key', 'Value'), tags))

    @staticmethod
    def create_deployment_package(destination_file):
        logger.info("create_deployment_package")
        """
        Creates a Lambda deployment package in .zip format in an in-memory buffer. This
        buffer can be passed directly to Lambda when creating the function.
        :param destination_file: The name to give the file when it's deployed to Lambda.
        :return: The deployment package.
        """
        buffer = io.BytesIO()
        with zipfile.ZipFile(buffer, 'w') as zipped:
            zipped.write('../rates-downloader-lambda/rate_downloader.py', destination_file)
            zipped.write('../rates-downloader-lambda/RatesToListConvertorJson.py', 'RatesToListConvertorJson.py')
            zipped.write('../rates-downloader-lambda/S3Client.py', 'S3Client.py')
        buffer.seek(0)
        return buffer.read()

    def get_function(self, function_name):
        """
        Gets data about a Lambda function.
        :param function_name: The name of the function.
        :return: The function data.
        """
        response = None
        try:
            response = self.lambda_client.get_function(FunctionName=function_name)
        except ClientError as err:
            if err.response['Error']['Code'] == 'ResourceNotFoundException':
                logger.info("Function %s does not exist.", function_name)
            else:
                logger.error(
                    "Couldn't get function %s: %s: %s", function_name,
                    err.response['Error']['Code'], err.response['Error']['Message'])
                raise
        return response

    def create_rates_download_function(self, function_name, handler_name, iam_role, deployment_package, bucket_name):
        """
        Deploys a Lambda function.
        :param function_name: The name of the Lambda function.
        :param handler_name: The fully qualified name of the handler function. This
                             must include the file name and the function name.
        :param iam_role: The IAM role to use for the function.
        :param deployment_package: The deployment package that contains the function
                                   code in .zip format.
        :return: The Amazon Resource Name (ARN) of the newly created function.
        """
        try:
            response = self.lambda_client.create_function(
                FunctionName=function_name,
                Description="Downloades file with rates and converts to required formats",
                Timeout=30,
                Environment={
                    'Variables': {
                        'bucket_name': bucket_name,
                        'file_name': 'rates.json',
                        'file_name_v2': 'rates-v2.json',
                        'file_path': '',
                        'url': 'http://www.floatrates.com/daily/usd.json',

                    }
                },
                Runtime='python3.8',
                Role=iam_role.arn,
                Handler=handler_name,
                Code={'ZipFile': deployment_package},
                Publish=True,
                Tags=self.tags_dict
            )
            function_arn = response['FunctionArn']
            waiter = self.lambda_client.get_waiter('function_active_v2')
            waiter.wait(FunctionName=function_name)
            logger.info("Created function '%s' with ARN: '%s'.",
                        function_name, response['FunctionArn'])
        except ClientError:
            raise
        else:
            return function_arn

    def create_rates_download_function_with_retry(self, function_name, handler_name, iam_role, deployment_package,
                                                  bucket_name):
        """
        Deploys a Lambda function. Tries several times because it is possible that all the previous resources like roles
        may not be ready yet.
        :param function_name: The name of the Lambda function.
        :param handler_name: The fully qualified name of the handler function. This
                             must include the file name and the function name.
        :param iam_role: The IAM role to use for the function.
        :param deployment_package: The deployment package that contains the function
                                   code in .zip format.
        :return: The Amazon Resource Name (ARN) of the newly created function.
        """

        logger.info("create_rates_download_function_with_retry")
        attempts_left = 10
        last_exception = Exception("Invalid state")
        while attempts_left > 0:
            try:
                function_arn = self.create_rates_download_function(function_name, handler_name, iam_role,
                                                                   deployment_package, bucket_name)
                return function_arn
            except ClientError as e:
                attempts_left -= 1
                print(f"Unable to create Lambda, attempts left:{attempts_left}")
                last_exception = e
                wait(1)

        raise last_exception

    @staticmethod
    def add_permission_to_lambda(lambda_arn, rule_arn):
        boto3.client('lambda').add_permission(
            FunctionName=lambda_arn,
            StatementId="IUseTheSameHereAsTheRuleIdButYouDoAsYouPlease",
            Action="lambda:InvokeFunction",
            Principal="events.amazonaws.com",
            SourceArn=rule_arn
        )

    def safe_delete_lambda(self, function_name):
        """
        Deletes a Lambda function.
        :param function_name: The name of the function to delete.
        """
        try:
            self.lambda_client.delete_function(FunctionName=function_name)
        except ClientError as err:
            if err.response['Error']['Code'] == 'ResourceNotFoundException':
                logger.info("Function %s does not exist.", function_name)

    def delete_lambdas_for_env(self, environment):
        lambdas = self.lambda_client.list_functions()['Functions']

        for l in lambdas:
            tags_dict = self.get_lambda_tags(l)
            if should_be_deleted_by_tags(environment, tags_dict):
                print(f"Deleting lambda {l} with tags {tags_dict}")
                self.safe_delete_lambda(l['FunctionName'])

    def get_lambda_tags(self, l):
        lam = self.lambda_client.get_function(FunctionName=l['FunctionName'])
        tags = lam.get('Tags', {})
        return tags

    def invoke_lambda(self, function_name):
        self.lambda_client.invoke(
            FunctionName=function_name,
            InvocationType='Event',
            LogType='Tail',
            # ClientContext='string',
            Payload='{"arg1":"val1"}',
            # Qualifier='string'
        )
