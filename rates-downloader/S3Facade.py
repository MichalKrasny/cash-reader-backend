import logging
from operator import itemgetter

import boto3
from botocore.exceptions import ClientError

from demo_tools.retries import wait

logger = logging.getLogger(__name__)


class S3Facade:
    def __init__(self, tags, environment):
        self.s3_client = boto3.client('s3')
        self.tags = tags
        self.tags_dict = dict(map(itemgetter('Key', 'Value'), tags))
        self.environment = environment

    def get_s3_object(self, bucket, file_name):

        self.s3_client = boto3.client('s3')
        s3_response_object = self.s3_client.get_object(Bucket=bucket, Key=file_name)
        return s3_response_object

    def get_s3_object_with_retry(self, bucket, file_name, attempts=10, wait_time=0.5):

        while attempts > 0:
            attempts -= 1
            try:
                result = self.get_s3_object(bucket, file_name)
                return result
            except ClientError:
                logger.info(f'Waiting attempts left={attempts}')
                wait(wait_time)

        raise (Exception(f'object not found bucket={bucket}, file_name={file_name}, attempts={attempts}'))
