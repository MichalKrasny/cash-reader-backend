import logging

import common
import logging_config
from LambdaFacade import LambdaFacade

logger = logging.getLogger(__name__)


def fire_event(environment):
    logger.info(f'Firing an event, environment="{environment}"')
    tags = common.get_tags(environment)
    lambda_facade = LambdaFacade(tags)
    lambda_facade.invoke_lambda(f'rates-downloader-{environment}')


if __name__ == '__main__':
    logging_config.setup_root_logger()
    args = common.get_args()
    env = args.env.lower()

    fire_event(env)
