import json
import logging
from operator import itemgetter

import boto3
from botocore.exceptions import ClientError

from demo_tools.retries import wait

logger = logging.getLogger(__name__)


class EventsFacade:
    def __init__(self, tags, environment):
        self.events_client = boto3.client('events')
        self.tags = tags
        self.tags_dict = dict(map(itemgetter('Key', 'Value'), tags))
        self.environment = environment

    def create_rate_download_rule(self, iam_role_arn, lambda_arn):
        """
        :param iam_role_arn: iam_role_arn
        :param lambda_arn: lambda_arn
        :return: rule.
        """
        logger.info("create_rate_download_rule")
        rate_download_rule_name = "rate-downloader-rule-" + self.environment

        # rule can be updated, however tags are ignored during update
        rule = self.events_client.put_rule(
            Name=("%s" % rate_download_rule_name),
            # daily at 01:00 UTC
            ScheduleExpression="cron(0 1 * * ? *)",
            # ScheduleExpression='rate(1 minute)',
            State="ENABLED",
            RoleArn=iam_role_arn
        )

        self.events_client.tag_resource(
            ResourceARN=rule["RuleArn"],
            Tags=self.tags
        )

        # can rewrite
        self.events_client.put_targets(
            Rule=rate_download_rule_name,
            Targets=[
                {
                    "Id": "rates_downloader-id-" + self.environment,
                    "Arn": lambda_arn,
                    "Input": json.dumps({"foo": "bar"})
                }
            ]
        )

        return rule

    def create_rate_download_rule_with_retry(self, iam_role_arn, lambda_arn):
        """
        :param iam_role_arn: iam_role_arn
        :param lambda_arn: lambda_arn
        :return: rule.
        """

        attempts_left = 10
        last_exception = Exception("Invalid state")
        while attempts_left > 0:
            try:
                rule = self.create_rate_download_rule(iam_role_arn, lambda_arn)
                return rule
            except ClientError as e:
                attempts_left -= 1
                print(f"Unable to create rule, attempts left:{attempts_left}")
                last_exception = e
                wait(1)
        raise last_exception
