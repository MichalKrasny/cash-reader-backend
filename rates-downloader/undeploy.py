import logging

import common
import logging_config
from IamFacade import IamFacade
from LambdaFacade import LambdaFacade

logger = logging.getLogger(__name__)


def undeploy(environment):
    logger.info(f'Undeploying "{environment}"')

    tags = common.get_tags(environment)
    iam_facade = IamFacade(tags)
    lambda_facade = LambdaFacade(tags)

    # lambda_role, events_role
    iam_facade.delete_roles_for_env(environment)

    # rates-downloader
    lambda_facade.delete_lambdas_for_env(environment)


if __name__ == '__main__':
    logging_config.setup_root_logger()
    args = common.get_args()
    env = args.env.lower()

    undeploy(env)
