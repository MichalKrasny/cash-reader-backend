Rates downloader is an AWS based solution to download current rates from an internet service, covert them to desired
format and store them to S3.

There are rates that are updated daily and less common rates that are updated weekly. Weekly updated rates aren't
included in the response from the internet service and appear only once a week. To cope with that we store rates in
file `old-rates.json` and when new rates are updated, we load this file, update by new rates and store to S3.

tags:
'application': 'cash-reader'
'service': 'rates-downloader'
'env' : dev|test|prod

# Architecture

The core of the system is a Lambda rates_downloader, which is triggered daily. Once a day a rate-downloader-rule
generates an event, which triggers Lambda execution. Lambda downloads current rates
from `http://www.floatrates.com/daily/usd.json`, transforms the file and stores it to S3. Cash Reader mobile
applications can then download the file and use it.

![Architecure](docs/architecture.png)

# Infrastructure

Infrastructure is installed by `deploy_lambda.py`. This script should contain only high level operations to be easily
readable what AWS resources are being created. All details should be in the facades and other scripts.

Lambda itself is in a different directory `rates-downloader-lambda` to be able to run its own venv. Lambda uses
different libraries than the installation scripts, it is just coincidence that both use Python.

## Installation

Run in Pycharm `deploy_lambda.py --env=[dev|test|prod]`

## Installation

Run in Pycharm `undeploy_lambda.py --env=[dev|test|prod]`

## Requirements

Python 3.9. to run installation, Lambda may be using different Python version.

# Developer documentation

## Scripts

Scripts that are to be called directly start with a lowercase letter. They should contain only minimal code and logic,
they should ideally be only a list of steps. They are:

deploy_lambda.py undeploy_lambda.py fire_event.py rates_downloader_integration_test.py

## Facades

Contain steps and their logic. Their name should end with "*Facade" suffix They should be a facade over boto3 and hide
the implementation details.

## Integration Tests

There is an integration test `rates_downloader_integration_test.py` which installs Lambda and the infrastructure to the
test environment, run Lambda and un-deploys the test environment.

## Unit Tests are in the lambda itself