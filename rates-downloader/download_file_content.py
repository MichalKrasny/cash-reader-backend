import logging

import common
import logging_config
from S3Facade import S3Facade

logger = logging.getLogger(__name__)


def download_file_content(environment):
    logger.info(f'Downloading file, env="{environment}"')
    tags = common.get_tags(environment)
    s3_facade = S3Facade(tags, environment)
    s3_response_object = s3_facade.get_s3_object(
        bucket="cz.hayaku.cashreader.test-rates-delete-me",
        file_name="rates-v2.json")

    object_content_bytes = s3_response_object['Body'].read()
    file_content = object_content_bytes.decode("utf-8")
    return file_content


if __name__ == '__main__':
    logging_config.setup_root_logger()
    args = common.get_args()
    env = args.env.lower()

    download_file_content(env)
