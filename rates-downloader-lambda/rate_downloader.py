import json
import os
from datetime import datetime
from urllib.request import Request, urlopen

from RatesToListConvertorJson import RatesToListConvertorJson
from S3Client import S3Client

SITE = os.environ['url']  # URL of the site to check, stored in the site environment variable
BUCKET_NAME = os.environ['bucket_name']
FILE_PATH = os.environ['file_path']
FILE_NAME = os.environ['file_name']
FILE_NAME_V2 = os.environ['file_name_v2']

OLD_RATES_FILE_NAME = "old-rates.json"

rates_to_list_convertor_regex = RatesToListConvertorJson()
s3Client = S3Client(BUCKET_NAME, FILE_PATH)


# There are rates that are updated daily and less common rates that are updated weekly. Weekly updated rates aren't
# included in the response from the internet service and appear only once a week. To cope with that we store rates
# in file `old-rates.json` and when new rates are updated, we load this file, update by new rates and store to S3.
def lambda_handler(event, context):
    print("Event=" + str(event))
    try:
        old_rates_str = s3Client.download_from_s3_or_empty(OLD_RATES_FILE_NAME, FILE_PATH)

        req = Request(SITE, headers={'User-Agent': 'AWS Lambda'})
        downloaded_json_string = str(urlopen(req).read().decode())
        print(downloaded_json_string)

        downloaded_json_dom = json.loads(downloaded_json_string)
        updated_rates_json_dom = json.loads(old_rates_str)
        updated_rates_json_dom.update(downloaded_json_dom)

        updated_rates_dict_str = json.dumps(updated_rates_json_dom)

        # old-rates
        s3Client.upload_to_s3(updated_rates_dict_str, OLD_RATES_FILE_NAME, FILE_PATH)

        # v1 - for backwards compatibility - deprecated now
        s3Client.upload_to_s3(updated_rates_dict_str, FILE_NAME, FILE_PATH)

        # v2
        json_string_v2 = rates_to_list_convertor_regex.convert(updated_rates_json_dom)
        s3Client.upload_to_s3(json_string_v2, FILE_NAME_V2, FILE_PATH)

    except Exception:
        print('Json download failed.')
        raise
    else:
        print('Json downloaded.')
        # return event['time']
        return
    finally:
        print('Check complete at {}'.format(str(datetime.now())))
