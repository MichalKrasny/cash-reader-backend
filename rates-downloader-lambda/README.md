# Python Rates Downloader

This is a lambda downloads rates from `http://www.floatrates.com/daily/usd.json` stores them to S3. 
This separate project exists to make Lambda Python independent of the infrastructure Python.

See full documentation [here](../rates-downloader/README.md).

## Configuration
Lambda requires these properties to be set.

* `url` - URL of the downloaded rates file
* `bucket_name` different for test and prod
* `file_path` can be blank if file should be stored to the root of the file
* `file_name` file name of the deprecated version 1 
* `file_name_v2` file name of the current version 2
