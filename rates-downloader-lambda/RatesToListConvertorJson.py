import json


class RatesToListConvertorJson:

    @staticmethod
    def convert(updated_rates_json_dom):
        updated_list = list(updated_rates_json_dom.values())
        return json.dumps(updated_list)
