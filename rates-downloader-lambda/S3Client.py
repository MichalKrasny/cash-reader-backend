import boto3
from botocore.exceptions import ClientError


class S3Client:

    def __init__(self, bucket_name, file_path):
        self.bucket_name = bucket_name
        self.file_path = file_path
        self.s3 = boto3.client("s3")

    def upload_to_s3(self, json_string, file_name, file_path):
        print(f'upload_to_s3, file_path={file_path}, file_name={file_name}')
        encoded_string = json_string.encode("utf-8")
        s3_path = self.get_s3_path(file_name, file_path)
        self.s3.put_object(Bucket=self.bucket_name, Key=s3_path, Body=encoded_string)

    def download_from_s3_or_empty(self, file_name, file_path):
        print(f'download_from_s3, file_path={file_path}, file_name={file_name}')
        s3_path = self.get_s3_path(file_name, file_path)

        try:
            result_object = self.s3.get_object(Bucket=self.bucket_name, Key=s3_path)
            print(f'Got a file from S3')
            result_bytes = result_object['Body'].read()
            result = result_bytes.decode("utf-8")
            return result
        except ClientError as e:
            print(f'Error retrieving file {s3_path}: %s', e.response['Error']['Message'])
            print('Returning empty result "{}"')
            # if there is no file, we'll work with empty array
            return '{}'

    @staticmethod
    def get_s3_path(file_name, file_path):
        if not file_path:
            s3_path = file_name
        else:
            s3_path = file_path + "/" + file_name
        return s3_path
